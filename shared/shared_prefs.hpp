#if !defined(HPPshared_prefs)
#define HPPshared_prefs

#include "CVector.hpp"

/**
 * Values to need to be accessible by the host and arduino.
 */
namespace shared_prefs
{
  /**
   * Range of each linear actuator.
   */
  constexpr CVector3f k_vecLinearActuatorRanges{
    430.0f,
    280.0f,
    240.0f
  };

  constexpr CVector3f k_vecLinearActuatorHalfRanges{
    k_vecLinearActuatorRanges / 2.0f
  };

  /**
   * Minimum and maximum input values to {@link CPanTilt::setTilt} and
   * {@link CPanTilt::setPan}.
   */
  static constexpr float k_flMinTilt{ -1.0f };
  static constexpr float k_flMaxTilt{ 1.0f };
  static constexpr float k_flMinPan{ -1.0f };
  static constexpr float k_flMaxPan{ 1.0f };

  /**
   * Offset of the camera's sensor from the camera's pivot.
   * If the camera is installed properly, this is 0.
   */
  constexpr CVector3f k_vecCameraPivotOffset{};

  /**
   * Distance from the camera's pivot to to tip of its lens.
   */
  constexpr float k_flCameraLength{ 170.0f };
}

#endif