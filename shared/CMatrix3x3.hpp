#if !defined(HPPshared_CMatrix3x3)
#define HPPshared_CMatrix3x3

#include "CVector.hpp"

template <class T>
class CMatrix3x3
{
public:
  using vec_type = CVector<T, 3>;

private:
  /**
   * 0 1 2
   * 3 4 5
   * 6 7 8
   */
  T m_data[9]{};

public:
  /**
   * 
   */
  vec_type operator*(const vec_type& vec)
  {
    return {
      this->m_data[0] * vec[0] + this->m_data[1] * vec[1] + this->m_data[2] * vec[2],
      this->m_data[3] * vec[0] + this->m_data[4] * vec[1] + this->m_data[5] * vec[2],
      this->m_data[6] * vec[0] + this->m_data[7] * vec[1] + this->m_data[8] * vec[2]
    };
  }

public:
  /**
   * 
   */
  CMatrix3x3(const T(&data)[9])
  {
    for (int i{ 0 }; i < 9; ++i)
    {
      this->m_data[i] = data[i];
    }
  }

public:

  /**
   * 
   */
  static CMatrix3x3 fromColumns(const vec_type& a, const vec_type& b, const vec_type& c)
  {
    return {
      a[0], b[0], c[0],
      a[1], b[1], c[1],
      a[2], b[2], c[2]
    };
  }
};

#endif