#if !defined(HPPshared_CListStatic)
#define HPPshared_CListStatic

#include "CList.hpp"

template <class T, long int k_iLength>
class CListStatic : public CList<T[static_cast<size_t>(k_iLength)], T, k_iLength>
{
public:
  using typename CList<T[static_cast<size_t>(k_iLength)], T, k_iLength>::CList;
  using typename CList<T[static_cast<size_t>(k_iLength)], T, k_iLength>::size_type;

  CListStatic(const T (&data)[k_iLength])
  {
    for (size_type i{ 0 }; i < k_iLength; ++i)
    {
      this->m_arrData[i] = data[i];
    }
  }
};

#endif