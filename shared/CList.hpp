#if !defined(HPPshared_CList)
#define HPPshared_CList

#include <stddef.h>

template <class TList, class TElement, int k_iLength>
class CList
{
public:
  using size_type = long int;
  using difference_type = long int;

protected:
  /**
   * 
   */
  TList m_arrData;

public:
  /**
   * 
   */
  const TElement* begin(void) const
  {
    return this->m_arrData;
  }

  /**
   * 
   */
  const TElement* end(void) const
  {
    return (this->m_arrData + k_iLength);
  }

  /**
   * 
   */
  TElement* begin(void)
  {
    return this->m_arrData;
  }

  /**
   * 
   */
  TElement* end(void)
  {
    return (this->m_arrData + k_iLength);
  }

  /**
   * 
   */
  TElement* insert(const TElement* position, const TElement& val)
  {
    size_type iIndex{ position - this->m_arrData };

    if (iIndex >= k_iLength)
    {
      // We can't throw, because Arduino doesn't use exceptions.
      return nullptr;
    }

    this->m_arrData[iIndex] = val;

    return position;
  }

  /**
   * 
   */
  TElement &operator[](size_type iIndex)
  {
    return this->m_arrData[static_cast<size_t>(iIndex)];
  }

  /**
   * 
   */
  const TElement &operator[](size_type iIndex) const
  {
    return this->m_arrData[iIndex];
  }

  /**
   * 
   */
  size_type size(void) const
  {
    return k_iLength;
  }

  /**
   * 
   */
  CList(const CList &that)
  {
    for (size_type i{ 0 }; i < k_iLength; ++i)
    {
      this->m_arrData[i] = that.m_arrData[i];
    }
  }

  /**
   * 
   */
  CList(void)
  {
  }
};

#endif