#if !defined(HPPshared_CObserverNode)
#define HPPshared_CObserverNode

#include "CVector.hpp"

#if !defined(ARDUINO)
#include "STraceHit.hpp"
#endif

class CObserverNode
{
private:
  /**
   * Postition of the node without offset adjustments.
   */
  CVector3f m_vecOrigin{};

  /**
   * Offset due to deformation.
   */
  CVector3f m_vecOffset{};

  /**
   * 
   */
  CVector3f m_vecDirection{};

  /**
   * Measured distance to the target. The ranging sensor is placed parallel to
   * the camera.
   */
  float m_flDistance;

#if !defined(ARDUINO)
public:
  /**
   * Hit that produced this node. Only used for preview generation.
   */
  STraceHit hit{};
#endif

public:
  /**
   * 
   */
  const CVector3f& getOrigin(void) const;
  
  /**
   * 
   */
  const CVector3f& getOffset(void) const;

  /**
   * 
   */
  const CVector3f& getDirection(void) const;

  /**
   * 
   */
  float getDistance(void) const;

  /**
   * @return {@link getOrigin} + {@link getOffset}
   */
  CVector3f getHardwareOrigin(void) const;

public:
  /**
   * Moves the node by @vecOffset without modifying the direction or offset.
   */
  void shift(const CVector3f& vecOffset);

  /**
   * Moves the node to @vecOrigin
   */
  void setOrigin(const CVector3f& vecOrigin, const CVector3f& vecOffset);

public:
  /**
   * 
   */
  CObserverNode(
      const CVector3f& vecOrigin, const CVector3f& vecOffset,
      const CVector3f& vecDirection,
      float flDistance);

  /**
   * 
   */
  CObserverNode(void);
};

#endif