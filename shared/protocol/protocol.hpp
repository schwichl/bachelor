#if !defined(HPPprotocol)
#define HPPprotocol

#if defined(ARDUINO)
#include "Arduino.h"
#else
#include <cstdint>
#include <cstring>
#endif

// arduino
namespace protocol
{
  namespace packet
  {
    class CVerifyProtocol;
  }
}

namespace protocol
{

#if defined(ARDUINO)
  using Bool = bool;
  using Char = char;
  using Int8 = int8_t;
  using Int16 = int16_t;
  using Int32 = int32_t;
  using Int64 = int64_t;
  using Float = float;
#else
  using Bool = bool;
  using Char = char;
  using Int8 = std::int_least8_t;
  using Int16 = std::int_least16_t;
  using Int32 = std::int_least32_t;
  using Int64 = std::int_least64_t;
  using Float = float;
#endif

  /**
   * Must be increased with every change to the shared files.
   */
  constexpr Int32 k_iVersion{ 27 };

  // {@link Int8} is enough, but we need 16 to align {@link CPacket::SHeader}.
  enum class ECommand : Int16
  {
    kInvalid = 0,

    kVerifyProtocol,
    kVerifyProtocolSuccess, // No payload.
    kVerifyProtocolFailed, // No payload, because communication is not stable.

    kPing, // No payload
    kPong, // No payload

    kRequestConfirmation, // No payload
    kConfirmationAllow, // No payload
    kConfirmationDeny, // No payload
    kPrint, // C-style string
    kImageCaptured, // Payload: Int32 iNodeId
    kStatus, // No payload
    kGetPosition, // No payload
    kPosition, // {@link packet::CPosition}
    kSnap, // Payload: Bool bFocusStack

    kHomeMotor, // {@link packet::CHomeMotor}
    kTurnMotor, // {@link packet::CTurnMotor}

    kMove, // {@link packet::CMove}
    kSetAngles, // {@link packet::CSetAngles}
    kView, // {@link packet::CView}
    kWalk, // Payload: Float flDistance

    kSetPath, // {@link packet::CSetPath}
    kGetPathSegment, // {@link packet::CGetPathSegment}
    kUploadPath, // {@link packet::CUploadPath}
    kUploadPathSuccess, // No payload.
    kUploadPathFailure, // No payload.
    kScan, // No payload.
    kScanComplete, // No payload.

    kDisconnect, // No payload.

    kCount,
  };

  // #if !defined(ARDUINO)
  static constexpr const char* ECommandSTR[static_cast<int>(ECommand::kCount)]{
    "invalid",

    "verify_protocol",
    "verify_protocol_success",
    "verify_protocol_failed",

    "ping",
    "pong",

    "request_confirmation",
    "confirmation_allow",
    "confirmation_deny",
    "print",
    "image_captured",
    "status",
    "get_position",
    "position",
    "snap",

    "home_motor"
    "turn_motor",

    "move",
    "set_angles",
    "view",
    "walk",

    "set_path",
    "get_path_segment",
    "upload_path",
    "upload_path_success",
    "upload_path_failed",
    "scan",
    "scan_complete",

    "disconnect"
  };
  // #endif

  enum class EVerificationError
  {
    kSuccess = 0,
    kVersionMismatch,
    kEndianness,
    kTwosComplement,
    kNegativeInteger,
    kFloatingPoint,

    kCount,
  };

#if !defined(ARDUINO)
  static constexpr const char* EVerificationErrorSTR[static_cast<int>(EVerificationError::kCount)]{
    "success",
    "version_mismatch",
    "endianness",
    "twos_complement",
    "negative_integer",
    "floating_point",
  };
#endif

  /**
   * @return True if @that matches a default-constructed {@link CVerifyProtocol}.
   */
  EVerificationError verify(const packet::CVerifyProtocol& that);

  /**
   * Verifies types at run-time.
   * If this test succeeds once, it will succeed every time, even after a
   * restart.
   */
  EVerificationError verifyOffline(void);

  class CPacket
  {
  public:
    struct SHeader
    {
      /**
       * 
       */
      ECommand command;

      /**
       * Payload length in bytes.
       */
      Int16 iLength;
    };

  private:
    /**
     * 
     */
    SHeader m_header;

    /**
     * 
     */
    char* m_pArrPayload;

  public:
    /**
     * @return The raw header. Invalidated when the packet goes out of scope.
     */
    const char* getRawHeader(void) const;

    /**
     * @return The raw payload. Invalidated when the packet goes out of scope.
     */
    const char* getRawPayload(void) const;

    /**
     * @return Length of the payload
     */
    Int16 getPayloadLength(void) const;

    /**
     * 
     */
    ECommand getCommand(void) const;

    /**
     * @return The payload. Invalidated when the packet goes out of scope.
     */
    template <class T>
    const T* getPayload(void) const
    {
      return reinterpret_cast<T*>(this->m_pArrPayload);
    }

  public:
    /**
     * 
     */
    CPacket& operator=(CPacket&& that);
    /**
     * 
     */
    CPacket& operator=(const CPacket& that);

  public:
    /**
     * 
     */
    template <class T>
    CPacket(ECommand command, const T& data)
        : m_header{ command, static_cast<Int16>(sizeof(T)) },
          m_pArrPayload{ new char[sizeof(T)] }
    {
      memcpy(
          static_cast<void*>(this->m_pArrPayload),
          static_cast<const void*>(&data),
          sizeof(T));
    }

    /**
     * 
     */
    CPacket(ECommand command, const char* arrData, int iLength);

    /**
     * Data will be moved.
     */
    CPacket(SHeader&& header, char* pArrData);

    /**
     * 
     */
    CPacket(CPacket&& that);

    /**
     * 
     */
    CPacket(CPacket& that);

    /**
     * 
     */
    CPacket(void);

    /**
     * 
     */
    ~CPacket(void);

  public:
    /**
     * Data will be moved.
     */
    static CPacket fromRaw(SHeader&& header, char* pArrData);
  };
}

#endif