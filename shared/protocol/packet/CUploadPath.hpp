#if !defined(HPPprotocol_packet_CUploadPath)
#define HPPprotocol_packet_CUploadPath

#include "CPacket.hpp"

#include "CObserverPath.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CUploadPath : public CPacket
    {
    public:
      /**
       * The caller has to free the memory
       * @param pISize Out data length
       */
      static char* serialize(const CObserverPath& path, int* pISize);

      /**
       * 
       */
      static CObserverPath deserialize(const char* data);
    };
  }
}

#endif