#if !defined(HPPprotocol_packet_CPosition)
#define HPPprotocol_packet_CPosition

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

#include "CVector.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CPosition : public CPacket
    {
    public:
      Float flX;
      Float flY;
      Float flZ;

      Float flOffsetX;
      Float flOffsetY;
      Float flOffsetZ;

      constexpr CPosition(const CVector3f& vecOrigin, const CVector3f& vecOffset)
          : flX{ vecOrigin[0] },
            flY{ vecOrigin[1] },
            flZ{ vecOrigin[2] },
            flOffsetX{ vecOffset[0] },
            flOffsetY{ vecOffset[1] },
            flOffsetZ{ vecOffset[2] }
      {
      }
    };
  }
}

#endif