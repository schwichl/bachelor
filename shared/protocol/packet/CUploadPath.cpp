#include "CUploadPath.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    /**
     * count: Int16 Number of observer nodes
     * {
     *   origin: Float[3]
     *   offset: Float[3]
     *   direction: Float[3]
     *   distance: Float
     * } * count
     */

    /**
     * 
     */
    char* CUploadPath::serialize(const CObserverPath& path, int* pISize)
    {
      int iNodeCount{ path.getNodeCount() };

      *pISize = static_cast<int>(sizeof(Int16) +
                                 static_cast<size_t>(iNodeCount) *
                                     ((sizeof(Float) * 10)));

      char* arrData{ new char[static_cast<size_t>(*pISize)] };

      *reinterpret_cast<Int16*>(arrData + 0) = static_cast<Int16>(iNodeCount);

      for (int i{ 0 }; i < iNodeCount; ++i)
      {
        auto uiBaseOffset{ sizeof(Int16) + ((sizeof(Float) * 10) * static_cast<size_t>(i)) };

        const CObserverNode* pNode{ &path.getNode(i) };

        for (int j{ 0 }; j < 3; ++j)
        {
          auto uiIndexOffset{ sizeof(Float) * static_cast<size_t>(j) };

          *reinterpret_cast<Float*>(arrData + uiBaseOffset + uiIndexOffset) = pNode->getOrigin()[j];
          *reinterpret_cast<Float*>(arrData + uiBaseOffset + uiIndexOffset + sizeof(Float) * 3) = pNode->getOffset()[j];
          *reinterpret_cast<Float*>(arrData + uiBaseOffset + uiIndexOffset + sizeof(Float) * 6) = pNode->getDirection()[j];
        }

        *reinterpret_cast<Float*>(arrData + uiBaseOffset + sizeof(Float) * 9) = pNode->getDistance();
      }

      return arrData;
    }

    /**
     * 
     */
    CObserverPath CUploadPath::deserialize(const char* data)
    {
      CObserverPath path{};

      int iNodeCount{ *reinterpret_cast<const Int16*>(data + 0) };

      path.reserve(iNodeCount);

      for (int i{ 0 }; i < iNodeCount; ++i)
      {
        auto uiBaseOffset{ sizeof(Int16) + ((sizeof(Float) * 10) * static_cast<size_t>(i)) };

        CVector3f vecOrigin;
        CVector3f vecOffset;
        CVector3f vecDirection;

        for (int j{ 0 }; j < 3; ++j)
        {
          auto uiIndexOffset{ sizeof(Float) * static_cast<size_t>(j) };

          vecOrigin[j] = *reinterpret_cast<const Float*>(data + uiBaseOffset + uiIndexOffset);
          vecOffset[j] = *reinterpret_cast<const Float*>(data + uiBaseOffset + uiIndexOffset + (sizeof(Float) * 3));
          vecDirection[j] = *reinterpret_cast<const Float*>(data + uiBaseOffset + uiIndexOffset + (sizeof(Float) * 6));
        }

        float flDistance{ *reinterpret_cast<const Float*>(data + uiBaseOffset + (sizeof(Float) * 9)) };

        CObserverNode point{
          vecOrigin,
          vecOffset,
          vecDirection,
          flDistance
        };

        path.push(point);
      }

      return path;
    }
  }
}