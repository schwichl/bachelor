#if !defined(HPPprotocol_packet_CSetPath)
#define HPPprotocol_packet_CSetPath

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    /**
     * Sends information about the path from the host to the Arduino
     */
    class CSetPath : public CPacket
    {
    public:
      Int32 iNodeCount{};
    };
  }
}

#endif