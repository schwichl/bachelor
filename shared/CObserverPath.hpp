#if !defined(HPPshared_CObserverPath)
#define HPPshared_CObserverPath

#include "CListDynamic.hpp"
#include "CObserverNode.hpp"
#include "CVector.hpp"

#if !defined(ARDUINO)
#include "model/CModel.hpp"
#include "model/CPly.hpp"

#include <string>
#endif

namespace protocol
{
  namespace packet
  {
    class CUploadPath;
  }
}

enum class ECheckZ;

/**
 * This is basically a {@link CListDynamic}.
 */
class CObserverPath : public CListDynamic<CObserverNode>
{
  // {@link CRobot} builds path segments.
  friend class CRobot;

  friend class protocol::packet::CUploadPath;

public:
  /**
   * @return Number of nodes.
   */
  int getNodeCount(void) const;

  /**
   * @return Node at the specified index
   */
  const CObserverNode& getNode(int iIndex) const;

#if !defined(ARDUINO)
private:
  /**
   * Generates a preview rectangle for a single hit.
   */
  static void generatePreviewImage(const STraceHit& hit, CPly* pPlyOut);

private:
  /**
   * Adds focus stack nodes.
   */
  void addFocusStack(void);

public:
  /**
   * Removes unreachable nodes.
   */
  void removeUnreachableNodes(ECheckZ checkZ);

  /**
   * Sorts the nodes for fastest traversal.
   */
  void sortNodes(void);

  /**
   * All nodes are shifted. Unreachable nodes are removed.
   * @param vecOffset How much to shift the nodes.
   */
  void offsetAndFilterNodes(const CVector3f& vecOffset);

  /**
   * Generates a preview path and writes it to a file.
   */
  void generatePreviewImages(const std::string &strFileName, const CVector3f &vecOffset = {}, float flScale = 1.0f) const;

  /**
   * Generates preview images and writes them to a file.
   */
  void generatePreviewPath(const std::string &strFileName, const CVector3f &vecOffset = {}, float flScale = 1.0f) const;

  /**
   * Generates a path centered at (0, 0, 0).
   * @param flSteps Horizontal steps on Y 0.
   * @param flScanDistance Distance between a point and the model's surface in
   *                       millimeters.
   */
  static CObserverPath fromModel(const CModel& model,
                                 float flSteps,
                                 float flScanDistance);
#endif
};

#endif