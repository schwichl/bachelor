#if !defined(HPPshared_CDeformer)
#define HPPshared_CDeformer

#include "CVector.hpp"

/**
 * 
 */
class CDeformer
{
private:
  CDeformer(void) = default;

public:
  /**
   * 
   */
  static CDeformer* instance(void);

public:
  /**
   * Calculates the offset of the camera's sensor from camera's neutral
   * position.
   * @param vecDirection The direction the camera is supposed to be facing.
   */
  CVector3f getCameraRotationalOffset(const CVector3f& vecDirection) const;

  /**
   * Calculates the offset of the camera based on structural deformation.
   * @param vecOrigin The origin we want to move to
   */
  CVector3f getStructuralDeformationOffset(const CVector3f& vecOrigin) const;

  /**
   * Applies deformation.
   * @param vecOrigin Origin we want to move to
   * @param vecDirection Direction we want to be facing
   * @return Offset from @vecOrigin
   */
  CVector3f realizeOrigin(const CVector3f& vecOrigin, const CVector3f& vecDirection) const;
};

#endif