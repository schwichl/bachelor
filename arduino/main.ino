#include "CLog.hpp"
#include "CLoopHook.hpp"
#include "CSerial.hpp"
#include "CVector.hpp"
#include "hardware.hpp"
#include "power.hpp"
#include "pinconfig.hpp"

#include "packethandler/CPacketHandler.hpp"

#include "protocol/packet/CTurnMotor.hpp"
#include "protocol/packet/CVerifyProtocol.hpp"
#include "protocol/protocol.hpp"

#include "robot/components/CVL6180X.hpp"

#include "robot/test/CTestManager.hpp"

#include "robot/CRobot.hpp"

#include "Arduino.h"

// Required for virtual destructors in avr-gcc.
void operator delete(void* pObject, size_t sSize) noexcept
{
  ::operator delete(pObject, sSize);
}

bool g_bSerialReady{ false };

/**
 * Extracts and executes a single command from the host.
 */
void listenOnce(void)
{
  protocol::CPacket pckReceived;

  if (CSerial::readPacket(&pckReceived))
  {
    CPacketHandler::instance()->processPacket(pckReceived);
    // processPacket(pckReceived);
  }
  else
  {
    slog << "couldn't read packet\n";
  }
}

/**
 * Gets called when a serial connection has been established.
 */
void initRobot()
{
  CSerial::write(protocol::ECommand::kVerifyProtocol, protocol::packet::CVerifyProtocol{});

  protocol::CPacket pckVerification{};

  if (CSerial::readPacket(&pckVerification))
  {
    switch (pckVerification.getCommand())
    {
    case protocol::ECommand::kVerifyProtocolSuccess:
      break;
    case protocol::ECommand::kVerifyProtocolFailed:
      slog.error(CLog::EErrorSource::kProtocol, 2);
      return;
    default:
      slog.error(CLog::EErrorSource::kProtocol, 1);
      return;
    }
  }
  else
  {
    // We didn't receive a reply.
    slog.error(CLog::EErrorSource::kProtocol, 0);
    return;
  }

  if (!CRobot::instance()->setup())
  {
    slog << "robot setup failed\n";
  }
  else
  {
    if (!CTestManager::instance()->run())
    {
      slog << "== self-test failed ==\n";

      slog << "continue anyway?\n";
      power::g_bAwaitingConfirmation = true;
      CSerial::write(protocol::ECommand::kRequestConfirmation, '\0');
    }
    else
    {
      slog << "hello :-) 123\n";
    }
  }
}

void setup()
{
  Serial.begin(9600);

  { // arduino
    auto error{ protocol::verifyOffline() };
    if (error != protocol::EVerificationError::kSuccess)
    {
      slog.error(CLog::EErrorSource::kProtocol, error);
    }
  }
}

void loop()
{
  // True if this is the first connected loop cycle.
  static bool s_bIsFirstCycle{ true };

  // Allows devices to react to power changes.
  static bool s_bLastCycle{ false };

  if (g_bSerialReady)
  {
    if (power::g_bDie)
    {
      if (s_bLastCycle || s_bIsFirstCycle)
      {
        // We never started or died more than 1 cycle ago.
        return;
      }
      else
      {
        slog << "power::g_bDie = true\n";
        s_bLastCycle = true;
      }
    }
    else
    {
      while (Serial.available() > 0)
      {
        listenOnce();
      }
    }

    g_loopHook.tick();

    // static auto ulNextSpin{ millis() };

    // if (ulNextSpin < millis())
    // {
    //   auto ulStart{ millis() };
    //   float flDistance{ hardware::pDistanceSensor->getDistance() };
    //   auto ulEnd{ millis() };

    //   slog << (ulEnd - ulStart) << " distance: " << flDistance << '\n';
    //   ulNextSpin = (millis() + 1000);
    // }

    s_bIsFirstCycle = false;
  }
  else
  {
    // We can't upload if we're stuck in a loop. This delay allows us to reset
    // the arduino, then upload.
    delay(1000);

    if (Serial.read() != -1)
    {
      g_bSerialReady = true;
      initRobot();
    }
  }
}