#if !defined(HPPshared_CListDynamic)
#define HPPshared_CListDynamic

#include "CList.hpp"

#include <stddef.h>

template <class T>
class CListDynamic : public CList<T*, T, 0>
{
public:
  using typename CList<T*, T, 0>::size_type;

private:
  /**
   * Reserved space.
   */
  size_type m_iCapacity{ 0 };

  /**
   * Used space.
   */
  size_type m_iLength{ 0 };

public:
  /**
   * 
   */
  const T* end(void) const
  {
    return (this->m_arrData + this->m_iLength);
  }

  /**
   * 
   */
  T* end(void)
  {
    return (this->m_arrData + this->m_iLength);
  }

  /**
   * 
   */
  T* insert(const T* position, const T& val)
  {
    size_type iIndex{ position - this->m_arrData };

    if (iIndex >= this->m_iLength)
    {
      this->resize(iIndex + 1);
    }

    this->m_arrData[iIndex] = val;

    return position;
  }

  /**
   * 
   */
  size_type size(void) const
  {
    return this->m_iLength;
  }

  /**
   * 
   */
  size_type capacity(void) const
  {
    return this->m_iCapacity;
  }

  /**
   * @return True if the list is empty.
   */
  bool empty(void) const
  {
    return (this->m_iLength == 0);
  }

  /**
   * Also down-sizes the list to be usable by {@link resize}.
   */
  void reserve(size_type iSize)
  {
    T* arrNew{ new T[static_cast<size_t>(iSize)]{} };

    for (size_type i{ 0 }; i < ((this->m_iLength < iSize) ? this->m_iLength : iSize); ++i)
    {
      arrNew[i] = this->m_arrData[i];
    }

    delete[] this->m_arrData;

    this->m_arrData = arrNew;
    this->m_iCapacity = iSize;
  }

  /**
   * 
   */
  void resize(size_type iSize)
  {
    this->reserve(iSize);

    this->m_iLength = iSize;
  }

  /**
   * Removes all elements.
   */
  void clear(void)
  {
    this->m_iLength = 0;
  }

  /**
   * 
   */
  void push(const T& t)
  {
    if (this->m_iLength == this->m_iCapacity)
    {
      this->reserve(((this->m_iCapacity * 3) / 2) + 1);
    }

    this->m_arrData[this->m_iLength] = t;
    ++this->m_iLength;
  }

public:
  /**
   * 
   */
  CListDynamic& operator=(const CListDynamic& that)
  {
    if (this->m_arrData != nullptr)
    {
      delete[] this->m_arrData;
    }

    this->m_iLength = that.m_iLength;

    this->m_arrData = new T[static_cast<size_t>(that.m_iLength)];

    for (size_type i{ 0 }; i < that.m_iLength; ++i)
    {
      this->m_arrData[i] = that.m_arrData[i];
    }

    return *this;
  }

public:
  /**
   *
   */
  CListDynamic(const CListDynamic& that)
  {
    this->m_iCapacity = that.m_iLength;
    this->m_iLength = that.m_iLength;

    this->m_arrData = new T[static_cast<size_t>(that.m_iLength)];

    for (size_type i{ 0 }; i < that.m_iLength; ++i)
    {
      this->m_arrData[i] = that.m_arrData[i];
    }
  }

  /**
   * 
   */
  template <size_type size>
  CListDynamic(const T (&arr)[size])
  {
    this->m_iCapacity = size;
    this->m_iLength = size;

    this->m_arrData = new T[static_cast<size_t>(size)];

    for (size_type i{ 0 }; i < size; ++i)
    {
      this->m_arrData[i] = arr[i];
    }
  }

  /**
   * 
   */
  CListDynamic(void)
  {
    this->m_arrData = nullptr;
  }

  /**
   * 
   */
  ~CListDynamic(void)
  {
    delete[] this->m_arrData;
  }
};

#endif