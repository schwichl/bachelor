#if !defined(HPPpower)
#define HPPpower

namespace power
{
  /**
   * Stop all components in place.
   * Stop listening.
   * Can only be un-done by resetting the arduino.
   */
  extern bool g_bDie;

  /**
   * Stop all components in place.
   * Ignores all non-confirmation commands.
   * Continues when {@link protocol::ECommand::kConfirmationAllow} is received.
   * If {@link protocol::ECommand::kConfirmationDeny} is received,
   * {@link g_bDie} is set to true.
   */
  extern bool g_bAwaitingConfirmation;
}

#endif