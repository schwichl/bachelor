#if !defined(HPPscanconfig)
#define HPPscanconfig

#include "shared_prefs.hpp"

/*

--------------------------
| camera                 X pivot
--------------------------
                ^
                | k_flRangingSensorOffset
                v
ranging sensor (-] <---> X
                     ^ k_flRangingSensorPivotDistance

*/

namespace scanconfig
{
  /**
   * Distance from the camera's pivot to the surface of the target in
   * millimeters.
   */
  constexpr float k_flScanDistance{ shared_prefs::k_flCameraLength + 100.0f };

  /**
   * Offset of the ranging sensor's ray to the camera's ray in millimeters.
   * The ranging sensor is facing parallel to the camera, offset to the bottom.
   */
  constexpr float k_flRangingSensorOffset{ 45.0f };

  /**
   * Offset of the ranging sensor from the camera's pivot in millimeters.
   */
  constexpr float k_flRangingSensorPivotDistance{ 110.0f };

  /**
   * Use the ranging sensor to focus during scans.
   */
  constexpr bool k_bEnableRangingSensor{ false };

  /**
   * Use focus stacking during a scan.
   */
  constexpr bool k_bFocusStackScan{ false };

  /**
   * Focus stack height in millimeters.
   */
  constexpr float k_flFocusStackHeight{ 5.0f };

  /**
   * Number of images taken inside the focus stack.
   */
  constexpr int k_iFocusStackSize{ 3 };
}

#endif