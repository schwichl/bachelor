#include "hardware.hpp"

#include "robot/components/CCamera.hpp"
#include "robot/components/CEncoder.hpp"
#include "robot/components/CGearMotor.hpp"
#include "robot/components/CLinearActuatorHorizontal.hpp"
#include "robot/components/CLinearActuatorVertical.hpp"
#include "robot/components/CServo.hpp"
#include "robot/components/CStepperMotor.hpp"
// #include "robot/components/CVL53L0X.hpp"
#include "robot/components/CPanTilt.hpp"
#include "robot/components/CVL6180X.hpp"

namespace hardware
{
  CCamera* pCamera{ nullptr };

  CEncoder* pEncoder{ nullptr };

  CGearMotor* pGearMotor{ nullptr };

  CLinearActuatorHorizontal* pLinearActuatorX{ nullptr };

  CLinearActuatorVertical* pLinearActuatorY{ nullptr };

  CLinearActuatorHorizontal* pLinearActuatorZ{ nullptr };

  CStepperMotor* pStepperX{ nullptr };

  CStepperMotor* pStepperZ{ nullptr };

  CServo* pServoTilt{ nullptr };

  CServo* pServoPan{ nullptr };

  CPanTilt* pPanTilt{ nullptr };

  CVL6180X* pDistanceSensor{ nullptr };
}