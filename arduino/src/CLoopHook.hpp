#if !defined(HPPCLoopHook)
#define HPPCLoopHook

#include "CLog.hpp"
#include "CActiveComponent.hpp"

class CLoopHook
{
private:
  static constexpr int k_iMaxCallbacks{ 16 };

private:
  /**
   * 
   */
  CActiveComponent *m_arrCallbacks[k_iMaxCallbacks]{};

  /**
   * 
   */
  int m_iCallbacks{ 0 };

public:
  /**
   * Registers a callback to a member function.
   */
  void registerCallback(CActiveComponent *pComponent);

  /**
   * Removes a callback.
   */
  void unregisterCallback(CActiveComponent *pComponent);

  /**
   * Called by the main loop.
   */
  void tick(void);
};

extern CLoopHook g_loopHook;

#endif