#if !defined(HPPCActiveComponent)
#define HPPCActiveComponent

/**
 * Everything that wants to hook the main loop is an {@link CActiveComponent}.
 */
class CActiveComponent
{
public:
  /**
   * Main loop callback
   */
  virtual void tick(void) = 0;

  /**
   * 
   */
  CActiveComponent(void);

  /**
   * 
   */
  virtual ~CActiveComponent(void);
};

#endif