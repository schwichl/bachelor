#include "CObserverNode.hpp"

/**
 * 
 */
const CVector3f& CObserverNode::getOrigin(void) const
{
  return this->m_vecOrigin;
}

/**
 * 
 */
const CVector3f& CObserverNode::getOffset(void) const
{
  return this->m_vecOffset;
}

/**
 * 
 */
const CVector3f& CObserverNode::getDirection(void) const
{
  return this->m_vecDirection;
}

/**
 * 
 */
float CObserverNode::getDistance(void) const
{
  return this->m_flDistance;
}

/**
 * 
 */
CVector3f CObserverNode::getHardwareOrigin(void) const
{
  return (this->m_vecOrigin + this->m_vecOffset);
}

/**
 * 
 */
void CObserverNode::shift(const CVector3f& vecOffset)
{
  this->m_vecOrigin += vecOffset;

#if !defined(ARDUINO)
  this->hit.vecOrigin += vecOffset;
#endif
}

/**
 * 
 */
void CObserverNode::setOrigin(const CVector3f& vecOrigin, const CVector3f& vecOffset)
{
  this->m_vecOrigin = vecOrigin;
  this->m_vecOffset = vecOffset;
}

/**
 * 
 */
CObserverNode::CObserverNode(
    const CVector3f& vecOrigin,
    const CVector3f& vecOffset,
    const CVector3f& vecDirection,
    float flDistance)
    : m_vecOrigin{ vecOrigin },
      m_vecOffset{ vecOffset },
      m_vecDirection{ vecDirection },
      m_flDistance{ flDistance }
{
}

/**
 * 
 */
CObserverNode::CObserverNode(void)
{
}