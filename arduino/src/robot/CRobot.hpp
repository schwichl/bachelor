#if !defined(HPProbot_CRobot)
#define HPProbot_CRobot

#include "CActiveComponent.hpp"
#include "CObserverPath.hpp"

#include "CListStatic.hpp"

#include "./components/ICamera.hpp"
#include "./components/IDistanceSensor.hpp"
#include "./components/ILinearActuator.hpp"
#include "./components/IPanTilt.hpp"

class CDeadMansSwitch;
class CLinearActuator;

/**
 * Coordinates in millimeters.
 * Coordinate (0 0 0) is front bottom left.
 * Coordinate (+ + +) is back top right.
 * 
 * Angles (0 0) are facing (0 0 1).
 * Angles (90 0) are facing (0 -1 0).
 * Angles (0 90) are facing (-1 0 0).
 */
class CRobot : public CActiveComponent
{
private:
  enum class EState
  {
    kInvalid,

    // We're adjusting the linear actuators and pan/tilt unit to move to the
    // next node.
    kMovingToNode,

    // We're taking a reading from the ranging sensor and moving the linear
    // actuators to focus the camera.
    kFocusingNode,

    // We're moving through the focus stack
    kFocusStacking,

    // The camera is capturing an image.
    kCapturingImage,

    // A node has been scanned or a path segment was requested. Wait for the
    // next node to be available for scanning.
    kWaitingForNextNode,

    kCount,
  };

private:
  /**
   * Nodes per segment (1 more than this will be in each segment).
   */
  static constexpr int k_iSegmentSize{ 32 };

public:
  /**
   * 
   */
  static CRobot* instance(void);

private:
  /**
   * Left/Right
   */
  ILinearActuator* m_pActuatorX{};

  /**
   * Forward/Backward
   */
  ILinearActuator* m_pActuatorY{};

  /**
   * Up/Down
   */
  ILinearActuator* m_pActuatorZ{};

  /**
   * 
   */
  ICamera* m_pCamera{};

  /**
   * 
   */
  IDistanceSensor* m_pDistanceSensor{};

  /**
   * 
   */
  IPanTilt* m_pPanTilt{};

  /**
   * 
   */
  CDeadMansSwitch* m_pDeadMansSwitch{};

  /**
   * True if we're currently scanning.
   */
  bool m_bScanning{ false };

  /**
   * Current scanning state.
   */
  EState m_state{ EState::kInvalid };

  /**
   * Current focus stack layer.
   */
  int m_iFocusStackIndex{ 0 };

  /**
   * Node count of the entire path.
   */
  int m_iTotalPathSize{};

  /**
   * Index of the node that is currently targeted.
   * If no node is currently targeted, this is the index of the node to be
   * targeted next.
   */
  int m_iNextNode{};

  /**
   * Index of the node to be downloaded next.
   */
  int m_iNextDownloadNode{};

  /**
   * True if we're waiting for the next segment to arrive.
   */
  bool m_bDownloading{ false };

  /**
   * Current path segment.
   */
  CObserverPath m_path{};

  /**
   * True if we're waiting for Z to be home.
   */
  bool m_bHomingZ{ false };

  /**
   * Time at which the robot stops shaking.
   */
  unsigned long m_uiShakeEndTime{ 0 };

  /**
   * Time in microseconds between the most recent- and the tick before.
   */
  unsigned long m_uiTickTime{ 0 };

  /**
   * Time in microseconds when the last tick took place.
   */
  unsigned long m_uiLastTick{ 0 };

  /**
   * Origin that we were asked to move to.
   */
  CVector3f m_vecOrigin{};

  /**
   * Offset applied to {@link m_vecOrigin}.
   */
  CVector3f m_vecOffset{};

private:
  /**
   * @return Movable components
   */
  CListStatic<IMovable*, 4> getMovables(void) const;

  /**
   * @return Number of nodes scanned in the current scan.
   */
  int getScannedNodeCount(void) const;

  /**
   * @return True if any motor is active.
   */
  bool isMoving(void) const;

  /**
   * @return True if the robot is structurally shaking.
   * This can only know about shaking introduced from motors, not from other
   * sources.
   */
  bool isShaking(void) const;

  /**
   * @return True if the origin is reachable.
   */
  bool isReachable(const CVector3f& vecOrigin) const;

private:
  /**
   * 
   */
  void setState(EState state);

  /**
   * Moves the linear actuators to @vecOrigin.
   * The pan/tilt's rotation is not considered!
   * @return Clamped origin.
   */
  CVector3f setHardwareOrigin(const CVector3f& vecOrigin);

  /**
   * Moves the camera's sensor to @vecOrigin. Applies deformation.
   * Does not set tilt/pan.
   */
  void setOrigin(const CVector3f& vecOrigin, const CVector3f& vecDirection);

  /**
   * Calls {@link setHardwareOrigin} with @vecOrigin + @vecOffset and updates
   * the internal position.
   */
  void setOriginOffset(const CVector3f& vecOrigin, const CVector3f& vecOffset);

  /**
   * Sets the camera's angles such that its sensor is aligned with
   * @vecAxis.
   * @param vecAxis Normalized axis.
   */
  void alignCameraWithAxis(const CVector3f& vecAxis);

  /**
   * Gets the origin after translating it to the focus stack layer at
   * @iLayerIndex.
   */
  void getFocusStackLayer(const CVector3f& vecOrigin, int iLayerIndex,
                                CVector3f* pVecLayer, CVector3f* pVecLayerOffset);

  /**
   * Gets the origin after translating it to the focus stack layer at
   * @iLayerIndex.
   * If @iLayerIndex is 0, returns the index and origin of the first reachable
   * layer.
   * @return Index of the layer or -1
   */
  int saveGetFocusStackLayer(const CVector3f& vecOrigin, int iLayerIndex,
                                CVector3f* pVecLayer, CVector3f* pVecLayerOffset);

  /**
   * Moves to the given node without focusing it.
   * Sets state!
   */
  void moveToNode(const CObserverNode& node);

  /**
   * Focuses the given node.
   */
  void focusNode(const CObserverNode& node);

  /**
   * Captures the given node. {@link moveToNode} and {@link focusNode} should
   * already have been called.
   */
  void captureNode(int iNodeIndex);

  /**
   * Fired whenever a state has been completed.
   */
  void onStateComplete(void);

  /**
   * Clears all scan properties.
   */
  void resetScan(void);

  /**
   * Firest when a scan ends.
   */
  void onScanComplete(void);

  /**
   * Requests the next path segment from the host.
   * Upon receival, {@link scan} will be called.
   */
  void requestNextSegment(void);

  /**
   * Updates the tick time.
   * Needs to be called once per tick.
   */
  void updateTickTime(void);

public:
  /**
   * 
   */
  virtual void tick(void) override;

  /**
   * The linear actuators' positions.
   */
  CVector3f getHardwareOrigin(void) const;

  /**
   * Origin we were asked to move to. No offset applied.
   */
  const CVector3f& getSoftwareOrigin(void) const;

  /**
   * Deformation offset.
   */
  const CVector3f& getOffset(void) const;

  /**
   * Origin of the camera's sensor.
   */
  CVector3f getCameraOrigin(void) const;

  /**
   * Called by components that cause structural shake.
   * @param uiDuration Duration in milliseconds until the shake stops.
   */
  void shake(unsigned long uiDuration);

  /**
   * 
   */
  void setPathInformation(int iNodeCount);

  /**
   * 
   */
  void setPath(const CObserverPath& path);

  /**
   * @return True if a scan is in progress.
   */
  bool isScanning(void) const;

  /**
   * Aborts the active scan. Homes.
   */
  void abort(void);

  /**
   * Scans the entire target object.
   */
  void scan(void);

  /**
   * 
   */
  void setAngles(const CVector2f& vecAngles);

  /**
   * Moves to the given position. Applies deformation.
   * Arguments may be nullptr to keep the position unchanged.
   * Assumes a valid software origin.
   */
  void move(float* pX, float* pY, float* pZ);

  /**
   * Moves to @vecOrigin and sets angles to @vecAngles (T, P in radians).
   * Doesn't update the state.
   * Doesn't realize origin.
   */
  void view(const CVector3f& vecOrigin, const CVector2f& vecAngles);

  /**
   * Moves the camera along its view axis.
   * @param flDistance Distance in millimeters. Positive and negative.
   * @return False if the camera can't be moved to the new point.
   */
  bool moveAlongRay(float flDistance);

  /**
   * Take a picture.
   * @param bStack Take pictures as a focus stack using the configuration from
   *               {@link scanconfig}
   */
  void snap(bool bFocusStack);

  /**
   * Prints the status of all components and scan progress.
   */
  void printStatus(void) const;

  /**
   * Used by the linear actuators to notify {@link CRobot} that the end switch
   * was reached.
   */
  void notifyHome(const CLinearActuator* pLinearActuator);

  /**
   * Homes all components.
   */
  void home(void);

  /**
   * Performs a setup routine.
   * Errors are printed.
   * @return True on success.
   */
  bool setup(void);

public:
  /**
   * 
   */
  CRobot(void);
};

#endif