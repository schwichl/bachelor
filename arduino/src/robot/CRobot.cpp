#include "CRobot.hpp"

#include "robot/components/CCamera.hpp"
#include "robot/components/CDeadMansSwitch.hpp"
#include "robot/components/CLinearActuatorHorizontal.hpp"
#include "robot/components/CLinearActuatorVertical.hpp"
#include "robot/components/CPanTilt.hpp"
// #include "robot/components/CVL53L0X.hpp"
#include "robot/components/CVL6180X.hpp"

#include "CDeformer.hpp"
#include "CLog.hpp"
#include "CMatrix3x3.hpp"
#include "CSerial.hpp"
#include "hardware.hpp"
#include "pinconfig.hpp"
#include "scanconfig.hpp"
#include "shared_prefs.hpp"

#include "protocol/packet/CGetPathSegment.hpp"

#include "cmath"

/**
 * 
 */
CRobot* CRobot::instance(void)
{
  static CRobot s_instance{};

  return &s_instance;
}

/**
 * 
 */
CListStatic<IMovable*, 4> CRobot::getMovables(void) const
{
  return { { this->m_pActuatorX,
             this->m_pActuatorY,
             this->m_pActuatorZ,
             this->m_pPanTilt } };
}

/**
 * This might be off by 1.
 */
int CRobot::getScannedNodeCount(void) const
{
  return ((this->m_iNextDownloadNode - k_iSegmentSize) + this->m_iNextNode);
}

/**
 * 
 */
bool CRobot::isMoving(void) const
{
  const auto& arrMovables{ this->getMovables() };

  for (const IMovable* pMovable : arrMovables)
  {
    if (pMovable->isMoving())
    {
      return true;
    }
  }

  return false;
}

/**
 * This function is missing wrap-around detection.
 * If
 * uiTime = 2^32-1
 * this->m_uiShakeEndTime = 3
 * then @this->m_uiShakeEndTime is in the future, even though it's less than
 * @uiTime.
 * The wrap-around will happen once every 7 weeks, it's not worth implementing a
 * detection.
 */
bool CRobot::isShaking(void) const
{
  return (this->m_uiShakeEndTime >= millis());
}

/**
 * 
 */
bool CRobot::isReachable(const CVector3f& vecOrigin) const
{
  for (int i{ 0 }; i < 3; ++i)
  {
    if ((vecOrigin[i] < 0.0f) ||
        (vecOrigin[i] > shared_prefs::k_vecLinearActuatorRanges[i]))
    {
      return false;
    }
  }

  return true;
}

/**
 * 
 */
void CRobot::setState(EState state)
{
  this->m_state = state;
}

/**
 * 
 */
CVector3f CRobot::setHardwareOrigin(const CVector3f& vecOrigin)
{
  return {
    this->m_pActuatorX->moveToRanged(vecOrigin[0]),
    this->m_pActuatorY->moveToRanged(vecOrigin[1]),
    this->m_pActuatorZ->moveToRanged(vecOrigin[2])
  };
}

/**
 * 
 */
void CRobot::setOrigin(const CVector3f& vecOrigin, const CVector3f& vecDirection)
{
  CVector3f vecOffset{
    CDeformer::instance()->realizeOrigin(vecOrigin, vecDirection)
  };

  // Clamping will mess up information sent via ECommand::kImageCaptured, but
  // deformation calculation is not reversible.
  this->m_vecOrigin = this->setHardwareOrigin(vecOrigin + vecOffset);
  this->m_vecOffset = vecOffset;
}

/**
 * 
 */
void CRobot::setOriginOffset(const CVector3f& vecOrigin, const CVector3f& vecOffset)
{
  this->setHardwareOrigin(vecOrigin + vecOffset);

  this->m_vecOrigin = vecOrigin;
  this->m_vecOffset = vecOffset;
}

/**
 * 
 */
void CRobot::alignCameraWithAxis(const CVector3f& vecAxis)
{
  float flPitch{ -std::atan2(vecAxis[1], std::sqrt(1.0f - (vecAxis[1] * vecAxis[1]))) };
  float flYaw{ -std::atan2(vecAxis[0], vecAxis[2]) };

  this->m_pPanTilt->setAngles({ flPitch, flYaw });
}

/**
 * 
 */
void CRobot::getFocusStackLayer(const CVector3f& vecOrigin, int iLayerIndex,
                                CVector3f* pVecLayer, CVector3f* pVecLayerOffset)
{
  const CVector3f& vecDirection{ this->m_pPanTilt->getAxis() };

  constexpr float k_flLayerHeight{ 
    scanconfig::k_flFocusStackHeight /
                                   (scanconfig::k_iFocusStackSize - 1) };

  CVector3f vecClosestLayer{
    vecOrigin + (scanconfig::k_flFocusStackHeight / 2.0f)
  };

  *pVecLayer = (vecClosestLayer -
                (vecDirection * k_flLayerHeight * iLayerIndex));

  *pVecLayerOffset = CDeformer::instance()->realizeOrigin(*pVecLayer, vecDirection);
}

/**
 * 
 */
int CRobot::saveGetFocusStackLayer(const CVector3f& vecOrigin, int iLayerIndex,
                                   CVector3f* pVecLayer, CVector3f* pVecLayerOffset)
{
  if (iLayerIndex == 0)
  {
    for (int i{ 0 }; i < scanconfig::k_iFocusStackSize; ++i)
    {
      CVector3f vecLayer;
      CVector3f vecLayerOffset;

      this->getFocusStackLayer(vecOrigin, i, &vecLayer, &vecLayerOffset);

      if (this->isReachable(vecLayer + vecLayerOffset))
      {
        *pVecLayer = vecLayer;
        *pVecLayerOffset = vecLayerOffset;
        return i;
      }
    }
  }
  else
  {
    CVector3f vecLayer;
    CVector3f vecLayerOffset;

    this->getFocusStackLayer(vecOrigin, iLayerIndex, &vecLayer, &vecLayerOffset);

    if (this->isReachable(vecLayer + vecLayerOffset))
    {
      *pVecLayer = vecLayer;
      *pVecLayerOffset = vecLayerOffset;
      return iLayerIndex;
    }
    else
    {
      return -1;
    }
  }
}

/**
 * 
 */
void CRobot::moveToNode(const CObserverNode& node)
{
  slog << "CRobot::moveToNode(" << node.getOrigin() << ", " << node.getOffset()
       << ", " << node.getDirection() << ")\n";

  CVector3f vecOrigin{ node.getOrigin() };
  CVector3f vecOffset{ CDeformer::instance()->realizeOrigin(vecOrigin, node.getDirection()) };

  this->alignCameraWithAxis(node.getDirection());
  // this->setOriginOffset(node.getOrigin(), node.getOffset());
  this->setOriginOffset(vecOrigin, vecOffset);

  this->setState(EState::kMovingToNode);
}

/**
 * The ranging sensor is placed parallel to the camera.
 * Focusing only works when the origin was set through software (ie.
 * {@link getSoftwareOrigin} returns a valid value.
 * We can't work around this, because we need to know the new software origin
 * and offset in order to get the camera sensor's position later on.
 */
void CRobot::focusNode(const CObserverNode& node)
{
  /**
   * If the ranging sensor reports a value larger than this, discard it.
   */
  static constexpr float k_flDistanceCutoff{ 300.0f };

  constexpr float k_flLayerHeight{ 
    scanconfig::k_flFocusStackHeight /
                                   (scanconfig::k_iFocusStackSize - 1) };

  // arduino
  if (!scanconfig::k_bEnableRangingSensor)
  {
    this->setState(EState::kFocusingNode);
    return;
  }

  float flIdealDistance{ node.getDistance() };

  if (flIdealDistance == 0.0f)
  {
    // No distance available. Probably lower neck.
    return;
  }

  float flDistance{ this->m_pDistanceSensor->getDistance() };

  if (flDistance == 0.0f)
  {
    slog << "couldn't get a distance reading for node at " << node.getOrigin() << '\n';
  }
  else if (flDistance > k_flDistanceCutoff)
  {
    slog << "distance from " << node.getOrigin() << " is too large ("
         << flDistance << "mm). expected " << flIdealDistance << "mm\n";
  }
  else
  {
    float flDiff{ flDistance - flIdealDistance };

    const CVector3f& vecOrigin{ this->getSoftwareOrigin() };
    CVector3f vecNewOrigin{ vecOrigin + (node.getDirection() * flDiff) };

    // Move forward @flDiff millimeters (Or backwards, if @flDiff is negative).
    this->setOrigin(vecNewOrigin, node.getDirection());
  }

  this->setState(EState::kFocusingNode);
}

/**
 * 
 */
void CRobot::captureNode(int iNodeIndex)
{
  this->m_pCamera->takePicture();

  CSerial::write(protocol::ECommand::kImageCaptured, protocol::Int32{ iNodeIndex });

  this->setState(EState::kCapturingImage);
}

/**
 * 
 */
void CRobot::onStateComplete(void)
{
  // Ready to enter the next state
  if (this->m_iNextNode >= this->m_path.getNodeCount())
  {
    if (this->m_iNextDownloadNode >= this->m_iTotalPathSize)
    {
      this->onScanComplete();
    }
    else
    {
      this->requestNextSegment();
    }

    return;
  }

  const CObserverNode& nodeNext{ this->m_path.getNode(this->m_iNextNode) };

  switch (this->m_state)
  {
  case EState::kMovingToNode:
    this->focusNode(nodeNext);
    break;
  case EState::kFocusingNode:
    this->captureNode(this->getScannedNodeCount());
    break;
  case EState::kCapturingImage:
    ++this->m_iNextNode;
    this->setState(EState::kWaitingForNextNode);
    break;
  case EState::kWaitingForNextNode:
    slog << this->getScannedNodeCount() << '/' << this->m_iTotalPathSize << " node(s) scanned\n";

    this->moveToNode(nodeNext);
    break;
  }
}

/**
 *
 */
void CRobot::resetScan(void)
{
  this->m_bScanning = false;

  this->m_iNextDownloadNode = 0;
  this->m_iNextNode = 0;
  this->m_path.clear();
}

/**
 * 
 */
void CRobot::onScanComplete(void)
{
  this->resetScan();

  this->home();

  CSerial::write(protocol::ECommand::kScanComplete, '\0');
}

/**
 * 
 */
void CRobot::requestNextSegment(void)
{
  protocol::packet::CGetPathSegment pckGetPathSegment{};

  pckGetPathSegment.iNodeStart = this->m_iNextDownloadNode;
  pckGetPathSegment.iNodeEnd = min((this->m_iNextDownloadNode + k_iSegmentSize), (this->m_iTotalPathSize - 1));

  CSerial::write(protocol::ECommand::kGetPathSegment, pckGetPathSegment);

  this->m_iNextDownloadNode += k_iSegmentSize;

  this->m_bDownloading = true;
}

/**
 * 
 */
void CRobot::updateTickTime(void)
{
  auto uiTime{ micros() };
  this->m_uiTickTime = (uiTime - this->m_uiLastTick);
  this->m_uiLastTick = micros();
}

/**
 * 
 */
void CRobot::tick(void)
{
  this->updateTickTime();

  if (this->m_bScanning)
  {
    if (!this->m_bDownloading)
    {
      if (!this->isMoving())
      {
        // Should we wait for shake before we measure the distance?

        if ((this->m_state != EState::kFocusingNode) || !this->isShaking())
        {
          if ((this->m_state != EState::kCapturingImage) || !this->m_pCamera->isCapturing())
          {
            this->onStateComplete();
          }
        }
      }
    }
  }
  else
  {
    if (this->m_iFocusStackIndex != 0)
    {
      if (this->m_state == EState::kFocusStacking)
      {
        if (!this->isMoving() && !this->isShaking())
        {
          this->m_pCamera->takePicture();
          this->m_state = EState::kInvalid;
          
          if (++this->m_iFocusStackIndex == scanconfig::k_iFocusStackSize)
          {
            slog << "focus stack done\n";
            this->m_iFocusStackIndex = 0;
          }
          else
          {
            slog << "captured focus stack " << this->m_iFocusStackIndex << '\n';
          }
        }
      }
      else
      {
        if (!this->m_pCamera->isCapturing())
        {
          constexpr float k_flFocusLayerHeight{
            scanconfig::k_flFocusStackHeight /
            (scanconfig::k_iFocusStackSize - 1)
          };

          if (this->moveAlongRay(-k_flFocusLayerHeight))
          {
            // We can reuse {@link m_state}, we're not scanning right now.
            this->m_state = EState::kFocusStacking;
          }
          else
          {
            // Can't move any further, abort
            slog << "focus stack layer " << this->m_iFocusStackIndex << " is out of range\n";
            this->m_iFocusStackIndex = 0;
          }
        }
      }
    }
  }

  if (this->m_bHomingZ)
  {
    if (!this->m_pActuatorZ->isMoving())
    {
      // Z arrived at home. Home everything else.
      this->m_bHomingZ = false;

      this->m_pActuatorX->home();
      this->m_pActuatorY->home();
      this->m_pPanTilt->home();
    }
  }
}

/**
 * 
 */
CVector3f CRobot::getHardwareOrigin(void) const
{
  return {
    this->m_pActuatorX->getRangedPosition(),
    this->m_pActuatorY->getRangedPosition(),
    this->m_pActuatorZ->getRangedPosition()
  };
}

/**
 * 
 */
const CVector3f& CRobot::getSoftwareOrigin(void) const
{
  return this->m_vecOrigin;
}

/**
 * 
 */
const CVector3f& CRobot::getOffset(void) const
{
  return this->m_vecOffset;
}

/**
 * 
 */
CVector3f CRobot::getCameraOrigin(void) const
{
  return this->m_vecOrigin;
}

/**
 * 
 */
void CRobot::shake(unsigned long uiDuration)
{
  unsigned long uiTime{ millis() };
  unsigned long uiShakeEndTime{ uiTime + uiDuration };

  if ((uiTime >= this->m_uiShakeEndTime) || // Wrap-around detection
      (uiShakeEndTime > this->m_uiShakeEndTime))
  {
    this->m_uiShakeEndTime = uiShakeEndTime;
  }
}

/**
 * 
 */
void CRobot::setPathInformation(int iNodeCount)
{
  this->m_iTotalPathSize = iNodeCount;
  this->m_iNextDownloadNode = 0;
}

/**
 * 
 */
void CRobot::setPath(const CObserverPath& path)
{
  this->m_path = path;
}

/**
 * 
 */
bool CRobot::isScanning(void) const
{
  return this->m_bScanning;
}

/**
 * 
 */
void CRobot::abort(void)
{
  this->resetScan();
  CSerial::write(protocol::ECommand::kScanComplete, '\0');

  slog << "aborted\n";
}

/**
 * 
 */
void CRobot::scan(void)
{
  // {@link CPacketHandler::processPacket} sets {@link m_path} and calls
  // {@link scan}.

  const CObserverPath& path{ this->m_path };

  this->m_bScanning = true;

  if (this->m_iNextDownloadNode == 0)
  {
    // Request the initial segment. {@link scan} will be fired one it arrives.
    this->requestNextSegment();
  }
  else
  {
    this->m_bDownloading = false;
    this->m_iNextNode = 0;
    this->setState(EState::kWaitingForNextNode);
  }
}

/**
 * 
 */
void CRobot::setAngles(const CVector2f& vecAngles)
{
  CVector3f vecAxis{
    CVector3f{ vecAngles[0], vecAngles[1], 0.0f }.toAxis()
  };

  this->alignCameraWithAxis(vecAxis);
}

/**
 *
 */
void CRobot::move(float* pX, float* pY, float* pZ)
{
  if (!pX && !pY && !pZ)
  {
    return;
  }

  CVector3f vecOrigin{
    (pX ? *pX : this->m_vecOrigin[0]),
    (pY ? *pY : this->m_vecOrigin[1]),
    (pZ ? *pZ : this->m_vecOrigin[2])
  };

  this->setOrigin(vecOrigin, this->m_pPanTilt->getAxis());
}

/**
 * 
 */
void CRobot::view(const CVector3f& vecOrigin, const CVector2f& vecAngles)
{
  CVector3f vecAxis{
    CVector3f{ vecAngles[0], vecAngles[1], 0.0f }.toAxis()
  };

  this->setOrigin(vecOrigin, vecAxis);
  this->alignCameraWithAxis(vecAxis);
}

/**
 * 
 */
bool CRobot::moveAlongRay(float flDistance)
{
  slog << "CRobot::moveAlongRay(" << flDistance << ")\n";

  CVector3f vecDirection{ this->m_pPanTilt->getAxis() };

  // TODO: {@link CPanTilt::getAxis} is wrong
  vecDirection[0] *= -1.0f;
  vecDirection[1] *= -1.0f;

  CVector3f vecNewOrigin{
    this->m_vecOrigin +
    (vecDirection * flDistance)
  };

  slog << "vecDirection " << vecDirection << '\n';
  slog << "vecOrigin " << this->m_vecOrigin << '\n';
  slog << "vecNewOrigin " << vecNewOrigin << '\n';

  CVector3f vecOffset{
    CDeformer::instance()->realizeOrigin(vecNewOrigin, vecDirection)
  };

  vecNewOrigin += vecOffset;

  if (!this->isReachable(vecNewOrigin))
  {
    slog << vecNewOrigin << " isn't reachable\n";
    return false;
  }

  // {@link setHardwareOrigin} should always return @vecNewOrigin, because we
  // verified its reachability.
  this->m_vecOrigin = this->setHardwareOrigin(vecNewOrigin);
  this->m_vecOffset = vecOffset;

  return true;
}

/**
 * 
 */
void CRobot::snap(bool bFocusStack)
{
  this->m_pCamera->takePicture();

  if (bFocusStack)
  {
    this->m_iFocusStackIndex = 1;
  }
}

/**
 * 
 */
void CRobot::printStatus(void) const
{
  static constexpr const char* k_arrSzState[static_cast<int>(EState::kCount)]{
    "invalid",
    "moving_to_node",
    "capturing_image",
    "waiting_for_next_node"
  };

  static constexpr const char* k_arrSzMovables[]{
    "x",
    "y",
    "z",
    "pan/tilt"
  };

  slog << "us/tick: " << this->m_uiTickTime << '\n';

  slog << "man: " << (this->m_pDeadMansSwitch->isAlive() ? "alive" : "dead") << '\n';

  slog << "scanning: " << this->m_bScanning << '\n';

  if (this->m_bScanning)
  {
    slog << " state: " << k_arrSzState[static_cast<int>(this->m_state)] << '\n';
    slog << " progress: " << this->getScannedNodeCount() << '/' << this->m_iTotalPathSize << '\n';
  }

  slog << "moving: " << this->isMoving() << '\n';

  const auto& lstMovables{ this->getMovables() };

  for (int i{ 0 }; i < lstMovables.size(); ++i)
  {
    if (lstMovables[i]->isMoving())
    {
      slog << ' ' << k_arrSzMovables[i] << '\n';
    }
  }

  slog << "capturing: " << this->m_pCamera->isCapturing() << '\n';

  slog << "shaking: " << this->isShaking() << '\n';

  if (scanconfig::k_bEnableRangingSensor)
  {
    slog << "distance: " << hardware::pDistanceSensor->getDistance() << "mm\n";
  }
  else
  {
    slog << "distance: disabled\n";
  }

  const auto& vecOrigin{ this->getHardwareOrigin() };
  slog << "XYZ: " << vecOrigin[0] << ' ' << vecOrigin[1] << ' ' << vecOrigin[2] << '\n';

  const auto& vecAngles{ this->m_pPanTilt->getAngles() };
  slog << "TP: " << vecAngles[0] << ' ' << vecAngles[1] << '\n';
}

/**
 * 
 */
void CRobot::notifyHome(const CLinearActuator* pLinearActuator)
{
  if (pLinearActuator == this->m_pActuatorX)
  {
    this->m_vecOrigin[0] = 0.0f;
    this->m_vecOffset[0] = 0.0f;
  }
  else if (pLinearActuator == this->m_pActuatorY)
  {
    this->m_vecOrigin[1] = 0.0f;
    this->m_vecOffset[1] = 0.0f;
  }
  else if (pLinearActuator == this->m_pActuatorZ)
  {
    this->m_vecOrigin[2] = 0.0f;
    this->m_vecOffset[2] = 0.0f;
  }
}

/**
 * Home Z first to move away from the target.
 * The other components are homed in {@link tick}
 */
void CRobot::home(void)
{
  if (this->m_pDeadMansSwitch->isAlive())
  {
    if (this->m_bScanning)
    {
      this->abort();
    }

    this->m_pActuatorZ->home();
    this->m_bHomingZ = true;
  }
}

/**
 *
 */
bool CRobot::setup(void)
{
  this->m_pCamera = new CCamera{ pinconfig::k_camera };
  hardware::pCamera = this->m_pCamera;

  {
    this->m_pActuatorX = hardware::pLinearActuatorX =
        new CLinearActuatorHorizontal{
          pinconfig::k_linearActuatorX,
          shared_prefs::k_vecLinearActuatorRanges[0]
        };

    hardware::pStepperX =
        &static_cast<CLinearActuatorHorizontal*>(this->m_pActuatorX)->m_motor;
  }

  {
    this->m_pActuatorY = hardware::pLinearActuatorY =
        new CLinearActuatorVertical{
          pinconfig::k_linearActuatorY,
          shared_prefs::k_vecLinearActuatorRanges[1]
        };

    hardware::pGearMotor =
        &static_cast<CLinearActuatorVertical*>(this->m_pActuatorY)->m_motor;
    hardware::pEncoder =
        &static_cast<CLinearActuatorVertical*>(this->m_pActuatorY)->m_encoder;
  }

  {
    this->m_pActuatorZ = hardware::pLinearActuatorZ =
        new CLinearActuatorHorizontal{
          pinconfig::k_linearActuatorZ,
          shared_prefs::k_vecLinearActuatorRanges[2]
        };

    hardware::pStepperZ =
        &static_cast<CLinearActuatorHorizontal*>(this->m_pActuatorZ)->m_motor;
  }

  this->m_pDistanceSensor = new CVL6180X{};
  this->m_pDistanceSensor->setup();
  hardware::pDistanceSensor = static_cast<CVL6180X*>(this->m_pDistanceSensor);

  this->m_pPanTilt = new CPanTilt{ pinconfig::k_panTilt };

  hardware::pPanTilt = static_cast<CPanTilt*>(this->m_pPanTilt);

  this->m_pDeadMansSwitch = new CDeadMansSwitch{ pinconfig::k_iRelay };

  return true;
}

CRobot::CRobot(void)
{
}