#if !defined(HPPCLinearActuatorVertical)
#define HPPCLinearActuatorVertical

#include "CLinearActuator.hpp"

#include "CEncoder.hpp"
#include "CGearMotor.hpp"

class CLinearActuatorVertical
    : public CLinearActuator
{
  // To allow {@link CRobot} to set the debugging hardware.
  friend class CRobot;

public:
  struct SPins
  {
    CGearMotor::SPins gearMotor{};
    CEncoder::SPins encoder{};
    int iEndSwitch{};
  };

private:
  /**
   * Lead screw pitch in mm.
   */
  static constexpr float k_flLeadScrewPitch{ 2.0f };

  /**
   * Number of thread starts in the lead screw.
   */
  static constexpr int k_iLeadScrewThreadStarts{ 4 };

  /**
   * Milliseconds until the robot stops shaking after this actuator stops
   * moving.
   */
  static constexpr unsigned long k_uiShakeTime{ 1000 };

private:
  /**
   * The motor powering the actuator.
   */
  CGearMotor m_motor{};

  /**
   * The encoder attached to {@link m_motor}
   */
  CEncoder m_encoder{};

  /**
   * Number of revolutions returned by {@link CGearMotor::getRevolutions} when
   * the actuator is at home.
   */
  float m_flMotorRevolutionsHomeOffset{ 0.0f };

  /**
   * True if we're returning from a pressed end switch to home.
   */
  bool m_bSeekingHome{ false };

protected:
  /**
   * @override
   */
  virtual void onStop(void) override;

  /**
   * @override
   */
  virtual void onEndSwitchPressed(void) override;

  /**
   * @override
   */
  virtual void onEndSwitchReleased(void) override;

public:
  /**
   * @override
   */
  virtual float getRangedPosition(void) const override;

public:
  /**
   * @override
   */
  virtual void home(void) override;

  /**
   * @override
   */
  virtual void tick(void) override;

public:
  /**
   * Parameters are forwarded to the setup routine of the stepper motor. 
   */
  CLinearActuatorVertical(
      const SPins& pins,
      float flTravelLength);

  /**
   * 
   */
  virtual ~CLinearActuatorVertical(void){};
};

#endif