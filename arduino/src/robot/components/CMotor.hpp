#if !defined(HPProbot_components_CMotor)
#define HPProbot_components_CMotor

#include "IMovable.hpp"

class CMotor : public IMovable
{
public:
  enum class EDirection
  {
    kClockwise,
    kCounterClockwise,
  };

protected:
  /**
   * 
   */
  bool m_bDirectionLocked{ false };

  /**
   * If 1 or -1, only allow movement in that direction.
   */
  EDirection m_iDirectionLock{};

public:
  /**
   * Turns the motor @flRevolutions.
   */
  virtual void spin(float flRevolutions) = 0;

  /**
   * Turns the motor in @iDirection until {@link stop} is called.
   */
  virtual void spin(EDirection iDirection) = 0;

  /**
   * Immediately stops the motor.
   */
  virtual void stop(void) = 0;

  /**
   * Prevents movement in one direction. Only movement in @iDirection will be
   * allowed.
   * @param iDirection Allow movement in this direction only.
   */
  void lockDirection(EDirection iDirection);

  /**
   * 
   */
  void unlockDirection(void);
};

#endif