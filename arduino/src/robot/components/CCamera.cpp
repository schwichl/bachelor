#include "CCamera.hpp"

#include "Arduino.h"

/**
 * 
 */
void CCamera::tick(void)
{
  if (this->m_bCapturing)
  {
    if (millis() > this->m_uiTriggerEndTime)
    {
      slog << "camera low\n";
      this->m_bCapturing = false;
      digitalWrite(this->m_iPinTrigger, LOW);
    }
  }
}

/**
 * 
 */
float CCamera::getLength(void) const
{
  return shared_prefs::k_flCameraLength;
}

/**
 * What if this gets called rapidly? The camera won't be able to keep up.
 */
void CCamera::takePicture(void)
{
  this->m_bCapturing = true;

  slog << "camera high\n";
  digitalWrite(this->m_iPinTrigger, HIGH);

  this->m_uiTriggerEndTime = (millis() + k_uiTriggerDuration);
}

/**
 * 
 */
bool CCamera::isCapturing(void) const
{
  return this->m_bCapturing;
}

/**
 * 
 */
CCamera::CCamera(const CCamera::SPins& pins)
{
  this->m_iPinTrigger = pins.iTrigger;

  pinMode(this->m_iPinTrigger, OUTPUT);
  digitalWrite(this->m_iPinTrigger, LOW);
}