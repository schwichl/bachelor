#include "CEncoder.hpp"

#include "CLog.hpp"

#include "CGearMotor.hpp"

static CEncoder* s_pInstance{ nullptr };

void interruptOutA(void)
{
  s_pInstance->m_iCounts += s_pInstance->m_pMotor->getDirection();
}

/**
 * 
 */
float CEncoder::getRevolutions(void) const
{
  return (static_cast<float>(this->m_iCounts) / k_iCountsPerRevolution);
}

/**
 * 
 */
void CEncoder::setup(const CEncoder::SPins& pins, const CGearMotor* pMotor)
{
  this->m_pins = pins;
  this->m_pMotor = pMotor;

  attachInterrupt(
      digitalPinToInterrupt(this->m_pins.iChannelA),
      interruptOutA,
      RISING);
}

/**
 * 
 */
CEncoder::CEncoder(void)
{
  s_pInstance = this;
}