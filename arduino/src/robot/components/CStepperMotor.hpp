#if !defined(HPPCStepperMotor)
#define HPPCStepperMotor

#include "CActiveComponent.hpp"
#include "CMotor.hpp"

#include "CAccelerationCurve.hpp"

#include "Arduino.h"

class CStepperMotor
    : public CActiveComponent,
      public CMotor
{
public:
  struct SPins
  {
    /**
     * Pin 1 of coil A.
     */
    int iCoilA1{};

    /**
     * Pin 2 of coil A.
     */
    int iCoilA2{};

    /**
     * Pin 1 of coil B.
     */
    int iCoilB1{};

    /**
     * Pin 2 of coil B.
     */
    int iCoilB2{};
  };

private:

  /**
   * 
   */
  static constexpr int k_iPins{ 4 };

  /**
   * Minimum number of microseconds between two starts of steps.
   * Max 120rpm = 2rps = min 2500 microseconds per step.
   */
  static constexpr unsigned long k_uiMinStepDelay{ 2500ul };

  /**
   * 
   */
  static constexpr int k_iControlPatterns{ 4 };

public:
  /**
   * 
   */
  static constexpr int k_iStepsPerRevolution{ 200 };

private:
  /**
   * Coil 1 is indexed by: 0, 1
   * Coil 2 is indexed by: 2, 3
   */
  int m_arrIPins[k_iPins]{};

  /**
   * Microseconds.
   */
  unsigned long m_uiStepDelay{ k_uiMinStepDelay };

  /**
   * Index into {@link k_arrIControlPatterns}.
   */
  int m_iCurrentPattern{ 0 };

  /**
   * Number of steps we started spinning at.
   */
  int m_iRequestedSteps{ 0 };

  /**
   * Positive as well as negative.
   */
  int m_iRemainingSteps{ 0 };

  /**
   * True if we're spinning indefinitely.
   */
  bool m_bFreeSpinning{ false };

  /**
   * Time in microseconds when the next step will be taken.
   */
  unsigned long m_uiNextStepTime{ 0ul };

  /**
   * Smooth acceleration.
   * Units are steps.
   */
  CAccelerationCurve m_accelerationCurve{};

private:
  /**
   * Sets the control pattern to @iIndex (into {@link k_arrIControlPatterns}).
   * The index is modulated.
   */
  void setPattern(int iIndex);

  /**
   * 
   */
  virtual void tick(void) override;

public:
  /**
   * @override
   */
  virtual bool isMoving(void) const override;

  /**
   * Steps @iStepCount steps. Steps are summed, not queued. eg.
   * 200 -> -50 will make 150 steps. Not 200, then -50.
   * @param iStepCount Number of steps. If this is INT_MAX, the remaining steps
   * will be set to INT_MAX and not overflow. Same for INT_MIN. After setting
   * this to a limit, the motor needs to be unlocked by using {@link stop}.
   */
  void step(int iStepCount);

  /**
   * @override
   */
  virtual void spin(float flRevolutions) override;

  /**
   * @override
   */
  virtual void spin(CMotor::EDirection iDirection) override;

  /**
   * @override
   */
  virtual void stop(void) override;

  /**
   * @flRPS will be clamped to an upper bound.
   */
  void setSpeed(float flRPS);

  /**
   * @param flAccelerationPeriod Acceleration period in revolutions.
   */
  void setAccelerationPeriod(float flAccelerationPeriod);

  /**
   * 
   */
  void setup(const SPins& pins);

public:
  /**
   * 
   */
  CStepperMotor(void);

  /**
   * 
   */
  virtual ~CStepperMotor(void) {}
};

#endif