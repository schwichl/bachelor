#if !defined(HPPCVL53L0X)
#define HPPCVL53L0X

#include "IDistanceSensor.hpp"

#include <VL53L0X.h>

class CVL53L0X : public IDistanceSensor
{
private:

  /**
   * Time in microseconds allowed per measurement.
   */
  static constexpr uint32_t k_uiMeasurementTimingBudget{ 1000000 };

private:
  /**
   * 
   */
  VL53L0X m_sensor{};

public:
  /**
   * @override
   */
  virtual float getDistance(void) override;

  /**
   * @override
   */
  virtual void setup(void) override;
};

#endif