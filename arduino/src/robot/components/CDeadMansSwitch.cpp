#include "CDeadMansSwitch.hpp"

#include "CLog.hpp"
#include "power.hpp"

#include "Arduino.h"

/**
 * 
 */
void CDeadMansSwitch::onDisconnect(void)
{
  slog << "dead man's switch opened\n";
  power::g_bDie = true;
}

/**
 * 
 */
void CDeadMansSwitch::tick(void)
{
  bool bConnected{ digitalRead(this->m_iPin) == LOW };

  if (!bConnected && this->m_bPreviousState)
  {
    this->onDisconnect();
  }

  this->m_bPreviousState = bConnected;
}

/**
 * 
 */
bool CDeadMansSwitch::isAlive(void) const
{
  return this->m_bPreviousState;
}

/**
 *
 */
CDeadMansSwitch::CDeadMansSwitch(int iPin)
    : m_iPin{ iPin }
{
  pinMode(this->m_iPin, INPUT_PULLUP);
}