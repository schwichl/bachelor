#include "CPanTilt.hpp"

#include "robot/CRobot.hpp"

#include "hardware.hpp"
#include "shared_prefs.hpp"

#include "CLog.hpp"

/**
 * 
 */
bool CPanTilt::isMoving(void) const
{
  return (this->m_svTilt.isMoving() || this->m_svPan.isMoving());
}

/**
 * 
 */
CVector2f CPanTilt::getAngles(void) const
{
  return {
    (this->m_svTilt.getAngle() / k_flServoRatio) - k_flOffsetTilt,
    (this->m_svPan.getAngle() / k_flServoRatio) - k_flOffsetPan
  };
}

/**
 *
 */
CVector3f CPanTilt::getAxis(void) const
{
  const CVector2f& vecAngles{ this->getAngles() };
  return CVector3f{ vecAngles[0], vecAngles[1], 0.0f }.toAxis();
}

/**
 * 
 */
void CPanTilt::setTilt(float flAngle)
{
  if ((flAngle < shared_prefs::k_flMinTilt) || (flAngle > shared_prefs::k_flMaxTilt))
  {
    slog << "refusing out-of-reach tilt: " << flAngle << '\n';
    return;
  }

  this->m_svTilt.setAngle((flAngle + k_flOffsetTilt) * k_flServoRatio);

  CRobot::instance()->shake(
      (this->m_svTilt.getShakeEndTime() - millis()) + 1000);
}

/**
 * 
 */
void CPanTilt::setPan(float flAngle)
{
  if ((flAngle < shared_prefs::k_flMinPan) || (flAngle > shared_prefs::k_flMaxPan))
  {
    slog << "refusing out-of-reach pan: " << flAngle << '\n';
    return;
  }

  this->m_svPan.setAngle((flAngle + k_flOffsetPan) * k_flServoRatio);

  CRobot::instance()->shake(
      (this->m_svPan.getShakeEndTime() - millis()) + 1000);
}

/**
 * 
 */
void CPanTilt::setAngles(const CVector2f& vecAngles)
{
  this->setTilt(vecAngles[0]);
  this->setPan(vecAngles[1]);
}

/**
 * 
 */
void CPanTilt::home(void)
{
  this->setAngles({ 0.0f, 0.0f });
}

/**
 * 
 */
CPanTilt::CPanTilt(const CPanTilt::SPins& pins)
{
  this->m_svTilt.setup(pins.iTilt);
  this->m_svPan.setup(pins.iPan);

  hardware::pServoTilt = &this->m_svTilt;
  hardware::pServoPan = &this->m_svPan;

  this->setAngles({ 0.0f, 0.0f });
}

/**
 * 
 */
CPanTilt::~CPanTilt(void)
{
}