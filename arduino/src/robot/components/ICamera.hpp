#if !defined(HPProbot_components_ICamera)
#define HPProbot_components_ICamera

#include "CVector.hpp"

class ICamera
{
public:

  /**
   * @return How far the camera reaches out from the pivot in millimeters.
   */
  virtual float getLength(void) const = 0;

  /**
   * Captures an image.
   */
  virtual void takePicture(void) = 0;

  /**
   * @return True if an image is currently being taken.
   */
  virtual bool isCapturing(void) const = 0;
};

#endif