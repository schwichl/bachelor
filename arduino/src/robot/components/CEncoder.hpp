#if !defined(HPPCEncoder)
#define HPPCEncoder

class CGearMotor;

/**
 * We're assuming that only 1 instance exists at a time. There cannot be more,
 * because an interrupt is required.
 */
class CEncoder
{
  friend void interruptOutA(void);

public:

  struct SPins
  {
    int iChannelA{};
    // Channel B is unused.
  };

public:

  /**
   * We're only capturing 1 edge of 1 channel.
   */
  static constexpr int k_iCountsPerRevolution{ 12 };

private:
  /**
   * Pins.
   */
  SPins m_pins{};

  /**
   * Counts of rising edges on channel A.
   */
  volatile long m_iCounts{ 0 };

  /**
   * Motor the encoder is attached to.
   */
  const CGearMotor *m_pMotor{};

public:

  /**
   * @return Revolutions.
   * Note that the encoder is attached to the motor's internal shaft. Multiply
   * with the transmission of the motor to get the counts on the output shaft.
   */
  float getRevolutions(void) const;

  /**
   * 
   */
  void setup(const SPins &pins, const CGearMotor *pMotor);

public:

  /**
   * 
   */
  CEncoder(void);
};

#endif