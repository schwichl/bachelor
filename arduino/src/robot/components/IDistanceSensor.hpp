#if !defined(HPPIDistanceSensor)
#define HPPIDistanceSensor

class IDistanceSensor
{
public:

  /**
   * @return 0.0 on error, otherwise the distance in millimeters.
   */
  virtual float getDistance(void) = 0;

  /**
   * 
   */
  virtual void setup(void) = 0;
};

#endif