#include "CServo.hpp"

#include "CLog.hpp"

#include <Arduino.h>

/**
 * 
 */
bool CServo::isMoving(void) const
{
  return (this->m_uiTurnEndTime >= millis());
}

/**
 * 
 */
unsigned long CServo::getShakeEndTime(void) const
{
  return this->m_uiTurnEndTime;
}

/**
 * Linear interpolation.
 * A curve would be better, because the servo slows down before it reaches the
 * target.
 * This function isn't used for anything sensitive, so it doesn't matter.
 */
float CServo::getAngle(void) const
{
  if (this->isMoving())
  {
    float flProgress{
      static_cast<float>((millis() - this->m_uiTurnStartTime) /
                         (this->m_uiTurnEndTime - this->m_uiTurnStartTime))
    };

    float flAngularProcess{
      (this->m_flAngle - this->m_flStartAngle) * flProgress
    };

    return (this->m_flStartAngle + flAngularProcess);
  }
  else
  {
    return this->m_flAngle;
  }
}

/**
 * 
 */
void CServo::setAngle(float flAngle)
{
  // slog << k_flMillisecondsPerRadian << " CServo::setAngle(" << flAngle << ")\n";

  // 625 top level
  // 1410 bottom level
  // 2175 top level

  // PWM distances are inequal. If this is noticeable during scan, we need a
  // function that maps an angle or desired PWM to the actual PWM.
  // To find this mapping, more measurements than top and bottom are required.

  // 2175 - 625 = 1550 PWM per (7 * 2pi)

  static constexpr float k_flPi{ acos(-1.0f) };

  int iPWM{ k_iPWMNeutral + static_cast<int>((flAngle / (2.0f * k_flPi)) * k_flPWMPerRotation) };

  // iPWM = static_cast<int>(flAngle);

  if ((iPWM < k_iPWMMin) || (iPWM > k_iPWMMax))
  {
    slog << "bad pwm " << iPWM << " on pin " << this->m_iPin << '\n';

    if (true)
    {
      return;
    }
    else
    {
      iPWM = max(k_iPWMMin, min(iPWM, k_iPWMMax));
    }
  }

  // slog << "angle " << flAngle << " pwm " << iPWM << " on pin " << this->m_iPin << '\n';

  this->m_servo.writeMicroseconds(iPWM);

  if (!this->m_servo.attached())
  {
    this->m_servo.attach(this->m_iPin, k_iPWMMin, k_iPWMMax);
  }

  float flAngleDiff{ abs(this->m_flAngle - flAngle) };

  this->m_flAngle = flAngle;

  this->m_uiTurnStartTime = millis();
  this->m_uiTurnEndTime = (this->m_uiTurnStartTime +
                           (flAngleDiff * k_flMillisecondsPerRadian));

  // slog << "turning " << flAngleDiff << " rad. takes " << (this->m_uiTurnEndTime - this->m_uiTurnStartTime) << "ms\n";
}

/**
 * 
 */
void CServo::tick(void)
{
}

/**
 * Sets up pin modes.
 */
void CServo::setup(int iPin)
{
  this->m_iPin = iPin;
}