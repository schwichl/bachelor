#if !defined(HPProbot_components_CServo)
#define HPProbot_components_CServo

#include "CActiveComponent.hpp"
#include "IMovable.hpp"

#include "cmath"

#include <Servo.h>

/**
 * HS-785HB
 * +PWM -> CCW
 */
class CServo
    : public CActiveComponent,
      public IMovable
{
private:
  /**
   * These aren't PWM values, they're a servo PWM PPM mix.
   */
  static constexpr int k_iPWMMin{ 600 };
  static constexpr int k_iPWMMax{ 2400 };

  /**
   * Travel in radians from {@link k_iPWMMin} to {@link k_iPWMMax}.
   */
  static constexpr float k_flMaxTravel{ 49.323f };

  /**
   * 
   */
  static constexpr float k_flPWMPerRotation{ 1550.0f / 7.0f };

  /**
   * Used to interpolate the position and decide if we're still moving.
   * See the datasheet for reference (Note: Typo in datasheet: 1.38sec/60 should
   * be 1.38sec/360).
   */
  static constexpr float k_flMillisecondsPerRadian{
    500.0f // Measured and adjusted for best results
  };

  /**
   * Neutral position.
   */
  static constexpr int k_iPWMNeutral{ k_iPWMMin + (k_iPWMMax - k_iPWMMin) / 2 };

private:
  /**
   * 
   */
  Servo m_servo{};

  /**
   * PWM pin.
   */
  int m_iPin{};

  /**
   * Current, or currently targeted, angle.
   */
  float m_flAngle{ 0.0f };

  /**
   * Angle at which the servo last started moving.
   */
  float m_flStartAngle{ 0.0f };

  /**
   * Time in milliseconds at which the servo started moving.
   */
  unsigned long m_uiTurnStartTime{ 0 };

  /**
   * Predicted time in milliseconds at which the servo stops moving.
   */
  unsigned long m_uiTurnEndTime{ 0 };

public:
  /**
   * @override
   */
  virtual bool isMoving(void) const override;

  /**
   * @return Time at which the servo will stop moving.
   */
  unsigned long getShakeEndTime(void) const;

  /**
   * @return Current angle in radians. Interpolated while moving.
   */
  float getAngle(void) const;

  /**
   * @param flAngle Angle in radians. + -> CW
   */
  void setAngle(float flAngle);

  /**
   * 
   */
  virtual void tick(void) override;

  /**
   * Sets up pin modes.
   */
  void setup(int iPin);
};

#endif