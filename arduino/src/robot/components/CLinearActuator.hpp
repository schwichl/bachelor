#if !defined(HPProbot_components_CLinearActuator)
#define HPProbot_components_CLinearActuator

#include "CActiveComponent.hpp"
#include "ILinearActuator.hpp"

class CMotor;

/**
 * Motorized linear actuator with a lead screw.
 */
class CLinearActuator
    : public CActiveComponent,
      public ILinearActuator

{
private:
  /**
   * 
   */
  CMotor* m_pMotor{};

  /**
   * True if the actuator was moving in the last tick. Used to detect when the
   * actuator stops.
   */
  bool m_bWasMoving{ false };

protected:
  /**
   * Maximum distance in millimeters from home.
   */
  float m_flTravelLength{};

  /**
   * Distance travelled in millimeters per full revolution of the motor.
   */
  float m_flDistancePerRevolution{};

  /**
   * Home pin. Digital input.
   */
  int m_iPinHome{};

  /**
   * Previous state of the end switch.
   */
  bool m_bOldHomeState{ false };

protected:
  /**
   * Fired when the actuator stops moving.
   */
  virtual void onStop(void) = 0;

  /**
   * Fired when the end switch gets pressed.
   */
  virtual void onEndSwitchPressed(void) = 0;

  /**
   * Fired when the end switch gets released.
   */
  virtual void onEndSwitchReleased(void) = 0;

  /**
   * Check the home button's state and fires event handlers.
   * @return True if the end switch is pressed.
   */
  bool processHomeButton(void);

public:
  /**
   * @override
   */
  virtual void tick(void);

  /**
   * @override
   */
  virtual bool isMoving(void) const override;

  /**
   * @override
   */
  virtual float getRange(void) const override;

  /**
   * @override
   */
  virtual float moveToRanged(float flPosition) override;

  /**
   * 
   */
  CLinearActuator(
      CMotor* pMotor,
      float flTravelLength,
      float flLeadScrewPitch,
      int iLeadScrewThreadStarts,
      int iPinHome);

  /**
   * 
   */
  CLinearActuator(void) = delete;
};
#endif