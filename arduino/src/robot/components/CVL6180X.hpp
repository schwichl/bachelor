#if !defined(HPProbot_components_CVL6180X)
#define HPProbot_components_CVL6180X

#include "IDistanceSensor.hpp"

#include <VL6180X.h>

class CVL6180X : public IDistanceSensor
{
private:

  /**
   * Maximum measurable distance in mm in 1x scaling.
   */
  static constexpr int k_iMaximumMeasurableDistance{ 197 };

  /**
   * Time in microseconds allowed per measurement. This is not supported by the
   * Arduino library, so it will be done by taking multiple measurements until
   * the budget is about to be reached.
   */
  static constexpr uint32_t k_uiMeasurementTimingBudget{ 200000 };

  /**
   * Base scaling factor. It's used to capture a rough measurement and will be
   * decreased for further readings during the same call.
   */
  static constexpr int k_iBaseScaling{ 3 };
  
  /**
   * Maximum distance in mm in 1x scaling.
   */
  static constexpr uint16_t k_uiDMAX{ 255 };
  static constexpr uint16_t k_uiDMAXScaled{ 255 * k_iBaseScaling };

public:
  /**
   * Only used for connection tests.
   * See datasheet.
   */
  static constexpr int k_iI2CAddress{ 0b0101001 };

private:
  /**
   * 
   */
  VL6180X m_sensor{};

public:
  /**
   * @override
   */
  virtual float getDistance(void) override;

  /**
   * @override
   */
  virtual void setup(void) override;
};

#endif