#include "CGearMotor.hpp"

#include "CEncoder.hpp"

#include "CLog.hpp"

#include "power.hpp"

// Gear motor wires
// https://www.pololu.com/product/3261

/**
 * 
 */
float CGearMotor::getCurrent(void) const
{
  int iRaw{ analogRead(this->m_pins.iCS) };
  // 1024 is the analog resolution.
  // 5 is operating voltage.
  float flVoltage{ (iRaw / 1024.0f) * 5.0f };

  float flCurrent{ flVoltage / k_flAmpsPerVolt };

  return flCurrent;
}

/**
 * Based on Pololu's dual driver shield library.
 * {@link https://github.com/pololu/dual-vnh5019-motor-shield/blob/master/DualVNH5019MotorShield.cpp}
 */
void CGearMotor::startSpin(bool bReverse)
{
  float flRPS{ this->m_flRPS };

  this->m_iDirection = (bReverse ? -1 : 1);

  if (bReverse)
  {
    flRPS *= -1.0f;
  }

  if (flRPS > k_flRPSMax)
  {
    flRPS = k_flRPSMax;
  }

  if (flRPS < k_flRPSMin)
  {
    // We might want to remove this to hold a load.
    flRPS = k_flRPSMin;
  }

  int iAnalogSpeed{ static_cast<int>((flRPS / k_flRPSMax) * 255.0f) };

  analogWrite(this->m_pins.iPWM, iAnalogSpeed);

  if (bReverse)
  {
    digitalWrite(this->m_pins.iInA, LOW);
    digitalWrite(this->m_pins.iInB, HIGH);
  }
  else
  {
    digitalWrite(this->m_pins.iInA, HIGH);
    digitalWrite(this->m_pins.iInB, LOW);
  }
}

/**
 * 
 */
void CGearMotor::stopSpin(void)
{
  this->m_bFreeSpinning = false;
  this->m_iDirection = 0;

  digitalWrite(this->m_pins.iInA, LOW);
  digitalWrite(this->m_pins.iInB, LOW);
  analogWrite(this->m_pins.iPWM, 255);
}

/**
 * 
 */
float CGearMotor::getRevolutions(void) const
{
  return (this->m_pEncoder->getRevolutions() / k_flTransmission);
}

/**
 * 
 */
bool CGearMotor::isMoving(void) const
{
  return (this->m_iDirection != 0);
}

/**
 * 
 */
void CGearMotor::spin(float flRevolutions)
{
  this->m_bFreeSpinning = false;
  this->m_flTargetRevolutions = (this->getRevolutions() + flRevolutions);

  this->startSpin(flRevolutions < 0.0f);
}

/**
 * 
 */
void CGearMotor::spin(CMotor::EDirection iDirection)
{
  this->m_bFreeSpinning = true;
  this->startSpin(iDirection == CMotor::EDirection::kCounterClockwise);
}

/**
 * 
 */
void CGearMotor::stop(void)
{
  this->stopSpin();
}

/**
 * 
 */
void CGearMotor::setSpeed(float flRPS)
{
  this->m_flRPS = flRPS;
}

/**
 * 
 */
void CGearMotor::tick(void)
{
  if (power::g_bDie)
  {
    this->stop();
    return;
  }

  if (true)
  { // arduino
    float flCurrent{ this->getCurrent() };

    if (flCurrent > k_flMaxCurrent)
    {
      auto uiTime{ millis() };

      if (this->m_uiOvercurrentStart == 0)
      {
        this->m_uiOvercurrentStart = uiTime;
      }

      auto uiOvercurrentDuration{ uiTime - this->m_uiOvercurrentStart };

      if ((k_uiOvercurrentThreshold == 0) ||
          (uiOvercurrentDuration > k_uiOvercurrentThreshold))
      {
        this->stopSpin();

        // Should we power::g_bDie here?
        slog << "gear motor overcurrent protection at " << flCurrent
             << "A after " << uiOvercurrentDuration << "ms\n";

        return;
      }
    }
    else
    {
      this->m_uiOvercurrentStart = 0;
    }
  }
  else
  {
    static bool s_bDoOnce{ true };

    if (s_bDoOnce)
    {
      s_bDoOnce = false;
      slog << "!! overcurrent protection disabled !!\n";
    }
  }

  if (!this->m_bFreeSpinning)
  {
    float flOutputRevolutions{ this->getRevolutions() };
    float flRemainingRevolutions{ this->m_flTargetRevolutions - flOutputRevolutions };

    flRemainingRevolutions *= this->m_iDirection;

    if (flRemainingRevolutions <= 0.0f)
    {
      this->stopSpin();
    }
  }
}

/**
 * 
 */
void CGearMotor::setup(const CGearMotor::SPins& pins, const CEncoder* pEncoder)
{
  this->m_pins = pins;
  this->m_pEncoder = pEncoder;

  pinMode(this->m_pins.iInA, OUTPUT);
  pinMode(this->m_pins.iInB, OUTPUT);
  pinMode(this->m_pins.iPWM, OUTPUT);
  pinMode(this->m_pins.iCS, INPUT);
}