#include "CStepperMotor.hpp"

#include "CLog.hpp"

#include "Arduino.h"

/**
 * 
 */
void CStepperMotor::setPattern(int iIndex)
{
  static constexpr int k_arrIControlPatterns[k_iControlPatterns][k_iPins]{
    { HIGH, LOW, HIGH, LOW },
    { LOW, HIGH, HIGH, LOW },
    { LOW, HIGH, LOW, HIGH },
    { HIGH, LOW, LOW, HIGH }
  };

  // @iIndex should only every be out of bounds by 1. We're allowing all offsets
  // anyway.

  while (iIndex < 0)
  {
    iIndex += k_iControlPatterns;
  }

  iIndex = (iIndex % k_iControlPatterns);

  for (int i{ 0 }; i < k_iPins; ++i)
  {
    digitalWrite(this->m_arrIPins[i], k_arrIControlPatterns[iIndex][i]);
  }

  this->m_iCurrentPattern = iIndex;
}

/**
 * 
 */
void CStepperMotor::tick(void)
{
  unsigned long ulTime{ micros() };

  if (ulTime >= this->m_uiNextStepTime)
  {
    if (this->m_bDirectionLocked)
    {
      if ((this->m_iDirectionLock == CMotor::EDirection::kClockwise) != (this->m_iRemainingSteps > 0))
      {
        // We're trying to move in a locked direction.
        this->stop();
        return;
      }
    }

    if (this->m_iRemainingSteps < 0)
    {
      this->setPattern(this->m_iCurrentPattern - 1);

      if (!this->m_bFreeSpinning)
      {
        ++this->m_iRemainingSteps;
      }
    }
    else if (this->m_iRemainingSteps > 0)
    {
      this->setPattern(this->m_iCurrentPattern + 1);

      if (!this->m_bFreeSpinning)
      {
        --this->m_iRemainingSteps;
      }
    }

    if (this->m_iRemainingSteps != 0)
    {
      this->m_uiNextStepTime = (ulTime + this->m_uiStepDelay);

      if (!this->m_bFreeSpinning)
      {
        // Only smooth the movement when we're not free spinning.
        // {@link m_iRemainingSteps} doesn't get updated while free spinning.

        float flCurve{ this->m_accelerationCurve.get(
            this->m_iRequestedSteps,
            0,
            this->m_iRemainingSteps) };

        auto uiSmoothDelay{ static_cast<unsigned long>((1.0f - flCurve) * (k_uiMinStepDelay * 2)) };

        this->m_uiNextStepTime += uiSmoothDelay;
      }
    }

    // Don't update {@link m_uiNextStepTime} here. Doing so will cause motors
    // to idle for up to @{k_uiStepSpeed} after receiving a step command.
  }
}

/**
 * 
 */
bool CStepperMotor::isMoving(void) const
{
  return (this->m_iRemainingSteps != 0);
}

/**
 * 
 */
void CStepperMotor::step(int iStepCount)
{
  this->m_iRemainingSteps += iStepCount;
  this->m_iRequestedSteps = this->m_iRemainingSteps;
}

/**
 * 
 */
void CStepperMotor::spin(float flRevolutions)
{
  this->m_bFreeSpinning = false;
  this->step(flRevolutions * k_iStepsPerRevolution);
}

/**
 * 
 */
void CStepperMotor::spin(CMotor::EDirection iDirection)
{
  this->m_bFreeSpinning = true;

  if (iDirection == CMotor::EDirection::kClockwise)
  {
    this->m_iRemainingSteps = 1;
  }
  else
  {
    this->m_iRemainingSteps = -1;
  }
}

/**
 * 
 */
void CStepperMotor::stop(void)
{
  this->m_bFreeSpinning = false;
  this->m_iRemainingSteps = 0;
}

/**
 * 
 */
void CStepperMotor::setSpeed(float flRPS)
{
  static constexpr float k_flMaxRPS{ (1000000.0f / k_uiMinStepDelay) / k_iStepsPerRevolution };

  if (flRPS > k_flMaxRPS)
  {
    flRPS = k_flMaxRPS;
  }

  this->m_uiStepDelay = static_cast<unsigned long>(1000000.0f / (flRPS * k_iStepsPerRevolution));
}

/**
 * 
 */
void CStepperMotor::setAccelerationPeriod(float flAccelerationPeriod)
{
  this->m_accelerationCurve.setAccelerationPeriod(
      flAccelerationPeriod * k_iStepsPerRevolution);
}

/**
 * 
 */
void CStepperMotor::setup(const CStepperMotor::SPins& pins)
{
  this->m_arrIPins[0] = pins.iCoilA1;
  this->m_arrIPins[1] = pins.iCoilA2;
  this->m_arrIPins[2] = pins.iCoilB1;
  this->m_arrIPins[3] = pins.iCoilB2;

  for (int iPin : this->m_arrIPins)
  {
    pinMode(iPin, OUTPUT);
  }

  this->setPattern(0);
}
/**
 * 
 */
CStepperMotor::CStepperMotor(void)
{
}