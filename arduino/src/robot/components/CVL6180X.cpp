#include "CVL6180X.hpp"

#include "CLog.hpp"

#include <Wire.h>

/**
 * We're using milliseconds the prevent wrap-arounds.
 */
float CVL6180X::getDistance(void)
{
  /**
   * Time in milliseconds for 1 single-shot measurement.
   */
  static constexpr unsigned long uiTimePerMeasurement{ 12 };

  unsigned long uiBudgetEnd{
    (millis() + (k_uiMeasurementTimingBudget / 1000)) -
    uiTimePerMeasurement
  };

  this->m_sensor.setScaling(k_iBaseScaling);

  auto uiLongRange{ this->m_sensor.readRangeSingleMillimeters() };

  if (this->m_sensor.timeoutOccurred())
  {
    slog << "VL6180X timed out\n";
    return static_cast<float>(k_uiDMAXScaled);
  }

  if (uiLongRange == k_uiDMAXScaled)
  {
    // No target detected, there's no point in trying any futher.
    return static_cast<float>(uiLongRange);
  }

  // Best scaling that still sees the target.
  int iLowestScaling{ static_cast<int>((uiLongRange / k_iMaximumMeasurableDistance) + 1) };

  // slog << "best scaling: " << iLowestScaling << '\n';

  this->m_sensor.setScaling(iLowestScaling);

  int32_t iSum{ (iLowestScaling == k_iBaseScaling) ? static_cast<int32_t>(uiLongRange) : 0 };
  int iReadings{ (iLowestScaling == k_iBaseScaling) ? 1 : 0 };

  while (millis() < uiBudgetEnd)
  {
    auto uiDistance{ this->m_sensor.readRangeSingleMillimeters() };

    if (this->m_sensor.timeoutOccurred())
    {
      slog << "VL6180X timed out\n";
      return static_cast<float>(k_uiDMAXScaled);
    }

    // Ignore out-of-range readings
    if (uiDistance != k_uiDMAXScaled)
    {
      iSum += static_cast<int32_t>(uiDistance);
      ++iReadings;
    }
  }

  return (static_cast<float>(iSum) / iReadings);
}

/**
 * 
 */
void CVL6180X::setup(void)
{
  Wire.begin();

  this->m_sensor.init();

  this->m_sensor.configureDefault();

  this->m_sensor.setTimeout((k_uiMeasurementTimingBudget / 1000) + 500);

  // Scaling is set by {@link getDistance}

  // Not supported by Pololu's VL6180X library
  // this->m_sensor.setMeasurementTimingBudget(k_uiMeasurementTimingBudget);
}