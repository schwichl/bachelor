#if !defined(HPProbot_components_CDeadMansSwitch)
#define HPProbot_components_CDeadMansSwitch

#include "CActiveComponent.hpp"
class C
{
public:
    constexpr void fn(){}
};
class CDeadMansSwitch : public CActiveComponent
{
private:
  /**
   * INPUT_PULLUP
   */
  int m_iPin{};

  /**
   * State of the DMS in the previous tick.
   */
  bool m_bPreviousState{ false };

private:
  /**
   * Fired when the man dies.
   */
  void onDisconnect(void);

public:
  /**
   * @override
   */
  virtual void tick(void) override;

  /**
   * @return Cached state.
   */
  bool isAlive(void) const;

public:
  /**
   * 
   */
  CDeadMansSwitch(int iPin);
};

#endif