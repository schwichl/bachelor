#if !defined(HPProbot_components_IPanTilt)
#define HPProbot_components_IPanTilt

#include "CVector.hpp"

#include "IMovable.hpp"

/**
 * Angle format is pitch/yaw or tilt/pan
 */
class IPanTilt : public IMovable
{
public:

  /**
   * @return Angles in radians. [0] tilt, [1] pan.
   */
  virtual CVector2f getAngles(void) const = 0;

  /**
   * @return View axis.
   */
  virtual CVector3f getAxis(void) const = 0;

public:

  /**
   * @param flAngle Angle in radians. 0 is horizontal.
   */
  virtual void setTilt(float flAngle);

  /**
   * @param flAngle Angle in radians. 0 is facing down positive Z.
   */
  virtual void setPan(float flAngle);

  /**
   * @param vecAngles [0] Tilt in radians [1] Pan in radians
   */
  virtual void setAngles(const CVector2f &vecAngles) = 0;

  /**
   * Homes the servos.
   */
  virtual void home(void) = 0;

  /**
   * 
   */
  // virtual ~IPanTilt(void) = 0;
};

#endif