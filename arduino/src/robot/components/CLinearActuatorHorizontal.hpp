#if !defined(HPPCLinearActuatorHorizontal)
#define HPPCLinearActuatorHorizontal

#include "CLinearActuator.hpp"
#include "CStepperMotor.hpp"

class CLinearActuatorHorizontal : public CLinearActuator
{
  // To allow {@link CRobot} to set the debugging hardware.
  friend class CRobot;

public:
  struct SPins
  {
    CStepperMotor::SPins stepper{};
    int iEndSwitch{};
  };

private:
  /**
   * Lead screw pitch in mm.
   */
  static constexpr float k_flLeadScrewPitch{ 2.0f };

  /**
   * Number of thread starts in the lead screw.
   */
  static constexpr int k_iLeadScrewThreadStarts{ 4 };

  /**
   * Milliseconds until the robot stops shaking after this actuator stops
   * moving.
   */
  static constexpr unsigned long k_uiShakeTime{ 10000 };

private:
  /**
   * The stepper motor powering the actuator.
   */
  CStepperMotor m_motor{};

  /**
   * Current position in mm.
   */
  float m_flPosition{ 0.0f };

  /**
   * True if we're returning from a pressed end switch to home.
   */
  bool m_bSeekingHome{ false };


protected:
  /**
   * @override
   */
  virtual void onStop(void) override;

  /**
   * @override
   */
  virtual void onEndSwitchPressed(void) override;

  /**
   * @override
   */
  virtual void onEndSwitchReleased(void) override;

public:
  /**
   * @override
   */
  virtual bool isMoving(void) const override;

  /**
   * @override
   */
  virtual float getRangedPosition(void) const override;

public:
  /**
   * @override
   */
  virtual float moveToRanged(float flPosition) override;

  /**
   * @override
   */
  virtual void home(void) override;

  /**
   * @override
   */
  virtual void tick(void) override;

  /**
   * @param flAccelerationPeriod Acceleration period in millimeters.
   */
  void setAccelerationPeriod(float flAccelerationPeriod);

public:
  /**
   * Parameters are forwarded to the setup routine of the stepper motor. 
   */
  CLinearActuatorHorizontal(const SPins& pins, float flMaxTravelLength);

  /**
   * 
   */
  virtual ~CLinearActuatorHorizontal(void){};
};

#endif