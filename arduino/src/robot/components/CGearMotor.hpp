#if !defined(HPPCGearMotor)
#define HPPCGearMotor

#include "CActiveComponent.hpp"
#include "CMotor.hpp"

class CEncoder;

/**
 * VNH5019A driver
 */
class CGearMotor
    : public CActiveComponent,
      public CMotor
{
public:
  struct SPins
  {
    int iInA{};
    int iInB{};
    int iPWM{};
    int iCS{};
  };

private:
  /**
   * Number of motor revolutions for 1 output revolution.
   * {@link https://www.exp-tech.de/motoren/dc-getriebemotoren/7478/34-1-metal-gearmotor-25dx52l-mm-hp-12v-with-48-cpr-encoder?c=1198}
   */
  static constexpr float k_flTransmission{ 34.014f };

  /**
   * RPS at 6V.
   */
  static constexpr float k_flRPSMin{ 145.0f / 60.0f };

  /**
   * RPS at 12V.
   */
  static constexpr float k_flRPSMax{ 290.0f / 60.0f };

  /**
   * How many amps the motor is using per volt output by CS.
   * {@link https://www.pololu.com/product/1451}
   */
  static constexpr float k_flAmpsPerVolt{ 0.140f };

  /**
   * Max amps. Stall is 5.6A.
   * {@link https://www.exp-tech.de/motoren/dc-getriebemotoren/7478/34-1-metal-gearmotor-25dx52l-mm-hp-12v-with-48-cpr-encoder?c=1198}
   */
  static constexpr float k_flMaxCurrent{ 5.0f };

  /**
   * How long (in milliseconds) an overcurrent is tolerated.
   */
  static constexpr unsigned long k_uiOvercurrentThreshold{ 100 };

private:
  /**
   * 
   */
  const CEncoder* m_pEncoder{ nullptr };

  /**
   * Pins.
   */
  SPins m_pins{};

  /**
   * 
   */
  float m_flRPS{ k_flRPSMin };

  /**
   * 
   */
  bool m_bFreeSpinning{ false };

  /**
   * Number of revolutions at which we'll stop spinning.
   * Ignored if {@link m_bFreeSpinning} is true.
   */
  float m_flTargetRevolutions{ 0.0f };

  /**
   * For the encoder.
   * 1 Clockwise.
   * 0 Not moving.
   * -1 Counter-clockwise.
   */
  int m_iDirection{ 1 };

  /**
   * Time in milliseconds at which the overcurrent started.
   * 0 if there is no overcurrent right now.
   */
  unsigned long m_uiOvercurrentStart{ 0 };

private:
  /**
   * @return Current current.
   */
  float getCurrent(void) const;

  /**
   * Starts spinning the motor.
   */
  void startSpin(bool bReverse);

  /**
   * Stops spinning.
   */
  void stopSpin(void);

public:
  /**
   * @return Revolutions of the output shaft.
   */
  float getRevolutions(void) const;

  /**
   * 
   */
  virtual bool isMoving(void) const override;

  /**
   * Inlined for the encoder.
   * @return 1: Clockwise. -1: Counter-clockwise.
   */
  inline int getDirection(void) const
  {
    return this->m_iDirection;
  }

  /**
   * Non-queueing.
   */
  virtual void spin(float flRevolutions) override;

  /**
   * 
   */
  virtual void spin(CMotor::EDirection iDirection) override;

  /**
   * 
   */
  virtual void stop(void) override;

  /**
   * @param flRPS Range [-{@link k_flRPSMax}, {@link k_flRPSMax}].
   * Deadzone (-{@link k_flRPSMin}, {@link k_flRPSMin}).
   */
  void setSpeed(float flRPS);

  /**
   * 
   */
  virtual void tick(void) override;

  /**
   * Sets up pin modes.
   */
  void setup(const SPins& pins, const CEncoder* pEncoder);
};

#endif