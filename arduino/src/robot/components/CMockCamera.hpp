#if !defined(HPProbot_component_CMockCamera)
#define HPProbot_component_CMockCamera

#include "ICamera.hpp"

#include "shared_prefs.hpp"

class CMockCamera : public ICamera
{
private:
public:
  virtual CVector3f getPivot(void) const override
  {
    return {};
  }

  /**
   * @return How far the camera reaches out from the pivot in millimeters.
   */
  virtual float getLength(void) const override
  {
    return shared_prefs::k_flCameraLength;
  }

  /**
   * Captures an image.
   * Blocking.
   */
  virtual void takePicture(void) override
  {

  }

  /**
   * 
   */
  virtual ~CMockCamera(void)
  {

  }
};

#endif