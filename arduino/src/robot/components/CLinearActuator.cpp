#include "CLinearActuator.hpp"

#include "CLog.hpp"
#include "CMotor.hpp"

#include "Arduino.h"

/**
 * 
 */
bool CLinearActuator::processHomeButton(void)
{
  bool bDown{ digitalRead(this->m_iPinHome) == LOW };

  if (bDown)
  {
    if (!this->m_bOldHomeState)
    {
      this->onEndSwitchPressed();
    }
  }
  else
  {
    if (this->m_bOldHomeState)
    {
      this->onEndSwitchReleased();
    }
  }

  this->m_bOldHomeState = bDown;

  return bDown;
}

/**
 * 
 */
void CLinearActuator::tick(void)
{
  this->processHomeButton();

  bool bIsMoving{ this->isMoving() };

  if (!bIsMoving && this->m_bWasMoving)
  {
    this->onStop();
  }

  this->m_bWasMoving = bIsMoving;
}

/**
 * 
 */
bool CLinearActuator::isMoving(void) const
{
  return this->m_pMotor->isMoving();
}

/**
 * 
 */
float CLinearActuator::getRange(void) const
{
  return this->m_flTravelLength;
}

/**
 * 
 */
float CLinearActuator::moveToRanged(float flPosition)
{
  bool bClamped{ true };
  float flOriginalPosition{ flPosition };

  if (flPosition < 0.0f)
  {
    flPosition = 0.0f;
  }
  else if (flPosition > this->m_flTravelLength)
  {
    flPosition = this->m_flTravelLength;
  }
  else
  {
    bClamped = false;
  }

  if (bClamped)
  {
    slog << "clamped linear actuator to " << flPosition << ". was "
         << flOriginalPosition << '\n';
  }

  float flOffset{ flPosition - this->getRangedPosition() };

  float flRevolutions{ flOffset / this->m_flDistancePerRevolution };
  
  this->m_pMotor->spin(flRevolutions);

  return flPosition;
}

/**
 * 
 */
CLinearActuator::CLinearActuator(
    CMotor* pMotor,
    float flTravelLength,
    float flLeadScrewPitch,
    int iLeadScrewThreadStarts,
    int iPinHome)
    : m_pMotor{ pMotor },
      m_flTravelLength{ flTravelLength },
      m_flDistancePerRevolution{ flLeadScrewPitch * iLeadScrewThreadStarts },
      m_iPinHome{ iPinHome }
{
  pinMode(this->m_iPinHome, INPUT_PULLUP);
}