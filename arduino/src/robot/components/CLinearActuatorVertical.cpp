#include "CLinearActuatorVertical.hpp"

#include "robot/CRobot.hpp"
#include "CLog.hpp"

/**
 * 
 */
void CLinearActuatorVertical::onStop(void)
{
  CRobot::instance()->shake(k_uiShakeTime);
}

/**
 * 
 */
void CLinearActuatorVertical::onEndSwitchPressed(void)
{
  this->m_motor.stop();
  this->m_motor.lockDirection(CMotor::EDirection::kClockwise);
  this->m_motor.spin(1.0f);
}

/**
 * 
 */
void CLinearActuatorVertical::onEndSwitchReleased(void)
{
  this->m_bSeekingHome = true;
}

/**
 * 
 */
float CLinearActuatorVertical::getRangedPosition(void) const
{
  return (
      (this->m_motor.getRevolutions() - this->m_flMotorRevolutionsHomeOffset) *
      this->m_flDistancePerRevolution);
}

/**
 * 
 */
void CLinearActuatorVertical::home(void)
{
  this->m_motor.spin(CMotor::EDirection::kCounterClockwise);
}

/**
 * 
 */
void CLinearActuatorVertical::tick(void)
{
  CLinearActuator::tick();

  if (this->m_bSeekingHome && !this->isMoving())
  {
    this->m_bSeekingHome = false;
    this->m_flMotorRevolutionsHomeOffset = this->m_motor.getRevolutions();
    this->m_motor.unlockDirection();
    
    CRobot::instance()->notifyHome(this);
  }
}

/**
 * 
 */
CLinearActuatorVertical::CLinearActuatorVertical(
    const CLinearActuatorVertical::SPins& pins,
    float flTravelLength)
    : CLinearActuator{
        &this->m_motor,
        flTravelLength,
        k_flLeadScrewPitch,
        k_iLeadScrewThreadStarts,
        pins.iEndSwitch
      }
{
  this->m_motor.setup(pins.gearMotor, &this->m_encoder);

  this->m_encoder.setup(pins.encoder, &this->m_motor);
}