#include "CMotor.hpp"

void CMotor::lockDirection(EDirection iDirection)
{
  this->m_bDirectionLocked = true;
  this->m_iDirectionLock = iDirection;
} 

/**
 * 
 */
void CMotor::unlockDirection(void)
{
  this->m_bDirectionLocked = false;
}