#include "CVL53L0X.hpp"

#include "CLog.hpp"

#include <Wire.h>

float CVL53L0X::getDistance(void)
{
  auto uiRange{ this->m_sensor.readRangeSingleMillimeters() };

  if (this->m_sensor.timeoutOccurred())
  {
    slog << "VL53L0X timed out\n";
    return 0.0f;
  }
  else
  {
    return static_cast<float>(uiRange);
  }
}

/**
 * 
 */
void CVL53L0X::setup(void)
{
  Wire.begin();

  this->m_sensor.init();

  this->m_sensor.setTimeout((k_uiMeasurementTimingBudget / 1000) + 500);

  this->m_sensor.setMeasurementTimingBudget(k_uiMeasurementTimingBudget);
}