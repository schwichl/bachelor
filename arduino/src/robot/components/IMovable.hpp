#if !defined(HPPcomponents_IMovable)
#define HPPcomponents_IMovable

/**
 * Motorized components.
 * This has nothing to do with move semantics.
 */
class IMovable
{
public:

  /**
   * @return True if the component is moving.
   */
  virtual bool isMoving(void) const = 0;
};

#endif