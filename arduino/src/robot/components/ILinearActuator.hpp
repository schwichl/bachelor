#if !defined(HPPILinearActuator)
#define HPPILinearActuator

#include "CVector.hpp"

#include "IMovable.hpp"

/**
 * 
 */
class ILinearActuator : public IMovable
{
public:
  /**
   * @return value in [0, range] in millimeters.
   */
  virtual float getRangedPosition(void) const = 0;

  /**
   * The lowest possible position is always 0.
   * @return The highest possible position in millimeters.
   */
  virtual float getRange(void) const = 0;

public:
  /**
   * @param flPosition in [0, range] in millimeters.
   * @return Potentially clamped position.
   */
  virtual float moveToRanged(float flPosition) = 0;

  /**
   * Seeks the home position.
   */
  virtual void home(void) = 0;

  /**
   * 
   */
  virtual ~ILinearActuator(void){};
};

#endif