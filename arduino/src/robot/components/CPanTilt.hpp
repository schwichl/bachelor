#if !defined(HPProbot_components_CPanTilt)
#define HPProbot_components_CPanTilt

#include "IPanTilt.hpp"

#include "CServo.hpp"

class CPanTilt : public IPanTilt
{
public:
  struct SPins
  {
    int iTilt{};
    int iPan{};
  };

private:
  /**
   * Turn a servo this many times for 1 real rotation.
   */
  static constexpr float k_flServoRatio{ 7.0f };

  /**
   * If the servos are not installed in a way that makes 90deg be facing
   * forward, these constants can be used to offset them to real 90deg.
   * Offsets to real 0 in radians.
   * Applied before applying gear ratio.
   * If either of these values exceeds 1.5, the servo needs to be reinstalled.
   */
  static constexpr float k_flOffsetTilt{ -0.025f };
  static constexpr float k_flOffsetPan{ 0.45f };

private:
  /**
   * 
   */
  CServo m_svTilt{};

  /**
   * 
   */
  CServo m_svPan{};

public:
  /**
   * @override
   */
  virtual bool isMoving(void) const override;

  /**
   * @override
   */
  virtual CVector2f getAngles(void) const override;

  /**
   * @override
   */
  virtual CVector3f getAxis(void) const override;

public:
  /**
   * @override
   */
  virtual void setTilt(float flAngle) override;

  /**
   * @override
   */
  virtual void setPan(float flAngle) override;

  /**
   * @override
   */
  virtual void setAngles(const CVector2f& vecAngles) override;

  /**
   * @override
   */
  virtual void home(void) override;

  /**
   * 
   */
  CPanTilt(const SPins& pins);

  /**
   * 
   */
  virtual ~CPanTilt(void);
};

#endif