#if !defined(HPProbot_component_CMockCamera)
#define HPProbot_component_CMockCamera

#include "ICamera.hpp"

#include "CActiveComponent.hpp"

#include "shared_prefs.hpp"

class CCamera
: public ICamera,
  public CActiveComponent
{
public:
  struct SPins
  {
    int iTrigger{};
  };

private:
  /**
   * Duration in milliseconds for which the trigger has to be HIGH.
   */
  static constexpr unsigned long k_uiTriggerDuration{ 500 };

private:
  /**
   * 
   */
  int m_iPinTrigger{};

  /**
   * True if we're currently taking a picture.
   */
  bool m_bCapturing{ false };

  /**
   * Time in microseconds at which the trigger is to be released.
   */
  unsigned long m_uiTriggerEndTime{};

public:
  /**
   * @override
   */
  virtual void tick(void) override;

  /**
   * @override
   */
  virtual float getLength(void) const override;

  /**
   * @override
   */
  virtual void takePicture(void) override;

  /**
   * @override
   */
  virtual bool isCapturing(void) const override;

public:
  /**
   * 
   */
  CCamera(const SPins& pins);

  /**
   * 
   */
  virtual ~CCamera(void) {}
};

#endif