#include "CLinearActuatorHorizontal.hpp"

#include "CLog.hpp"
#include "robot/CRobot.hpp"

/**
 * 
 */
void CLinearActuatorHorizontal::onStop(void)
{
  CRobot::instance()->shake(k_uiShakeTime);
}

/**
 * 
 */
void CLinearActuatorHorizontal::onEndSwitchPressed(void)
{
  // slog << "CLinearActuatorHorizontal::onEndSwitchPressed\n";
  this->m_motor.stop();
  this->m_motor.lockDirection(CMotor::EDirection::kClockwise);
  this->m_motor.spin(1.0f);
}

/**
 * 
 */
void CLinearActuatorHorizontal::onEndSwitchReleased(void)
{
  // slog << "CLinearActuatorHorizontal::onEndSwitchReleased\n";
  this->m_bSeekingHome = true;
}

/**
 * 
 */
bool CLinearActuatorHorizontal::isMoving(void) const
{
  return this->m_motor.isMoving();
}

/**
 * 
 */
float CLinearActuatorHorizontal::getRangedPosition(void) const
{
  return this->m_flPosition;
}

/**
 * 
 */
float CLinearActuatorHorizontal::moveToRanged(float flPosition)
{
  this->m_flPosition = CLinearActuator::moveToRanged(flPosition);

  return this->m_flPosition;
}

/**
 * 
 */
void CLinearActuatorHorizontal::home(void)
{
  this->m_motor.spin(CMotor::EDirection::kCounterClockwise);
}

/**
 * 
 */
void CLinearActuatorHorizontal::tick(void)
{
  CLinearActuator::tick();

  if (!this->isMoving())
  {
    if (this->m_bSeekingHome)
    {
      this->m_bSeekingHome = false;
      this->m_motor.unlockDirection();

      CRobot::instance()->notifyHome(this);
    }
  }
}

/**
 * 
 */
void CLinearActuatorHorizontal::setAccelerationPeriod(float flAccelerationPeriod)
{
  this->m_motor.setAccelerationPeriod(flAccelerationPeriod / (k_iLeadScrewThreadStarts * k_flLeadScrewPitch));
}

/**
 * 
 */
CLinearActuatorHorizontal::CLinearActuatorHorizontal(const CLinearActuatorHorizontal::SPins& pins, float flTravelLength)
    : CLinearActuator{
        &this->m_motor,
        flTravelLength,
        k_flLeadScrewPitch,
        k_iLeadScrewThreadStarts,
        pins.iEndSwitch
      }
{
  this->m_motor.setup(pins.stepper);
  this->setAccelerationPeriod(10.0f);
}