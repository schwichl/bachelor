#if !defined(HPPCtest_TestManager)
#define HPPCtest_TestManager

#include "CTest.hpp"

#include "CListDynamic.hpp"

class CTestManager
{
private:

  /**
   * 
   */
  CListDynamic<CTest> m_lstTests{};

public:

  /**
   * Runs all tests. Output is printed.
   * @return True if all tests were successful.
   */
  bool run(void) const;

public:

  /**
   * 
   */
  CTestManager(void);

public:

  /**
   * 
   */
  static CTestManager *instance(void);
};

#endif