#include "CTestManager.hpp"

#include "robot/components/CVL6180X.hpp"

#include "CLog.hpp"

#include "pinconfig.hpp"
#include "scanconfig.hpp"

#include <Wire.h>

/**
 * 
 */
bool CTestManager::run(void) const
{
  auto iTotal{ this->m_lstTests.size() };
  decltype(iTotal) iSuccess{ 0 };

  for (decltype(iTotal) i{ 0 }; i < iTotal; ++i)
  {
    const auto* pTest{ &this->m_lstTests[i] };

    if (pTest->run())
    {
      ++iSuccess;
    }
    else
    {
      slog << pTest->getName() << ": fail\n";
    }
  }

  slog << iSuccess << '/' << iTotal << " test(s) successful\n";

  return (iSuccess == iTotal);
}

/**
 * 
 */
CTestManager::CTestManager(void)
    : m_lstTests{
        { { "gearmotor_cs",
            []() {
              int iRead{ analogRead(pinconfig::k_linearActuatorY.gearMotor.iCS) };
              return (iRead <= 10);
            } },
          { "vl6180x",
            []() {
              if (!scanconfig::k_bEnableRangingSensor)
              {
                return true;
              }

              return (
                  (Wire.beginTransmission(CVL6180X::k_iI2CAddress),
                   Wire.endTransmission()) == 0);
            } } }
      }
{
}

/**
 * 
 */
CTestManager* CTestManager::instance(void)
{
  static CTestManager s_instance{};

  return &s_instance;
}