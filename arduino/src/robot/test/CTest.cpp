#include "CTest.hpp"

#include <Arduino.h>

/**
 * 
 */
const char* CTest::getName(void) const
{
  return this->m_szName;
}

/**
 * 
 */
bool CTest::run(void) const
{
  return this->m_fnRun();
}

/**
 * 
 */
CTest::CTest(const char* szName, bool (*fnRun)(void))
    : m_szName{ szName },
      m_fnRun{ fnRun }
{
}

/**
 * 
 */
static bool isShort(int iPinA, int iPinB)
{
  pinMode(iPinA, OUTPUT);
  pinMode(iPinB, INPUT);

  int arrIModes[]{ LOW, HIGH };

  for (int iMode : arrIModes)
  {
    digitalWrite(iPinA, iMode);

    if (digitalRead(iPinB) != iMode)
    {
      return false;
    }
  }

  return true;
}