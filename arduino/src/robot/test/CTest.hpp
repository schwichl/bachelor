#if !defined(HPPtest_CTest)
#define HPPtest_CTest

class CTest
{
private:
  /**
   * 
   */
  const char* m_szName{};

  /**
   * 
   */
  bool (*m_fnRun)(void){};

public:
  /**
   * 
   */
  const char* getName(void) const;

public:
  /**
   * Runs a blocking test.
   * @return True on success. False otherwise.
   */
  bool run(void) const;

public:
  /**
   * 
   */
  CTest(const char* szName, bool (*fnRun)(void));

  /**
   * 
   */
  CTest(void) = default;

public:
  /**
   * Tests if @iPinA is connected to @iPinB.
   */
  static bool isShort(int iPinA, int iPinB);
};

#endif