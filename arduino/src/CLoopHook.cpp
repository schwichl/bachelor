#include "CLoopHook.hpp"

CLoopHook g_loopHook{};

/**
 * 
 */
void CLoopHook::registerCallback(CActiveComponent *pComponent)
{
  this->m_arrCallbacks[this->m_iCallbacks] = pComponent;

  ++this->m_iCallbacks;
}

/**
 * 
 */
void CLoopHook::unregisterCallback(CActiveComponent *pComponent)
{
  int iFound{ -1 };

  for (int i{ 0 }; i < this->m_iCallbacks; ++i)
  {
    if (this->m_arrCallbacks[i] == pComponent)
    {
      iFound = i;
      break;
    }
  }

  if (iFound == -1)
  {
    slog << "tried removing a callback that doesn't exist\n";
  }
  else
  {
    for (int i{ iFound }; i < (this->m_iCallbacks - 1); ++i)
    {
      this->m_arrCallbacks[i] = this->m_arrCallbacks[i + 1];
    }

    --this->m_iCallbacks;
  }
}

/**
 * 
 */
void CLoopHook::tick(void)
{
  for (int i{ 0 }; i < this->m_iCallbacks; ++i)
  {
    this->m_arrCallbacks[i]->tick();
  }
}