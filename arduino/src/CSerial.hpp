#if !defined(HPPCSerial)
#define HPPCSerial

#include "protocol/protocol.hpp"

#include "Arduino.h"

class CSerial
{
public:
  enum EError
  {
    kSuccess,

    kPartialHeaderRead,
    kPartialPayloadRead,
    kDeviceDisconnected,

    kCount,
  };

public:

  static EError s_lastError;

public:
  /**
   * 
   */
  static bool readPacket(protocol::CPacket *pOut);

  /**
   * 
   */
  static void write(
      protocol::ECommand command,
      const char *arrData,
      int iLength);

  /**
   * A tailing line feed is added by the host.
   * Note that %f formatting is not supported. Use %s and `String(val).c_str()`
   * instead.
   */
  template <class... Args>
  static void printf(const char *szFormat, Args... args)
  {
    int iBufferLength{ snprintf(nullptr, 0, szFormat, args...) + 1 };

    char *buf{ new char[iBufferLength]{} };

    sprintf(buf, szFormat, args...);

    CSerial::write(protocol::ECommand::kPrint, buf, iBufferLength);

    delete[] buf;
  }

  /**
   * 
   */
  static void print(const char *szMessage);

  /**
   * 
   */
  template <class T>
  static void write(protocol::ECommand command, const T &data)
  {
    protocol::CPacket pckSend{
      command,
      data
    };

    // Incomplete writes are detected by the host as incomplete reads.
    Serial.write(pckSend.getRawHeader(), sizeof(protocol::CPacket::SHeader));
    Serial.write(pckSend.getRawPayload(), static_cast<size_t>(pckSend.getPayloadLength()));
  }

  template <class T>
  static T read(void)
  {
    return {};
  }
};

#endif