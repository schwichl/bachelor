// This file would greatly benefit from C++2a's designated initializers.
#if !defined(HPPpinconfig)
#define HPPpinconfig

#include "robot/components/CCamera.hpp"
#include "robot/components/CEncoder.hpp"
#include "robot/components/CGearMotor.hpp"
#include "robot/components/CLinearActuatorHorizontal.hpp"
#include "robot/components/CLinearActuatorVertical.hpp"
#include "robot/components/CPanTilt.hpp"
#include "robot/components/CStepperMotor.hpp"


/*

Board layout

Power lanes are at the bottom.
Indexes are bottom up.

[Power lanes]
1 12V
2 6V
3 GND
4 5V
Unused lane
[Pins]
1  SN754410NE(X) 2A
2  SN754410NE(X) 1A
3  SN754410NE(X) 3A
4  SN754410NE(X) 4A
5  SN754410NE(Z) 2A
6  SN754410NE(Z) 1A
7  SN754410NE(Z) 3A
8  SN754410NE(Z) 4A
9  VNH5019A INB
10 VNH5019A CS
11 VNH5019A PWM
12 VNH5019A INA
13 Encoder A
14 PWM Pan (Purple)
15 PWM Tilt (Yellow)
16 Home X
17 Home Z
18 Home Y
19 Relay

33 SDA
34 SCL

*/

// connect pan (purple) only
// turn t 0
// pan moves

/*

VL6180X

Red   5V
Black GND
White SDA
Gray  SCL

*/

namespace pinconfig
{
  constexpr int k_iRelay{ 12 };

  constexpr CCamera::SPins k_camera{
    13
  };

  constexpr CLinearActuatorHorizontal::SPins k_linearActuatorX{
    CStepperMotor::SPins{
        22, 24,
        26, 28 },
    8
  };

  constexpr CLinearActuatorVertical::SPins k_linearActuatorY{
    CGearMotor::SPins{
        4, // InA
        2, // InB
        3, // PWM
        PIN_A0 // CS
    },
    CEncoder::SPins{
        18 // Channel A
    },
    10
  };

  constexpr CLinearActuatorHorizontal::SPins k_linearActuatorZ{
    CStepperMotor::SPins{
        23, 25,
        27, 29 },
    9
  };

  constexpr CPanTilt::SPins k_panTilt{
    7, // Tilt
    6 // Pan
  };
}

#endif