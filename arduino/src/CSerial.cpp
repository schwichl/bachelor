#include "CSerial.hpp"


CSerial::EError CSerial::s_lastError{ EError::kSuccess };

#include "CLog.hpp"

/**
 * 
 */
bool CSerial::readPacket(protocol::CPacket *pOut)
{
    static constexpr int k_iHeaderSize{
    static_cast<int>(sizeof(protocol::CPacket::SHeader))
  };

  protocol::CPacket::SHeader header;

  int iBytesRead{ static_cast<int>(Serial.readBytes(reinterpret_cast<char *>(&header), k_iHeaderSize)) };

  if (iBytesRead != k_iHeaderSize)
  {
    if (iBytesRead > 0)
    {
      CSerial::s_lastError = EError::kPartialHeaderRead;
      slog.error(CLog::EErrorSource::kSerial, EError::kPartialHeaderRead);
    }

    // No data available.
    return false;
  }

  char *arrPayload{ new char[static_cast<size_t>(header.iLength)] };

  iBytesRead = Serial.readBytes(arrPayload, header.iLength);

  if (iBytesRead != header.iLength)
  {
    CSerial::s_lastError = EError::kPartialPayloadRead;
    slog.error(CLog::EErrorSource::kSerial, EError::kPartialPayloadRead);
    return false;
  }

  *pOut = static_cast<protocol::CPacket &&>(protocol::CPacket::fromRaw(static_cast<protocol::CPacket::SHeader &&>(header), arrPayload));
  return true;
}

/**
 * 
 */
void CSerial::print(const char *szMessage)
{
  CSerial::write(protocol::ECommand::kPrint, szMessage, strlen(szMessage) + 1);
}

/**
 * 
 */
void CSerial::write(
    protocol::ECommand command,
    const char *arrData,
    int iLength)
{
  protocol::CPacket packSend{
    command,
    arrData,
    iLength
  };

  // Incomplete writes are detected by the host as incomplete reads.
  Serial.write(packSend.getRawHeader(), sizeof(protocol::CPacket::SHeader));
  Serial.write(packSend.getRawPayload(), static_cast<size_t>(packSend.getPayloadLength()));
}