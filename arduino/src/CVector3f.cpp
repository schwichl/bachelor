#include "CVector.hpp"

#include <math.h>

/**
 * 
 */
CVector3f CVector3f::cross(const CVector3f& that) const
{
  return {
    (this->m_data[1] * that.m_data[2]) -
        (this->m_data[2] * that.m_data[1]),

    (this->m_data[2] * that.m_data[0]) -
        (this->m_data[0] * that.m_data[2]),

    (this->m_data[0] * that.m_data[1]) -
        (this->m_data[1] * that.m_data[0])
  };
}

/**
 * 
 */
CVector3f CVector3f::toAngles(void) const
{
  float flLength2d{ static_cast<float>(sqrt(this->m_data[0] * this->m_data[0] + this->m_data[2] * this->m_data[2])) };

  return {
    static_cast<float>(atan2(this->m_data[1], flLength2d)),
    static_cast<float>(atan2(this->m_data[0], this->m_data[2])),
    0.0f
  };
}

/**
 * 
 */
CVector3f CVector3f::toAxis(void) const
{
  float flCos{ cos(this->m_data[0]) };

  return {
    static_cast<float>(flCos * sin(this->m_data[1])),
    static_cast<float>(sin(this->m_data[0])),
    static_cast<float>(flCos * cos(this->m_data[1]))
  };
}

/**
 * 
 */
CVector3f CVector3f::fromSphericalCoordinates(float flPhi, float flTheta)
{
  float flSinPhi{ sin(flPhi) };

  return {
    flSinPhi * cos(flTheta),
    cos(flPhi),
    flSinPhi * sin(flTheta),
  };
}