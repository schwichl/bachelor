#include "CPacketHandler.hpp"

#include "CLog.hpp"
#include "CSerial.hpp"
#include "hardware.hpp"
#include "power.hpp"

#include "robot/CRobot.hpp"
#include "robot/components/CCamera.hpp"
#include "robot/components/CLinearActuatorHorizontal.hpp"
#include "robot/components/CLinearActuatorVertical.hpp"
#include "robot/components/CStepperMotor.hpp"
// #include "robot/components/CServo.hpp"
#include "robot/components/CGearMotor.hpp"
#include "robot/components/CPanTilt.hpp"

#include "protocol/packet/CHomeMotor.hpp"
#include "protocol/packet/CMove.hpp"
#include "protocol/packet/CPosition.hpp"
#include "protocol/packet/CSetAngles.hpp"
#include "protocol/packet/CSetPath.hpp"
#include "protocol/packet/CTurnMotor.hpp"
#include "protocol/packet/CUploadPath.hpp"
#include "protocol/packet/CView.hpp"

/**
 * 
 */
CPacketHandler* CPacketHandler::instance(void)
{
  static CPacketHandler s_instance{};

  return &s_instance;
}

/**
 * 
 */
void CPacketHandler::homeMotor(const protocol::packet::CHomeMotor* pHomeMotor) const
{
  if (pHomeMotor->arrChMotors[0] == '\0')
  {
    CRobot::instance()->home();
  }
  else
  {
    for (int i{ 0 };
         (i < static_cast<int>(sizeof(protocol::packet::CHomeMotor::arrChMotors))) &&
         (pHomeMotor->arrChMotors[i] != '\0');
         ++i)
    {
      switch (pHomeMotor->arrChMotors[i])
      {
      case 'x':
        hardware::pLinearActuatorX->home();
        break;
      case 'y':
        hardware::pLinearActuatorY->home();
        break;
      case 'z':
        hardware::pLinearActuatorZ->home();
        break;

      case 't':
        hardware::pPanTilt->setTilt(0.0f);
        break;
      case 'p':
        hardware::pPanTilt->setPan(0.0f);
        break;

      default:
        slog << "invalid motor selected: " << pHomeMotor->arrChMotors[i] << '\n';
        return;
      }
    }
  }
}

/**
 * 
 */
void CPacketHandler::turnMotor(const protocol::packet::CTurnMotor* pTurnMotor) const
{
  for (int i{ 0 };
       (i < static_cast<int>(sizeof(protocol::packet::CTurnMotor::arrChMotors))) &&
       (pTurnMotor->arrChMotors[i] != '\0');
       ++i)
  {
    switch (pTurnMotor->arrChMotors[i])
    {
    case 'x':
      hardware::pStepperX->spin(pTurnMotor->flRevolutions);
      break;
    case 'z':
      hardware::pStepperZ->spin(pTurnMotor->flRevolutions);
      break;

    case 'y':
      hardware::pGearMotor->spin(pTurnMotor->flRevolutions);
      break;

    case 't':
      hardware::pPanTilt->setTilt(pTurnMotor->flRevolutions);
      break;
    case 'p':
      hardware::pPanTilt->setPan(pTurnMotor->flRevolutions);
      break;

    default:
      slog << "invalid motor selected: " << pTurnMotor->arrChMotors[i] << '\n';
      return;
    }
  }
}

/**
 * 
 */
void CPacketHandler::move(const protocol::packet::CMove* pMove) const
{
  float* pX{};
  float* pY{};
  float* pZ{};

  for (int i{ 0 };
       (i < static_cast<int>(sizeof(protocol::packet::CTurnMotor::arrChMotors))) &&
       (pMove->arrChMotors[i] != '\0');
       ++i)
  {
    switch (pMove->arrChMotors[i])
    {
    case 'x':
      pX = &pMove->flPosition;
      break;
    case 'y':
      pY = &pMove->flPosition;
      break;
    case 'z':
      pZ = &pMove->flPosition;
      break;

    default:
      slog << "invalid motor selected: " << pMove->arrChMotors[i] << '\n';
      return;
    }
  }

  CRobot::instance()->move(pX, pY, pZ);
}

/**
 * 
 */
void CPacketHandler::processPacket(const protocol::CPacket& pckReceived) const
{
  if (power::g_bAwaitingConfirmation)
  {
    switch (pckReceived.getCommand())
    {
    case protocol::ECommand::kConfirmationDeny:
      power::g_bDie = true;
      [[fallthrough]];
    case protocol::ECommand::kConfirmationAllow:
      power::g_bAwaitingConfirmation = false;
      break;
    }

    return;
  }

  switch (pckReceived.getCommand())
  {
  case protocol::ECommand::kPing:
    CSerial::write(protocol::ECommand::kPong, '\0');
    break;
  case protocol::ECommand::kDisconnect:
    CRobot::instance()->home();
    // This implementation doesn't correspond to what the document says. It's
    // not resetting the arduino. There doesn't seem to be a reliable software
    // way of resetting the arduino.
    // The host only sends kDisconnect when the host program stops. Reconnecting
    // to the arduino mega causes a reset.
    break;
  case protocol::ECommand::kSetPath:
  {
    auto pPckPathInformation{ pckReceived.getPayload<protocol::packet::CSetPath>() };
    CRobot::instance()->setPathInformation(pPckPathInformation->iNodeCount);
    break;
  }
  case protocol::ECommand::kUploadPath:
    CRobot::instance()->setPath(protocol::packet::CUploadPath::deserialize(pckReceived.getRawPayload()));
    CSerial::write(protocol::ECommand::kUploadPathSuccess, '\0');
    CRobot::instance()->scan();
    break;
  case protocol::ECommand::kScan:
    CRobot::instance()->scan();
    break;
  case protocol::ECommand::kStatus:
    CRobot::instance()->printStatus();
    break;
  case protocol::ECommand::kGetPosition:
    CSerial::write(protocol::ECommand::kPosition,
                   protocol::packet::CPosition{
                       CRobot::instance()->getSoftwareOrigin(),
                       CRobot::instance()->getOffset() });
    break;
  case protocol::ECommand::kSnap:
    CRobot::instance()->snap(*pckReceived.getPayload<protocol::Bool>());
    break;
  case protocol::ECommand::kHomeMotor:
    if (CRobot::instance()->isScanning())
    {
      CRobot::instance()->abort();
    }
    else
    {
      this->homeMotor(pckReceived.getPayload<protocol::packet::CHomeMotor>());
    }
    break;
  case protocol::ECommand::kTurnMotor:
    this->turnMotor(pckReceived.getPayload<protocol::packet::CTurnMotor>());
    break;
  case protocol::ECommand::kMove:
    this->move(pckReceived.getPayload<protocol::packet::CMove>());
    break;
  case protocol::ECommand::kSetAngles:
  {
    auto pckSetAngles{ pckReceived.getPayload<protocol::packet::CSetAngles>() };
    CRobot::instance()->setAngles({ pckSetAngles->flTilt, pckSetAngles->flPan });
    break;
  }
  case protocol::ECommand::kView:
  {
    auto pckView{ pckReceived.getPayload<protocol::packet::CView>() };
    CVector3f vecOrigin{ pckView->flX, pckView->flY, pckView->flZ };

    CRobot::instance()->view(vecOrigin, { pckView->flTilt, pckView->flPan });
    break;
  }
  case protocol::ECommand::kWalk:
    if (!CRobot::instance()->moveAlongRay(*pckReceived.getPayload<protocol::Float>()))
    {
      slog << "couldn't move, invalid position\n";
    }
    break;
  default:
    slog << "unhandled command " << static_cast<int>(pckReceived.getCommand()) << '\n';
    break;
  }
}