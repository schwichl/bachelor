#if !defined(HPPcommands_CPacketHandler)
#define HPPcommands_CPacketHandler

#include "protocol/packet/CPacket.hpp"

#include "protocol/protocol.hpp"

namespace protocol
{
  // arduino
  namespace packet
  {
    class CHomeMotor;
    class CTurnMotor;
    class CMove;
  }
}

class CPacketHandler
{
public:
  /**
   * 
   */
  static CPacketHandler* instance(void);

private:
  /**
   * 
   */
  void homeMotor(const protocol::packet::CHomeMotor* pHomeMotor) const;

  /**
   * 
   */
  void turnMotor(const protocol::packet::CTurnMotor* pTurnMotor) const;

  /**
   * 
   */
  void move(const protocol::packet::CMove* pMove) const;

public:
  /**
   * 
   */
  void processPacket(const protocol::CPacket& pckReceived) const;
};

#endif