#if !defined(ARDUINO)
#if !defined(HPPSTraceHit)
#define HPPSTraceHit

#include "CVector.hpp"

struct STraceHit
{
  CVector3f vecOrigin;
  CVector3f vecNormal;
};

#endif
#endif