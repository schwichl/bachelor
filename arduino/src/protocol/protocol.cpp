#include "protocol.hpp"
#include "packet/CVerifyProtocol.hpp"

namespace protocol
{
  // avr-gcc requires old static_assert
  static_assert(sizeof(Bool) == 1, "");
  static_assert(sizeof(Char) == 1, "");
  static_assert(sizeof(Int8) == 1, "");
  static_assert(sizeof(Int16) == 2, "");
  static_assert(sizeof(Int32) == 4, "");
  static_assert(sizeof(Int64) == 8, "");
  static_assert(sizeof(Float) == 4, "");
  static_assert(sizeof(CPacket::SHeader) == 4, "");

  /**
   * 
   */
  EVerificationError verify(const packet::CVerifyProtocol& that)
  {
    packet::CVerifyProtocol test{};

    if (test.iProtocolVersion != that.iProtocolVersion)
    {
      return EVerificationError::kVersionMismatch;
    }

    return EVerificationError::kSuccess;
  }

  /**
   * 
   */
  EVerificationError verifyOffline(void)
  {
    // Type size has been verified at compile-time.
    // We assume that all integer types use the same internal representation.

    // Little endian
    {
      Int16 i{ 1 };

      if (*reinterpret_cast<char*>(&i) != 1)
      {
        return EVerificationError::kEndianness;
      }
    }

    // Two's complement. Guaranteed since C++20.
    {
      Int8 i{ -5 };

      if (((*reinterpret_cast<unsigned char*>(&i) & (1 << 7)) == 0) || ((*reinterpret_cast<unsigned char*>(&i) & ~(1 << 7)) != 0b0111'1011))
      {
        return EVerificationError::kTwosComplement;
      }
    }

    // IEEE-754
    {
      Float fl{ 123.456f };
      unsigned char arrUchTest[]{ 121, 233, 246, 66 };

      auto pData{ reinterpret_cast<unsigned char*>(&fl) };

      for (int i{ 0 }; i < 4; ++i, ++pData)
      {
        auto uchRead{ *pData };

        if (uchRead != arrUchTest[i])
        {
          return EVerificationError::kFloatingPoint;
        }
      }
    }

    return EVerificationError::kSuccess;
  }

  /**
   * 
   */
  const char* CPacket::getRawHeader(void) const
  {
    return reinterpret_cast<const char*>(&this->m_header);
  }

  /**
   * 
   */
  const char* CPacket::getRawPayload(void) const
  {
    return this->m_pArrPayload;
  }

  /**
   * 
   */
  Int16 CPacket::getPayloadLength(void) const
  {
    return this->m_header.iLength;
  }

  /**
   * 
   */
  ECommand CPacket::getCommand(void) const
  {
    return this->m_header.command;
  }

  /**
   * 
   */
  CPacket& CPacket::operator=(CPacket&& that)
  {
    this->m_header = static_cast<SHeader&&>(that.m_header);
    this->m_pArrPayload = that.m_pArrPayload;

    that.m_pArrPayload = nullptr;

    return *this;
  }

  /**
   * 
   */
  CPacket& CPacket::operator=(const CPacket& that)
  {
    this->m_header = that.m_header;
    this->m_pArrPayload = new char[static_cast<size_t>(that.m_header.iLength)];

    memcpy(this->m_pArrPayload, that.m_pArrPayload, static_cast<size_t>(that.m_header.iLength));

    return *this;
  }

  /**
   * 
   */
  CPacket::CPacket(ECommand command, const char* arrData, int iLength)
      : m_header{ command, static_cast<Int16>(iLength) },
        m_pArrPayload{ new char[static_cast<size_t>(iLength)] }
  {
    memcpy(
        static_cast<void*>(this->m_pArrPayload),
        static_cast<const void*>(arrData),
        static_cast<size_t>(iLength));
  }

  /**
   * 
   */
  CPacket::CPacket(CPacket::SHeader&& header, char* pArrData)
      : m_header{ static_cast<CPacket::SHeader&&>(header) },
        m_pArrPayload{ pArrData }
  {
  }

  /**
   * 
   */
  CPacket::CPacket(CPacket&& that)
      : CPacket{ static_cast<CPacket::SHeader&&>(that.m_header), that.m_pArrPayload }
  {
    that.m_pArrPayload = nullptr;
  }

  /**
   * 
   */
  CPacket::CPacket(CPacket& that)
      : m_header{ that.m_header },
        m_pArrPayload{
          (that.m_pArrPayload == nullptr) ? nullptr : new char[static_cast<size_t>(that.m_header.iLength)]
        }
  {
    this->m_header = that.m_header;

    if (that.m_pArrPayload != nullptr)
    {
      memcpy(
          this->m_pArrPayload,
          that.m_pArrPayload,
          static_cast<size_t>(that.m_header.iLength));
    }
  }

  /**
   * 
   */
  CPacket::CPacket(void)
  {
  }

  /**
   * 
   */
  CPacket::~CPacket(void)
  {
    if (this->m_pArrPayload != nullptr)
    {
      delete[] this->m_pArrPayload;
    }
  }

  /**
   * 
   */
  CPacket CPacket::fromRaw(CPacket::SHeader&& header, char* pArrData)
  {
    return { static_cast<CPacket::SHeader&&>(header), pArrData };
  }
}