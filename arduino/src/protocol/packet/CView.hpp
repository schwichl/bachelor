#if !defined(HPPprotocol_packet_CView)
#define HPPprotocol_packet_CView

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CView : public CPacket
    {
    public:
      Float flX;
      Float flY;
      Float flZ;

      Float flTilt;
      Float flPan;

      CView(void) = default;

      CView(Float flX, Float flY, Float flZ, Float flTilt, Float flPan)
          : flX{ flX },
            flY{ flY },
            flZ{ flZ },
            flTilt{ flTilt },
            flPan{ flPan }
      {
      }
    };
  }
}

#endif