#if !defined(HPPprotocol_packet_CVerifyProtocol)
#define HPPprotocol_packet_CVerifyProtocol

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CVerifyProtocol : public CPacket
    {
    public:
      Int32 iProtocolVersion{ k_iVersion };
    };
  }
}

#endif