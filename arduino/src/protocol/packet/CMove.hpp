#if !defined(HPPprotocol_packet_CMove)
#define HPPprotocol_packet_CMove

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CMove : public CPacket
    {
    public:
      /**
       * Motors that are supposed to turn. Valid options are
       * x
       * y
       * z
       */
      Char arrChMotors[4]{};

      /**
       * Position in mm.
       */
      Float flPosition;
    };
  }
}

#endif