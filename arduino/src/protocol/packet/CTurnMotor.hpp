#if !defined(HPPprotocol_packet_CTurnMotor)
#define HPPprotocol_packet_CTurnMotor

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CTurnMotor : public CPacket
    {
    public:
      /**
       * Motors that are supposed to turn. Valid options are
       * x
       * y
       * z
       * p(an)
       * t(ilt)
       */
      Char arrChMotors[8]{};

      /**
       * For servos, this is the angle in radians.
       * Otherwise, number of revolutions.
       */
      Float flRevolutions;
    };
  }
}

#endif