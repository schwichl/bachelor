#if !defined(HPPprotocol_packet_CHomeMotor)
#define HPPprotocol_packet_CHomeMotor

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CHomeMotor : public CPacket
    {
    public:
      /**
       * Motors that are supposed to be homed. Valid options are
       * x
       * y
       * z
       * p(an)
       * t(ilt)
       */
      Char arrChMotors[8]{};
    };
  }
}

#endif