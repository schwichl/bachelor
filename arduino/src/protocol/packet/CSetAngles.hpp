#if !defined(HPPprotocol_packet_CSetAngles)
#define HPPprotocol_packet_CSetAngles

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    class CSetAngles : public CPacket
    {
    public:
      Float flTilt;
      Float flPan;

      CSetAngles(Float flTilt, Float flPan)
          : flTilt{ flTilt }, flPan{ flPan }
      {
      }
    };
  }
}

#endif