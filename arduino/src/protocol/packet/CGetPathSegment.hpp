#if !defined(HPPprotocol_packet_CGetPathSegment)
#define HPPprotocol_packet_CGetPathSegment

#include "CPacket.hpp"

#include "protocol/protocol.hpp"

// arduino
namespace protocol
{
  namespace packet
  {
    /**
     * Used by the Arduino the request a segment of the path from the host.
     */
    class CGetPathSegment : public CPacket
    {
    public:
      /**
       * Inclusive.
       */
      Int32 iNodeStart{};

      /**
       * Inclusive.
       */
      Int32 iNodeEnd{};
    };
  }
}

#endif