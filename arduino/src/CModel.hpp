#if !defined(ARDUINO)
#if !defined(HPPshared_CModel)
#define HPPshared_CModel

#include "CVector.hpp"
#include "STraceHit.hpp"

/**
 * Model based on .obj structure.
 * +X Right
 * +Y Up
 * +Z Forward
 * Targets should face towards negative Z.
 * 0 0 0 is the trace center used to construct the observer path.
 */
class CModel
{
public:

  struct STriangle
  {
    int arrIVertexIndexes[3]{};
    int arrINormalIndexes[3]{};
  };

private:
  /**
   * 
   */
  CVector3f *m_arrVertexes{};

  /**
   * 
   */
  int m_iVertexCount{};

  /**
   * 
   */
  CVector3f *m_arrNormals{};

  /**
   * 
   */
  int m_iNormalCount{};

  /**
   * 
   */
  STriangle *m_arrTriangles{};

  /**
   * 
   */
  int m_iTriangleCount{};

public:

  /**
   * 
   */
  int getVertexCount(void) const;

  /**
   * @return Raw vertex array.
   */
  const CVector3f *getVertexes(void) const;

  /**
   * 
   */
  int getNormalCount(void) const;

  /**
   * @return Raw normal array.
   */
  const CVector3f *getNormals(void) const;

  /**
   * 
   */
  int getTriangleCount(void) const;

  /**
   * @return Triangle vertexes at the given index.
   */
  const int *getTriangleVertex(int iIndex) const;

  /**
   * @return Triangle normals at the given index.
   */
  const int *getTriangleNormal(int iIndex) const;

public:

  /**
   * 
   */
  void setVertex(int iIndex, const CVector3f &vecVertex);

  /**
   * 
   */
  void setNormal(int iIndex, const CVector3f &vecNormal);

  /**
   * 
   */
  void setTriangle(int iIndex, const STriangle &triangle);

public:

  /**
   * Moves every vertex by the given offset.
   */
  void move(const CVector3f &vecOffset);

  /**
   * Calculates the object's AABB.
   */
  void getAABB(CVector3f *pVecMin, CVector3f *pVecMax) const;

  /**
   * Calculates the nearest intersection with the given ray.
   * @param vecDirection Normalized direction
   * @return True if the trace hit something.
   */
  bool trace(const CVector3f &vecStart, const CVector3f &vecDirection, STraceHit *pHit) const;

public:

  /**
   * 
   */
  CModel(int iVertexCount, int iNormalCount, int iTriangleCount);

  /**
   * 
   */
  CModel(void);
};

#endif
#endif