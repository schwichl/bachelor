#include "CActiveComponent.hpp"

#include "CLoopHook.hpp"

/**
 * 
 */
CActiveComponent::CActiveComponent(void)
{
  g_loopHook.registerCallback(this);
}

/**
 * 
 */
CActiveComponent::~CActiveComponent(void)
{
}