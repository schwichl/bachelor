#if !defined(HPPCLog)
#define HPPCLog

#include "Arduino.h"

/**
 * Streamed logging
 */
class CLog
{
public:
  enum class EErrorSource
  {
    kNone = 1,
    kSerial,
    kProtocol
  };

private:
  /**
   * 
   */
  static constexpr int k_iBufferLength{ 256 };

private:
  /**
   * 
   */
  char m_arrBuffer[k_iBufferLength]{};

  /**
   * 
   */
  int m_iUsedBuffer{ 0 };

private:
  /**
   * 
   */
  void flush(void);

  /**
   * 
   */
  template <class... Args>
  void buffer(const char *szFormat, Args... args)
  {
    int iLength{ snprintf(
        &this->m_arrBuffer[this->m_iUsedBuffer],
        this->k_iBufferLength - this->m_iUsedBuffer,
        szFormat,
        args...) };

    this->m_iUsedBuffer += iLength;
  }

public:
  /**
   * Tries to notify the host that an error occurred via CSerial and Serial.
   * Flashes LED_BUILTIN and blocks.
   */
  template <class T>
  void error(EErrorSource source, T error)
  {
    static constexpr int k_iRepeatDelay{ 2000 };
    static constexpr int k_iSeparatorDelay{ 1000 };
    static constexpr int k_iCodeDelay{ 500 };

    static constexpr int k_iLED{ LED_BUILTIN };
    static constexpr int k_iLEDOn{ HIGH };
    static constexpr int k_iLEDOff{ LOW };

    *this << "a fatal error has occurred: " << static_cast<int>(source) << ' ' << (static_cast<int>(error) + 1) << '\n';
    Serial.print("a fatal error has occurred: ");
    Serial.print(static_cast<int>(source));
    Serial.print(" ");
    Serial.println(static_cast<int>(error) + 1);

    pinMode(k_iLED, OUTPUT);

    while (true)
    {
      for (int i{ 0 }; i < static_cast<int>(source); ++i)
      {
        digitalWrite(k_iLED, k_iLEDOn);
        delay(k_iCodeDelay);

        digitalWrite(k_iLED, k_iLEDOff);
        delay(k_iCodeDelay);
      }

      // We already slept for k_iCodeDelay ms at the end of the loop
      delay(k_iSeparatorDelay - k_iCodeDelay);

      // +1 to allow 0-valued enumerators.
      for (int i{ 0 }; i < (static_cast<int>(error) + 1); ++i)
      {
        digitalWrite(k_iLED, k_iLEDOn);
        delay(k_iCodeDelay);

        digitalWrite(k_iLED, k_iLEDOff);
        delay(k_iCodeDelay);
      }

      // We already slept for k_iCodeDelay ms at the end of the loop
      delay(k_iRepeatDelay - k_iCodeDelay);
    }
  }

public:
  CLog &operator<<(char value);
  CLog &operator<<(const char *value);
  CLog &operator<<(String value);
  CLog &operator<<(int value);
  CLog &operator<<(long value);
  CLog &operator<<(unsigned long value);
  CLog &operator<<(float value);
  CLog &operator<<(bool value);
};

extern CLog slog;

#endif