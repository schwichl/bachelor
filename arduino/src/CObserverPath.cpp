#include "CObserverPath.hpp"

#include "CVector.hpp"

#if !defined(ARDUINO)
#include "model/CPly.hpp"
#include "robot/CRobot.hpp"

#include "scanconfig.hpp"

#include <algorithm>
#include <execution>
#include <fstream>
#include <vector>
#endif

#include <math.h>

/**
 * {@link CObserverPath} didn't inherit from the beginning. This function is a
 * leftover.
 */
int CObserverPath::getNodeCount(void) const
{
  return static_cast<int>(this->size());
}

/**
 * {@link CObserverPath} didn't inherit from the beginning. This function is a
 * leftover.
 */
const CObserverNode& CObserverPath::getNode(int iIndex) const
{
  return this->operator[](iIndex);
}

#if !defined(ARDUINO)
/**
 * 
 */
void CObserverPath::generatePreviewImage(const STraceHit& hit, CPly* pPlyOut)
{
  // Meters
  static constexpr float k_flImageHalfSize{ 0.17f };
  static constexpr float k_flSurfaceDistance{ 0.01f };

  // Span a coordinate system with the normal as the Y axis
  CVector3f vecX{ (hit.vecNormal.dot({ 0.0f, 1.0f, 0.0f }) == 0.0f) ? CVector3f{ 1.0f, 0.0f, 0.0f } : hit.vecNormal.cross({ 0.0f, 1.0f, 0.0f }) };
  CVector3f vecZ{ hit.vecNormal.cross(vecX) };

  vecX.normalize();
  vecZ.normalize();

  std::vector<CVector3f> vVecCorners(4, hit.vecOrigin + hit.vecNormal * k_flSurfaceDistance);

  for (int i{ 0 }; i < 4; ++i)
  {
    CVector2f vecOffset{
      // -1 1 1 -1
      k_flImageHalfSize * (((((i + 1) / 2) % 2) == 0) ? -1.0f : 1.0f),
      // 1 1 -1 -1
      k_flImageHalfSize * (((i / 2) == 0) ? 1.0f : -1.0f)
    };

    vVecCorners[static_cast<decltype(vVecCorners)::size_type>(i)] += {
      vecX * vecOffset[0] * k_flImageHalfSize +
      vecZ * vecOffset[1] * k_flImageHalfSize
    };
  }

  pPlyOut->addFace(vVecCorners[0], vVecCorners[1], vVecCorners[2], vVecCorners[3]);
}

/**
 * 
 */
void CObserverPath::removeUnreachableNodes(ECheckZ checkZ)
{
  size_type iNewSize{
    std::remove_if(this->begin(), this->end(), [=](const auto& node) {
      return !CRobot::instance()->isNodeReachable(node, checkZ);
    }) -
    this->begin()
  };
  this->resize(iNewSize);
}

/**
 * Nodes are sliced into horizontal layers top-to-bottom.
 * Inside each layer, the nodes are ordered left-to-right and right-to-left,
 * alternating.
 */
void CObserverPath::sortNodes(void)
{
  // Millimeters
  constexpr float k_flLayerHeight{ 50.0f };

  if (this->empty())
  {
    return;
  }

  // Y-Coordinate of the highest node
  float flHighest{
    std::max_element(std::execution::par_unseq, this->begin(), this->end(),
                     [](const auto& nodeA, const auto& nodeB) {
                       return (nodeA.getOrigin()[1] < nodeB.getOrigin()[1]);
                     })
        ->getOrigin()[1]
  };

  // Y-Coordinate of the lowest node
  float flLowest{
    std::min_element(std::execution::par_unseq, this->begin(), this->end(),
                     [](const auto& nodeA, const auto& nodeB) {
                       return (nodeA.getOrigin()[1] < nodeB.getOrigin()[1]);
                     })
        ->getOrigin()[1]
  };

  std::vector<std::vector<CObserverNode>> vLayers(
      std::max(1ul, static_cast<decltype(vLayers)::size_type>(
                        std::ceil((flHighest - flLowest) / k_flLayerHeight))));

  for (const auto& node : *this)
  {
    int iLayerIndex{ static_cast<int>((flHighest - node.getOrigin()[1]) / k_flLayerHeight) };

    vLayers[static_cast<decltype(vLayers)::size_type>(iLayerIndex)].push_back(node);
  }

  bool bLeftToRight{ true };

  for (auto& vLayer : vLayers)
  {
    std::sort(
        vLayer.begin(), vLayer.end(),
        [&](const auto& nodeA, const auto& nodeB) {
          return (bLeftToRight ? (nodeA.getOrigin()[0] < nodeB.getOrigin()[0])
                               : (nodeA.getOrigin()[0] > nodeB.getOrigin()[0]));
        });

    bLeftToRight = !bLeftToRight;
  }

  int iNodeIndex{ 0 };

  for (const auto& vLayer : vLayers)
  {
    for (const auto& node : vLayer)
    {
      this->operator[](iNodeIndex++) = node;
    }
  }
}

/**
 * 
 */
void CObserverPath::offsetAndFilterNodes(const CVector3f& vecOffset)
{
  std::for_each(this->begin(), this->end(), [&](auto& node) {
    node.shift(vecOffset);
  });

  this->removeUnreachableNodes(ECheckZ::kNormal);
}

/**
 * 
 */
void CObserverPath::generatePreviewImages(const std::string& strFileName, const CVector3f& vecOffset, float flScale) const
{
  CPly plyPreview{};

  std::for_each(std::execution::seq,
                this->begin(), this->end(), [&](const auto& node) {
                  CObserverPath::generatePreviewImage(
                      { (node.hit.vecOrigin + vecOffset) * flScale, node.hit.vecNormal },
                      &plyPreview);
                });

  std::ofstream ofs{ strFileName };

  if (ofs << plyPreview)
  {
    std::cout << strFileName << " written\n";
  }
  else
  {
    std::cout << "couldn't write " << strFileName << '\n';
  }
}

/**
 * 
 */
void CObserverPath::generatePreviewPath(const std::string& strFileName, const CVector3f& vecOffset, float flScale) const
{
  CPly plyPreview{};

  std::for_each(std::execution::seq,
                this->begin(), this->end(), [&](const auto& node) {
                  plyPreview.addVertex((node.getOrigin() + vecOffset) * flScale);
                });

  std::ofstream ofs{ strFileName };

  if (ofs << plyPreview)
  {
    std::cout << strFileName << " written\n";
  }
  else
  {
    std::cout << "couldn't write " << strFileName << '\n';
  }
}

/**
 * Trace origin: 0 0 0
 * Trace a hemisphere (with cut-offs) pointing at 0 0 -1.
 */
CObserverPath CObserverPath::fromModel(
    const CModel& model,
    float flSteps,
    float flScanDistance)
{
  CObserverPath opResult{};

  static const float k_flPi{ acos(-1.0f) };

  /**
   * Margin of phi.
   */
  static constexpr float k_flPhiCutOff{ 0.1f };

  float flVerticalStepSize{ k_flPi / flSteps };

  for (float flPhi{ k_flPhiCutOff }; flPhi <= (k_flPi - k_flPhiCutOff); flPhi += flVerticalStepSize)
  {
    float flSinPhi{ sin(flPhi) };

    float flHorizontalSteps{ flSteps * flSinPhi };
    float flHorizontalStepSize{ k_flPi / flHorizontalSteps };

    for (float flTheta{ 0.0f }; flTheta <= k_flPi; flTheta += flHorizontalStepSize)
    {
      auto vecTraceDirection{ CVector3f::fromSphericalCoordinates(flPhi, flTheta) };
      vecTraceDirection[2] *= -1.0f;

      STraceHit hit{};

      if (model.trace({}, vecTraceDirection, &hit))
      {
        CVector3f vecNodeOrigin{
          hit.vecOrigin + (hit.vecNormal * flScanDistance)
        };

        float flRangingDistance{ 0.0f };

        if constexpr (scanconfig::k_bEnableRangingSensor)
        {
          CVector3f vecNodeAngle{ CVector3f{ -hit.vecNormal }.toAngles() };
          vecNodeAngle[0] -= (k_flPi / 2.0f);

          CVector3f vecRangingSensorOrigin{
            vecNodeOrigin +
            vecNodeAngle.toAxis() * scanconfig::k_flRangingSensorOffset +
            -hit.vecNormal * scanconfig::k_flRangingSensorPivotDistance
          };
          // @vecRangingSensorOrigin is now next to the camera, move it along the
          // camera's normal to where the sensor is attached.

          STraceHit hitRange{};

          if (model.trace(vecRangingSensorOrigin, -hit.vecNormal, &hitRange))
          {
            flRangingDistance = (hitRange.vecOrigin - vecRangingSensorOrigin).length();
          }
        }

        CObserverNode node{
          vecNodeOrigin,
          {},
          -hit.vecNormal,
          flRangingDistance
        };

        node.hit = hit;

        opResult.push(node);
      }
    }
  }

  if (opResult.empty())
  {
    return opResult;
  }

  return opResult;
}
#endif