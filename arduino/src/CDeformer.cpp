#include "CDeformer.hpp"

#include "shared_prefs.hpp"

#if defined(ARDUINO)
#include "cmath"
#else
#include <cmath>
#endif

/**
 * 
 */
CDeformer* CDeformer::instance(void)
{
  static CDeformer s_instance{};

  return &s_instance;
}

/**
 * 
 */
CVector3f CDeformer::getCameraRotationalOffset(const CVector3f& vecDirection) const
{
  CVector3f vecBaseOffset{ shared_prefs::k_vecCameraPivotOffset };

  // These probably need adjustment (*-1 +90, etc).
  const CVector2f& vecAngles{
    std::atan2(vecDirection[1], std::sqrt(vecDirection[0] * vecDirection[0] + vecDirection[2] * vecDirection[2])),
    std::atan2(vecDirection[0], vecDirection[2])
  };

  float flSin{ std::sin(vecAngles[0]) };
  float flCos{ std::cos(vecAngles[0]) };

  CVector3f vecRotatedX{
    vecBaseOffset[0],
    (vecBaseOffset[1] * flCos) - (vecBaseOffset[2] * flSin),
    (vecBaseOffset[1] * flSin) + (vecBaseOffset[2] * flCos)
  };

  flSin = std::sin(vecAngles[1]);
  flCos = std::cos(vecAngles[1]);

  CVector3f vecRotatedY{
    (vecRotatedX[0] * flCos) + (vecRotatedX[2] * flSin),
    vecRotatedX[1],
    (vecRotatedX[0] * -flSin) + (vecRotatedX[2] * flCos)
  };

  return (vecRotatedY - vecBaseOffset);
}

/**
 * 
 */
CVector3f CDeformer::getStructuralDeformationOffset(
    [[maybe_unused]] const CVector3f& vecOrigin) const
{
  return {};
}

/**
 *
 */
CVector3f CDeformer::realizeOrigin(const CVector3f& vecOrigin, const CVector3f& vecDirection) const
{
  auto vecRotationalOffset{ this->getCameraRotationalOffset(vecDirection) };
  auto vecDeformationOffset{ this->getStructuralDeformationOffset(vecOrigin) };

  auto vecRealOrigin{ vecRotationalOffset + vecDeformationOffset };

  return vecRealOrigin;
}