#include "./CLog.hpp"

#include "CSerial.hpp"

CLog slog{};

/**
 * 
 */
void CLog::flush(void)
{
  CSerial::print(this->m_arrBuffer);

  memset(this->m_arrBuffer, 0, sizeof(this->m_arrBuffer));

  this->m_iUsedBuffer = 0;
}

CLog &CLog::operator<<(char value)
{
  if (value == '\n')
  {
    this->flush();
  }
  else
  {
    this->buffer("%c", value);
  }

  return *this;
}

CLog &CLog::operator<<(const char *value)
{
  int iTextLength{ static_cast<int>(strlen(value)) - 1 };

  if (value[iTextLength] == '\n')
  {
    memcpy(&this->m_arrBuffer[this->m_iUsedBuffer], value, iTextLength);
    this->m_iUsedBuffer += iTextLength;
    this->flush();
  }
  else
  {
    this->buffer("%s", value);
  }

  return *this;
}

CLog &CLog::operator<<(String value)
{
  return this->operator<<(value.c_str());
}

CLog &CLog::operator<<(int value)
{
  this->buffer("%i", value);

  return *this;
}

CLog &CLog::operator<<(long value)
{
  this->buffer("%i", value);

  return *this;
}

CLog &CLog::operator<<(unsigned long value)
{
  this->buffer("%u", value);

  return *this;
}

CLog &CLog::operator<<(float value)
{
  // Arduino doesn't support %f formatting.
  // this->buffer("%f", value);

  this->buffer("%s", String(value).c_str());

  return *this;
}

CLog &CLog::operator<<(bool value)
{
  this->buffer("%i", value);

  return *this;
}