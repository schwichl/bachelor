#if !defined(HPPhardware)
#define HPPhardware

class CCamera;
class CEncoder;
class CGearMotor;
class CLinearActuatorHorizontal;
class CLinearActuatorVertical;
class CPanTilt;
class CServo;
class CStepperMotor;
class CVL53L0X;
class CVL6180X;

/**
 * Exposes all hardware components to make them usable for debugging.
 * It is undefined who manages these components. It is up to the user to verify
 * that the components are valid before using them.
 */
namespace hardware
{
  /**
   * The camera.
   */
  extern CCamera* pCamera;

  /**
   * Encoder attached to {@link pGearMotor}.
   */
  extern CEncoder* pEncoder;

  /**
   * Gear motor controlling translation along the Y axis.
   * Owned by {@link CRobot (CLinearActuatorVertical)}
   */
  extern CGearMotor* pGearMotor;

  /**
   * Linear actuators controlling the X axis.
   * Owned by {@link CRobot}.
   */
  extern CLinearActuatorHorizontal* pLinearActuatorX;

  /**
   * Linear actuator controlling the Z axis.
   * Owned by {@link CRobot}.
   */
  extern CLinearActuatorVertical* pLinearActuatorY;

  /**
   * Linear actuators controlling the Z axis.
   * Owned by {@link CRobot}.
   */
  extern CLinearActuatorHorizontal* pLinearActuatorZ;

  /**
   * Stepper motor controlling translation along the X axis.
   * Owned by {@link CRobot (CLinearActuatorHorizontal)}
   */
  extern CStepperMotor* pStepperX;

  /**
   * Stepper motor controlling translation along the Z axis.
   * Owned by {@link CRobot (CLinearActuatorHorizontal)}
   */
  extern CStepperMotor* pStepperZ;

  /**
   * Servo controlling tilt.
   * Owned by {@link CRobot (CPanTilt)}
   */
  extern CServo* pServoTilt;

  /**
   * Servo controlling pan.
   * Owned by {@link CRobot (CPanTilt)}
   */
  extern CServo* pServoPan;

  /**
   * Owned by {@link CRobot}
   */
  extern CPanTilt* pPanTilt;

#if 0
  /**
   * VL53L0X. Owned by {@link CRobot}.
   */
  extern CVL53L0X* pDistanceSensor;
#else
  /**
   * VL6180X. Owned by {@link CRobot}.
   */
  extern CVL6180X* pDistanceSensor;
#endif
}

#endif