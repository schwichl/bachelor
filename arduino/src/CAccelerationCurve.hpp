#if !defined(HPPCAccelerationCurve)
#define HPPCAccelerationCurve

class CAccelerationCurve
{
private:

  /**
   * How long it takes to go from 0 to 1.
   */
  float m_flAccelerationPeriod{};

private:

  /**
   * @param fl 0 to 1
   * @return Acceleration curve on the range 0 to 1
   */
  float accelerate(float fl) const;

public:

  /**
   * 
   */
  void setAccelerationPeriod(float flAccelerationPeriod);

  /**
   * Calculates the curve at @flPosition.
   * @param flStart Start position
   * @param flEnd End position
   * @param flPosition Current position
   */
  float get(float flStart, float flEnd, float flPosition) const;
};

#endif