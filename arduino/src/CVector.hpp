#if !defined(HPPCVector)
#define HPPCVector

#include <cmath>

#if defined(ARDUINO)
#include "CLog.hpp"
#else
#include <iostream>
#endif

template <class T, int k_iSize>
class CVector
{
protected:
  T m_data[k_iSize]{};

public:
  /**
   * Sets all components to 0
   */
  constexpr void clear(void)
  {
    for (T& el : this->m_data)
    {
      el = {};
    }
  }

  /**
   * 
   */
  constexpr T dot(const CVector& that) const
  {
    T result{};

    for (int i{ 0 }; i < k_iSize; ++i)
    {
      result += (this->m_data[i] * that.m_data[i]);
    }

    return result;
  }

  /**
   * 
   */
  constexpr T lengthSqr(void) const
  {
    T result{};

    for (int i{ 0 }; i < k_iSize; ++i)
    {
      result += (this->m_data[i] * this->m_data[i]);
    }

    return result;
  }

  /**
   * 
   */
  constexpr T length(void) const
  {
    // Can't use @std::sqrt, arduino.
    return static_cast<T>(sqrt(this->lengthSqr()));
  }

  /**
   * 
   */
  constexpr CVector& normalize(void)
  {
    return this->operator/=(this->length());
  }

public:
  /**
   * 
   */
  constexpr bool operator==(const CVector& that) const
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      if (this->m_data[i] != that.m_data[i])
      {
        return false;
      }
    }

    return true;
  }

  /**
   * 
   */
  constexpr bool operator!=(const CVector& that) const
  {
    return !this->operator==(that);
  }

  /**
   *
   */
  constexpr CVector& operator=(const CVector& that)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] = that.m_data[i];
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector& operator+=(const CVector& that)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] += that.m_data[i];
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector& operator-=(const CVector& that)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] -= that.m_data[i];
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector& operator*=(const CVector& that)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] *= that.m_data[i];
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector& operator*=(T value)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] *= value;
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector& operator/=(T value)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] /= value;
    }

    return *this;
  }

  /**
   * 
   */
  constexpr CVector operator+(const CVector& that) const
  {
    CVector vecResult{ *this };

    vecResult += that;

    return vecResult;
  }

  /**
   * 
   */
  constexpr CVector operator-(const CVector& that) const
  {
    CVector vecResult{ *this };

    vecResult -= that;

    return vecResult;
  }

  /**
   * 
   */
  constexpr CVector operator*(const CVector& that) const
  {
    CVector vecResult{ *this };

    vecResult *= that;

    return vecResult;
  }

  /**
   * 
   */
  constexpr CVector operator*(T value) const
  {
    CVector vecResult{ *this };

    vecResult *= value;

    return vecResult;
  }

  /**
   * 
   */
  constexpr CVector operator/(T value) const
  {
    CVector vecResult{ *this };

    vecResult /= value;

    return vecResult;
  }

  /**
   * 
   */
  constexpr CVector operator-(void) const
  {
    return (*this * -1);
  }

  /**
   * Doesn't check bounds!
   */
  constexpr const T operator[](int iIndex) const
  {
    return this->m_data[iIndex];
  }

  /**
   * Doesn't check bounds!
   */
  constexpr T& operator[](int iIndex)
  {
    return this->m_data[iIndex];
  }

public:
  /**
   * 
   */
  constexpr CVector(const CVector<T, k_iSize>& that)
  {
    for (int i{ 0 }; i < k_iSize; ++i)
    {
      this->m_data[i] = that.m_data[i];
    }
  }

  /**
   * 
   */
  template <class... Args>
  constexpr CVector(Args... args)
      : m_data{ static_cast<T>(args)... }
  {
  }

  /**
   * 
   */
  constexpr CVector(void)
  {
  }
};

template <class T, int k_iSize>
#if defined(ARDUINO)
CLog& operator<<(CLog& s, const CVector<T, k_iSize>& vec)
#else
std::ostream& operator<<(std::ostream& s, const CVector<T, k_iSize>& vec)
#endif
{
  for (int i{ 0 }; i < k_iSize; ++i)
  {
    s << vec[i];

    if (i != (k_iSize - 1))
    {
      s << ' ';
    }
  }

  return s;
}

using CVector2f = CVector<float, 2>;

class CVector3f : public CVector<float, 3>
{
public:
  using CVector<float, 3>::CVector;

  /**
   * 
   */
  CVector3f cross(const CVector3f& that) const;

  /**
   * 
   */
  CVector3f toAngles(void) const;

  /**
   * 
   */
  CVector3f toAxis(void) const;

public:
  constexpr CVector3f(const CVector<float, 3>& that)
      : CVector<float, 3>::CVector{ that }
  {
  }

public:
  /**
   * flPhi Vertical
   * flTheta Horizontal
   */
  static CVector3f fromSphericalCoordinates(float flPhi, float flTheta);
};

#endif