#include "CAccelerationCurve.hpp"

#include "algorithm"
#include "cmath"

/**
 * https://www.desmos.com/calculator/fbgwcpir1k (2019-09-04)
 */
float CAccelerationCurve::accelerate(float fl) const
{
  static const float k_flPi{ std::acos(-1.0f) };

  return ((std::sin((fl * k_flPi) - (k_flPi / 2.0f)) + 1.0f) / 2.0f);
}

/**
 * 
 */
void CAccelerationCurve::setAccelerationPeriod(float flAccelerationPeriod)
{
  this->m_flAccelerationPeriod = flAccelerationPeriod;
}

/**
 * 
 */
float CAccelerationCurve::get(float flStart, float flEnd, float flPosition) const
{
  double dbMinEdge{
    std::min(std::abs(flPosition - flStart),
             std::abs(flEnd - flPosition))
  };

  if (dbMinEdge <= this->m_flAccelerationPeriod)
  {
    return this->accelerate(dbMinEdge / this->m_flAccelerationPeriod);
  }

  return 1.0f;
}