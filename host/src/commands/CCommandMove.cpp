#include "CCommandMove.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CMove.hpp"

#include <cstring>

/**
 * 
 */
const std::string &CCommandMove::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " motors position" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandMove::execute(const std::vector<std::string> &vStrInput)
{
  protocol::packet::CMove pckMove{};

  auto uiMotorLength{
    std::min(sizeof(protocol::packet::CMove::arrChMotors),
             vStrInput[1].length())
  };

  std::copy_n(vStrInput[1].begin(), uiMotorLength, pckMove.arrChMotors);

  pckMove.flPosition = static_cast<protocol::Float>(std::atof(vStrInput[2].c_str()));

  CRobot::instance()->getSerial()->write(protocol::ECommand::kMove, pckMove);
}

/**
 * 
 */
bool CCommandMove::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 3)
  {
    return false;
  }

  if (vStrInput[1].length() > sizeof(protocol::packet::CMove::arrChMotors))
  {
    return false;
  }

  // {@link std::from_chars} doesn't appear to have a float implementation
  // yet...
  char *szEnd{ nullptr };

  if (std::strtod(vStrInput[2].c_str(), &szEnd); szEnd == vStrInput[2].c_str())
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandMove::~CCommandMove(void)
{
}