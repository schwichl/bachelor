#if !defined(HPPcommands_CCommandView)
#define HPPcommands_CCommandView

#include "CCommand.hpp"

class CCommandView : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandView(void);
};

#endif