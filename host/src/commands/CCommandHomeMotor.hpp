#if !defined(HPPcommands_CCommandHomeMotor)
#define HPPcommands_CCommandHomeMotor

#include "CCommand.hpp"

class CCommandHomeMotor : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandHomeMotor(void);
};

#endif