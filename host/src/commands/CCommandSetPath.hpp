#if !defined(HPPcommands_CCommandSetPath)
#define HPPcommands_CCommandSetPath

#include "CCommand.hpp"

class CCommandSetPath : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  /**
   * 
   */
  virtual ~CCommandSetPath(void);
};

#endif