#if !defined(HPPcommands_CCommandSnap)
#define HPPcommands_CCommandSnap

#include "CCommand.hpp"

class CCommandSnap : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandSnap(void);
};

#endif