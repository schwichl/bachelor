#include "CCommandPing.hpp"

#include "robot/CRobot.hpp"

/**
 * 
 */
const std::string &CCommandPing::getSynopsis(void) const
{
  return this->m_strName;
}

/**
 * 
 */
void CCommandPing::execute(const std::vector<std::string> &)
{
  CRobot::instance()->getSerial()->write(protocol::ECommand::kPing, '\0');
}

/**
 * 
 */
bool CCommandPing::checkArguments(const std::vector<std::string> &vStrInput) const
{
  return (vStrInput.size() == 1);
}

/**
 * 
 */
CCommandPing::~CCommandPing(void)
{

}