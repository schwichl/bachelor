#if !defined(HPPcommands_CCommandAlign)
#define HPPcommands_CCommandAlign

#include "CCommand.hpp"

class CCommandAlign : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandAlign(void);
};

#endif