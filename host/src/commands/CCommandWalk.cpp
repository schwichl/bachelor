#include "CCommandWalk.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CTurnMotor.hpp"

#include <cstdlib>

/**
 * 
 */
const std::string &CCommandWalk::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " distance" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandWalk::execute(const std::vector<std::string> &vStrInput)
{
  protocol::Float flDistance{ std::strtof(vStrInput[1].c_str(), nullptr) };

  CRobot::instance()->getSerial()->write(protocol::ECommand::kWalk, flDistance);
}

/**
 * 
 */
bool CCommandWalk::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 2)
  {
    return false;
  }

  char *szEnd{ nullptr };

  if (std::strtof(vStrInput[1].c_str(), &szEnd); szEnd == vStrInput[1].c_str())
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandWalk::~CCommandWalk(void)
{
}