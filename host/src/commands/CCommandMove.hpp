#if !defined(HPPcommands_CCommandMove)
#define HPPcommands_CCommandMove

#include "CCommand.hpp"

class CCommandMove : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandMove(void);
};

#endif