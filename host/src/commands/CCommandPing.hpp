#if !defined(HPPcommands_CCommandPing)
#define HPPcommands_CCommandPing

#include "CCommand.hpp"

class CCommandPing : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandPing(void);
};

#endif