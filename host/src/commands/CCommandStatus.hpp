#if !defined(HPPcommands_CCommandStatus)
#define HPPcommands_CCommandStatus

#include "CCommand.hpp"

class CCommandStatus : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  /**
   * 
   */
  virtual ~CCommandStatus(void);
};

#endif