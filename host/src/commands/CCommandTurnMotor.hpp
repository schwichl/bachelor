#if !defined(HPPcommands_CCommandTurnMotor)
#define HPPcommands_CCommandTurnMotor

#include "CCommand.hpp"

class CCommandTurnMotor : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  virtual ~CCommandTurnMotor(void);
};

#endif