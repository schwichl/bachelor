#include "CCommandStatus.hpp"

#include "robot/CRobot.hpp"

#include <string>

/**
 * 
 */
const std::string &CCommandStatus::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandStatus::execute(const std::vector<std::string> &)
{
  CRobot::instance()->getSerial()->write(protocol::ECommand::kStatus, '\0');
}

/**
 * 
 */
bool CCommandStatus::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 1)
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandStatus::~CCommandStatus(void)
{
}