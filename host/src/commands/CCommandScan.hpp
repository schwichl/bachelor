#if !defined(HPPcommands_CCommandScan)
#define HPPcommands_CCommandScan

#include "CCommand.hpp"

class CCommandScan : public CCommand
{
  /**
   * @override
   */
  virtual const std::string &getSynopsis(void) const override;

  /**
   * @override
   */
  virtual void execute(const std::vector<std::string> &vStrInput) override;

  /**
   * @override
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const override;

  using CCommand::CCommand;

public:

  /**
   * 
   */
  virtual ~CCommandScan(void);
};

#endif