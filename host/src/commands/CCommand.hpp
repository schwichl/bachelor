#if !defined(HPPcommands_CCommand)
#define HPPcommands_CCommand

#include <string>
#include <vector>

class CCommand
{
private:
protected:
  /**
   * Lower case name.
   */
  std::string m_strName{};

public:
  /**
   * @return Usage string
   */
  virtual const std::string &getSynopsis(void) const = 0;

  /**
   * Executes the command
   */
  virtual void execute(const std::vector<std::string> &vStrInput) = 0;

  /**
   * @param vStrInput Space separated arguments. vStrInput[0] is the command.
   * @return True if the given arguments are valid.
   */
  virtual bool checkArguments(const std::vector<std::string> &vStrInput) const = 0;

  /**
   * @param strInput The first word (text before the first space) of the input.
   * @return True if this command is responsible for @strInput
   */
  virtual bool checkCommand(const std::string &strInput) const;

public:

  /**
   * Command name
   */
  CCommand(const std::string &strName);

  /**
   * 
   */
  virtual ~CCommand(void);
};

#endif