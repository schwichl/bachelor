#include "CCommandScan.hpp"

#include "robot/CRobot.hpp"

#include <string>

/**
 * 
 */
const std::string &CCommandScan::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandScan::execute(const std::vector<std::string> &)
{
  CRobot::instance()->scan();
}

/**
 * 
 */
bool CCommandScan::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 1)
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandScan::~CCommandScan(void)
{
}