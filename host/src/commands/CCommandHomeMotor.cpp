#include "CCommandHomeMotor.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CHomeMotor.hpp"

#include <algorithm>
#include <string>
#include <cstring>

/**
 * 
 */
const std::string& CCommandHomeMotor::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " motors" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandHomeMotor::execute(const std::vector<std::string>& vStrInput)
{
  protocol::packet::CHomeMotor pckHome{};

  if (vStrInput.size() > 1)
  {
    auto uiMotorLength{
      std::min(sizeof(protocol::packet::CHomeMotor::arrChMotors),
              vStrInput[1].length())
    };

    std::copy_n(vStrInput[1].begin(), uiMotorLength, pckHome.arrChMotors);
  }

  CRobot::instance()->getSerial()->write(protocol::ECommand::kHomeMotor, pckHome);
}

/**
 * 
 */
bool CCommandHomeMotor::checkArguments(const std::vector<std::string>& vStrInput) const
{
  if (vStrInput.size() > 2)
  {
    return false;
  }

  if (vStrInput.size() > 1)
  {
    if (vStrInput[1].length() > sizeof(protocol::packet::CHomeMotor::arrChMotors))
    {
      return false;
    }
  }

  return true;
}

/**
 * 
 */
CCommandHomeMotor::~CCommandHomeMotor(void)
{
}