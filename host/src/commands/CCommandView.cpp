#include "CCommandView.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CView.hpp"

#include <cstring>

/**
 * 
 */
const std::string &CCommandView::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " x y z t p" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandView::execute(const std::vector<std::string> &vStrInput)
{
  protocol::packet::CView pckView{};

  pckView.flX = std::strtof(vStrInput[1].c_str(), nullptr);
  pckView.flY = std::strtof(vStrInput[2].c_str(), nullptr);
  pckView.flZ = std::strtof(vStrInput[3].c_str(), nullptr);
  pckView.flTilt = std::strtof(vStrInput[4].c_str(), nullptr);
  pckView.flPan = std::strtof(vStrInput[5].c_str(), nullptr);

  CRobot::instance()->getSerial()->write(protocol::ECommand::kView, pckView);
}

/**
 * 
 */
bool CCommandView::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 6)
  {
    return false;
  }

  for (auto it{ vStrInput.begin() + 1 }; it != vStrInput.end(); ++it)
  {
    const std::string& strInput{ *it };
    char *szEnd{ nullptr };

    if (std::strtof(strInput.c_str(), &szEnd); szEnd == strInput.c_str())
    {
      return false;
    }
  }

  return true;
}

/**
 * 
 */
CCommandView::~CCommandView(void)
{
}