#include "CCommand.hpp"

/**
 * 
 */
bool CCommand::checkCommand(const std::string &strInput) const
{
  return (strInput == this->m_strName);
}

/**
 * 
 */
CCommand::CCommand(const std::string &strName)
    : m_strName{ strName }
{
}

/**
 * 
 */
CCommand::~CCommand(void)
{
}