#include "CCommandSnap.hpp"

#include "robot/CRobot.hpp"

/**
 * 
 */
const std::string &CCommandSnap::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " <snap>" };
  return s_strSynopsis;
}

/**
 * 
 */
void CCommandSnap::execute(const std::vector<std::string> &vStrInput)
{
  CRobot::instance()->getSerial()->write(
    protocol::ECommand::kSnap,
    protocol::Bool{ vStrInput.size() == 2 });
}

/**
 * 
 */
bool CCommandSnap::checkArguments(const std::vector<std::string> &vStrInput) const
{
  switch (vStrInput.size())
  {
    case 1:
      return true;
    case 2:
      return (vStrInput[1] == "stack");
    default:
      return false;
  }
}

/**
 * 
 */
CCommandSnap::~CCommandSnap(void)
{

}