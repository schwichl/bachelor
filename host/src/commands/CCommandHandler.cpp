#include "CCommandHandler.hpp"

#include "CCommandAlign.hpp"
#include "CCommandHomeMotor.hpp"
#include "CCommandMove.hpp"
#include "CCommandPing.hpp"
#include "CCommandScan.hpp"
#include "CCommandSnap.hpp"
#include "CCommandStatus.hpp"
#include "CCommandSetPath.hpp"
#include "CCommandTurnMotor.hpp"
#include "CCommandView.hpp"
#include "CCommandWalk.hpp"

#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>

/**
 * 
 */
CCommandHandler* CCommandHandler::instance(void)
{
  static auto s_pInstance{ std::make_unique<CCommandHandler>() };

  return s_pInstance.get();
}

/**
 * 
 */
std::vector<std::string> CCommandHandler::splitInput(const std::string& strInput) const
{
  std::stringstream ssInput{ strInput };
  std::vector<std::string> vStrArgv{};

  std::string strPart;

  while (ssInput >> strPart)
  {
    vStrArgv.push_back(strPart);
  }

  return vStrArgv;
}

/**
 * 
 */
CCommand* CCommandHandler::findCommand(const std::string& strName)
{
  auto pFound{ std::find_if(this->m_vCommands.begin(),
                            this->m_vCommands.end(),
                            [&](const CCommand* pCommand) {
                              return pCommand->checkCommand(strName);
                            }) };

  if (pFound == this->m_vCommands.end())
  {
    return nullptr;
  }
  else
  {
    return *pFound;
  }
}

/**
 * 
 */
void CCommandHandler::processInput(const std::string& strInput)
{
  auto vStrArgv{ this->splitInput(strInput) };

  // @vStrArgv.size() is at least 1, because @strInput isn't empty.

  const std::string& strCommand{ vStrArgv[0] };

  if (strCommand == "help")
  {
    for (const auto *pCommand : this->m_vCommands)
    {
      std::cout << pCommand->getSynopsis() << '\n';
    }
  }
  else
  {
    if (auto pCommand{ this->findCommand(strCommand) })
    {
      if (pCommand->checkArguments(vStrArgv))
      {
        pCommand->execute(vStrArgv);
      }
      else
      {
        std::cout << "usage: " << pCommand->getSynopsis() << '\n';
      }
    }
    else
    {
      std::cout << "no such command " << strCommand << '\n';
    }
  }
}

/**
 * 
 */
CCommandHandler::CCommandHandler(void)
{
  this->m_vCommands.push_back(new CCommandAlign{ "align" });
  this->m_vCommands.push_back(new CCommandHomeMotor{ "home" });
  this->m_vCommands.push_back(new CCommandSetPath{ "model" });
  this->m_vCommands.push_back(new CCommandMove{ "move" });
  this->m_vCommands.push_back(new CCommandPing{ "ping" });
  this->m_vCommands.push_back(new CCommandScan{ "scan" });
  this->m_vCommands.push_back(new CCommandSnap{ "snap" });
  this->m_vCommands.push_back(new CCommandStatus{ "status" });
  this->m_vCommands.push_back(new CCommandTurnMotor{ "turn" });
  this->m_vCommands.push_back(new CCommandView{ "view" });
  this->m_vCommands.push_back(new CCommandWalk{ "walk" });
}

/**
 * 
 */
CCommandHandler::~CCommandHandler(void)
{
  for (auto& pCommand : this->m_vCommands)
  {
    delete pCommand;
    pCommand = nullptr;
  }
}