#include "CCommandTurnMotor.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CTurnMotor.hpp"

#include <charconv>
#include <cstring>
#include <sstream>

/**
 * 
 */
const std::string &CCommandTurnMotor::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " motors revolutions/degrees" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandTurnMotor::execute(const std::vector<std::string> &vStrInput)
{
  protocol::packet::CTurnMotor pckTurn{};

  auto uiMotorLength{
    std::min(sizeof(protocol::packet::CTurnMotor::arrChMotors),
             vStrInput[1].length())
  };

  std::copy_n(vStrInput[1].begin(), uiMotorLength, pckTurn.arrChMotors);

  pckTurn.flRevolutions = static_cast<protocol::Float>(std::atof(vStrInput[2].c_str()));

  CRobot::instance()->getSerial()->write(protocol::ECommand::kTurnMotor, pckTurn);
}

/**
 * 
 */
bool CCommandTurnMotor::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 3)
  {
    return false;
  }

  if (vStrInput[1].length() > sizeof(protocol::packet::CTurnMotor::arrChMotors))
  {
    return false;
  }

  // {@link std::from_chars} doesn't appear to have a float implementation
  // yet...
  char *szEnd{ nullptr };

  if (std::strtod(vStrInput[2].c_str(), &szEnd); szEnd == vStrInput[2].c_str())
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandTurnMotor::~CCommandTurnMotor(void)
{
}