#include "CCommandSetPath.hpp"

#include "robot/CRobot.hpp"
#include "model/COBJParser.hpp"

#include <cstring>
#include <filesystem>

/**
 * 
 */
const std::string &CCommandSetPath::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " model.obj" };

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandSetPath::execute(const std::vector<std::string> &vStrInput)
{
  if (auto mdlObject{ COBJParser::instance()->fromFile(vStrInput[1], 1000.0f) })
  {
    if (!CRobot::instance()->setTarget(*mdlObject))
    {
      std::cout << "couldn't set target\n";
    }
    else
    {
      std::cout << "target set: " << CRobot::instance()->getPath().size() << " nodes\n";
    }
  }
  else
  {
    std::cout << "couldn't load model. make sure it's triangulated.\n";
  }
}

/**
 * 
 */
bool CCommandSetPath::checkArguments(const std::vector<std::string> &vStrInput) const
{
  if (vStrInput.size() != 2)
  {
    return false;
  }

  if (!std::filesystem::exists(vStrInput[1]))
  {
    return false;
  }

  return true;
}

/**
 * 
 */
CCommandSetPath::~CCommandSetPath(void)
{
}