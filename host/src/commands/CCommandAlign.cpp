#include "CCommandAlign.hpp"

#include "robot/CRobot.hpp"

#include "protocol/packet/CView.hpp"

#include <cstring>

/**
 * 
 */
const std::string &CCommandAlign::getSynopsis(void) const
{
  static std::string s_strSynopsis{ this->m_strName + " [set]"};

  return s_strSynopsis;
}

/**
 * 
 */
void CCommandAlign::execute(const std::vector<std::string>& vStrInput)
{
  if (vStrInput.size() == 2)
  {
    // Parameter was verified in {@link checkArguments}
    CRobot::instance()->setAlign();
  }
  else
  {
    CRobot::instance()->align();
  }
}

/**
 * 
 */
bool CCommandAlign::checkArguments(const std::vector<std::string>& vStrInput) const
{
  switch (vStrInput.size())
  {
    case 1:
      return true;
    case 2:
      return (vStrInput[1] == "set");
    default:
      return false;
  }
}

/**
 * 
 */
CCommandAlign::~CCommandAlign(void)
{
}