#if !defined(HPPcommand_CCommandHandler)
#define HPPcommand_CCommandHandler

#include "CCommand.hpp"

#include <vector>

class CCommandHandler
{
public:
  /**
   * 
   */
  static CCommandHandler *instance(void);

private:
  /**
   * 
   */
  std::vector<CCommand *> m_vCommands{};

private:

  /**
   * Splits the input at spaces.
   */
  std::vector<std::string> splitInput(const std::string &strInput) const;

  /**
   * Searches for the command with the given name.
   */
  CCommand *findCommand(const std::string &strName);

public:
  /**
   * Processes input from the command line. Errors are printed to
   * {@link stdout}.
   * @param strInput Raw input from the command line. Must not be empty!
   */
  void processInput(const std::string &strInput);

public:
  /**
   * 
   */
  CCommandHandler(void);

  /**
   * 
   */
  ~CCommandHandler(void);
};

#endif