#include "CPly.hpp"

#include <ostream>

std::ostream& operator<<(std::ostream& s, const CPly& ply)
{
  s << "ply\n"
       "format ascii 1.0\n"
       "element vertex "
    << ply.m_vVecVertexes.size() << '\n'
    << "property float x\n"
       "property float y\n"
       "property float z\n";

  if (!ply.m_vIVertexIndexes.empty())
  {
    s << "element face " << (ply.m_vIVertexIndexes.size() / 4) << '\n';
    s << "property list uchar int vertex_index\n";
  }

  s << "end_header\n";

  for (const auto& vecVertex : ply.m_vVecVertexes)
  {
    s << vecVertex[0] << ' ' << vecVertex[1] << ' ' << vecVertex[2] << '\n';
  }

  for (int i{ 0 }; i < static_cast<int>(ply.m_vIVertexIndexes.size()); i += 4)
  {
    s << "4 ";

    for (int j{ 0 }; j < 4; ++j)
    {
      s << ply.m_vIVertexIndexes[static_cast<decltype(ply.m_vIVertexIndexes)::size_type>(i + j)] << ' ';
    }

    s << '\n';
  }

  return s;
}

/**
 * 
 */
void CPly::addVertex(const CVector3f& vec)
{
  this->m_vVecVertexes.push_back(vec);
}

/**
 * 
 */
void CPly::addFace(const CVector3f& vecA, const CVector3f& vecB, const CVector3f& vecC, const CVector3f& vecD)
{
  auto iBaseIndex{ static_cast<int>(this->m_vVecVertexes.size()) };

  this->m_vVecVertexes.push_back(vecA);
  this->m_vVecVertexes.push_back(vecB);
  this->m_vVecVertexes.push_back(vecC);
  this->m_vVecVertexes.push_back(vecD);


  this->m_vIVertexIndexes.push_back(iBaseIndex);
  this->m_vIVertexIndexes.push_back(iBaseIndex + 1);
  this->m_vIVertexIndexes.push_back(iBaseIndex + 2);
  this->m_vIVertexIndexes.push_back(iBaseIndex + 3);
}

/**
 * 
 */
void CPly::addFace(const CVector3f& vecA, const CVector3f& vecB, const CVector3f& vecC)
{
  this->addFace(vecA, vecB, vecC, vecC);
}