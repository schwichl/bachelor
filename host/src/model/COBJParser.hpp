#if !defined(HPPmodel_COBJParser)
#define HPPmodel_COBJParser

#include "CModel.hpp"

#include <optional>

/**
 * .obj parser.
 * Only supports 3-vertex faces.
 */
class COBJParser
{
private:
  COBJParser(void) {}

public:
  /**
   * Singleton
   */
  static COBJParser *instance(void);

  /**
   * 
   */
  std::optional<CModel> fromFile(const std::string &strPath, float flScale = 1.0f) const;
};

#endif