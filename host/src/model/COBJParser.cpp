#include "COBJParser.hpp"

#include "CVector.hpp"

#include <array>
#include <fstream>
#include <limits>
#include <vector>

#include <iostream>

/**
 * 
 */
COBJParser *COBJParser::instance(void)
{
  static COBJParser s_instance{};

  return &s_instance;
}

/**
 * 
 */
std::optional<CModel> COBJParser::fromFile(const std::string &strPath, float flScale) const
{
  std::ifstream ifs{ strPath };

  if (ifs.is_open())
  {
    std::vector<CVector3f> vVertexes{};
    std::vector<CVector3f> vNormals{};
    std::vector<std::array<std::array<int, 2>, 3>> vFaces{};

    std::string strCommand{};

    // This doesn't leverage the obj's layout, but we're on the host, so we
    // don't need high efficiency.
    while (ifs >> strCommand)
    {
      if (strCommand == "v")
      {
        CVector3f vecVertex;

        ifs >> vecVertex[0] >> vecVertex[1] >> vecVertex[2];

        vVertexes.push_back(vecVertex * flScale);
      }
      else if (strCommand == "vn")
      {
        CVector3f vecNormal;

        ifs >> vecNormal[0] >> vecNormal[1] >> vecNormal[2];

        vNormals.push_back(vecNormal);
      }
      else if (strCommand == "f")
      {
        std::array<std::array<int, 2>, 3> arrFace{};

        for (int i{ 0 }; i < 3; ++i)
        {
          std::array<int, 2> arrVertex{};

          ifs >> arrVertex[0];
          ifs.ignore();

          // Skip texture index
          ifs.ignore(std::numeric_limits<std::streamsize>::max(), '/');

          ifs >> arrVertex[1];

          // obj starts indexing at 1
          --arrVertex[0];
          --arrVertex[1];

          arrFace[static_cast<std::size_t>(i)] = arrVertex;
        }

        if (ifs.peek() != '\n')
        {
          // More than 3 vertexes per face
          return {};
        }

        vFaces.push_back(arrFace);
      }
      else
      {
        ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      }
    }

    CModel mdlObject{
      static_cast<int>(vVertexes.size()),
      static_cast<int>(vNormals.size()),
      static_cast<int>(vFaces.size())
    };

    for (int i{ 0 }; i < mdlObject.getVertexCount(); ++i)
    {
      mdlObject.setVertex(i, vVertexes[static_cast<std::size_t>(i)]);
    }

    for (int i{ 0 }; i < mdlObject.getNormalCount(); ++i)
    {    
      mdlObject.setNormal(i, vNormals[static_cast<std::size_t>(i)]);
    }

    for (int i{ 0 }; i < mdlObject.getTriangleCount(); ++i)
    {
      auto arrFace{ vFaces[static_cast<std::size_t>(i)] };
      mdlObject.setTriangle(i, CModel::STriangle{
                                   { arrFace[0][0], arrFace[1][0], arrFace[2][0] },
                                   { arrFace[0][1], arrFace[1][1], arrFace[2][1] } });
    }

    ifs.close();

    return mdlObject;
  }
  else
  {
    return {};
  }
}