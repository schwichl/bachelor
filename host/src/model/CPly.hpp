#if !defined(HPPmodel_CPly)
#define HPPmodel_CPly

#include "CVector.hpp"

#include <ostream>
#include <vector>

class CPly
{
  friend std::ostream& operator<<(std::ostream& s, const CPly& ply);

private:

  /**
   * 
   */
  std::vector<CVector3f> m_vVecVertexes{};

  /**
   * 
   */
  std::vector<int> m_vIVertexIndexes{};

public:

  /**
   * 
   */
  void addVertex(const CVector3f& vec);

  /**
   * 
   */
  void addFace(const CVector3f& vecA, const CVector3f& vecB, const CVector3f& vecC, const CVector3f& vecD);
  void addFace(const CVector3f& vecA, const CVector3f& vecB, const CVector3f& vecC);

};

#endif