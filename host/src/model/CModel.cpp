#include "CModel.hpp"

#include <atomic>
#include <algorithm>
#include <cmath>
#include <execution>
#include <limits>

/**
 * 
 */
int CModel::getVertexCount(void) const
{
  return this->m_iVertexCount;
}

/**
 * 
 */
const CVector3f* CModel::getVertexes(void) const
{
  return this->m_arrVertexes;
}

/**
 * 
 */
int CModel::getNormalCount(void) const
{
  return this->m_iNormalCount;
}

/**
 * 
 */
const CVector3f* CModel::getNormals(void) const
{
  return this->m_arrNormals;
}

/**
 * 
 */
int CModel::getTriangleCount(void) const
{
  return this->m_iTriangleCount;
}

const int* CModel::getTriangleVertex(int iIndex) const
{
  return this->m_arrTriangles[iIndex].arrIVertexIndexes;
}

/**
 * 
 */
const int* CModel::getTriangleNormal(int iIndex) const
{
  return this->m_arrTriangles[iIndex].arrINormalIndexes;
}

/**
 * 
 */
void CModel::setVertex(int iIndex, const CVector3f& vecVertex)
{
  this->m_arrVertexes[iIndex] = vecVertex;
}

/**
 * 
 */
void CModel::setNormal(int iIndex, const CVector3f& vecNormal)
{
  this->m_arrNormals[iIndex] = vecNormal;
}

/**
 * 
 */
void CModel::setTriangle(int iIndex, const STriangle& triangle)
{
  this->m_arrTriangles[iIndex] = triangle;
}

/**
 * 
 */
void CModel::move(const CVector3f& vecOffset)
{
  std::for_each(std::execution::unseq,
                this->m_arrVertexes, this->m_arrVertexes + this->m_iVertexCount,
                [vecOffset](CVector3f& vertex) {
                  vertex += vecOffset;
                });
}

/**
 * 
 */
void CModel::getAABB(CVector3f* pVecMin, CVector3f* pVecMax) const
{
  for (int i{ 0 }; i < 3; ++i)
  {
    pVecMin->operator[](i) = 1.0f;
    pVecMax->operator[](i) = -1.0f;
  }

  for (int i{ 0 }; i < this->m_iVertexCount; ++i)
  {
    const CVector3f& vecVertex{ this->m_arrVertexes[i] };

    for (int j{ 0 }; j < 3; ++j)
    {
      if (vecVertex[j] < pVecMin->operator[](j))
      {
        pVecMin->operator[](j) = vecVertex[j];
      }

      if (vecVertex[j] > pVecMax->operator[](j))
      {
        pVecMax->operator[](j) = vecVertex[j];
      }
    }
  }
}

/**
 * 
 */
bool CModel::trace(const CVector3f& vecStart, const CVector3f& vecDirection, STraceHit* pHit) const
{
  struct SVertex
  {
    const CVector3f& vecOrigin;
    const CVector3f& vecNormal;
  };

  // Bildsynthese 2.1 Thorsten Thormaehlen

  constexpr float k_flEpsilon{ std::numeric_limits<float>::epsilon() };

  std::atomic<float> flClosestIntersection{ 0.0f };

  std::for_each_n(std::execution::par_unseq, this->m_arrTriangles, this->m_iTriangleCount, [&](const STriangle& triangle)
  {
    // Old code used a pointer.
    const STriangle* pTriangle{ &triangle };
    const int* arrVertexIndexes{ pTriangle->arrIVertexIndexes };
    const int* arrNormalIndexes{ pTriangle->arrINormalIndexes };

    SVertex vertA{
      this->m_arrVertexes[arrVertexIndexes[0]],
      this->m_arrNormals[arrNormalIndexes[0]]
    };

    SVertex vertB{
      this->m_arrVertexes[arrVertexIndexes[1]],
      this->m_arrNormals[arrNormalIndexes[1]]
    };

    SVertex vertC{
      this->m_arrVertexes[arrVertexIndexes[2]],
      this->m_arrNormals[arrNormalIndexes[2]]
    };

    CVector3f vecAB{ vertB.vecOrigin - vertA.vecOrigin };
    CVector3f vecAC{ vertC.vecOrigin - vertA.vecOrigin };
    CVector3f vecOffset{ vecStart - vertA.vecOrigin };

    CVector3f vecDirectionCrossAC{ vecDirection.cross(vecAC) };
    float flDirectionCrossACDotU{ vecDirectionCrossAC.dot(vecAB) };

    // if (std::abs(flDirectionCrossACDotU < k_flEpsilon))
    if (std::abs(flDirectionCrossACDotU) < 0.0f)
    {
      return;
    }

    float flInverse{ 1.0f / flDirectionCrossACDotU };

    float flS{ vecDirectionCrossAC.dot(vecOffset) * flInverse };

    if ((flS < 0.0f) || (flS > 1.0f))
    {
      return;
    }

    CVector3f vecOffsetCrossAB{ vecOffset.cross(vecAB) };

    float flT{ vecOffsetCrossAB.dot(vecDirection) * flInverse };

    if ((flT < 0.0f) || (flT > 1.0f))
    {
      return;
    }

    if ((flS + flT) > 1.0f)
    {
      return;
    }

    float flR{ vecOffsetCrossAB.dot(vecAC) * flInverse };

    if (flR < k_flEpsilon)
    {
      return;
    }

    if ((flClosestIntersection == 0.0f) || (flR < flClosestIntersection))
    {
      flClosestIntersection = flR;

      CVector3f vecHit{ vecDirection * flR };
      CVector3f vecAHit{ vecHit - vertA.vecOrigin };

      pHit->vecOrigin = (vecStart + vecHit);

      float flArea{ vecAB.cross(vecAC).length() };
      float flProportionAreaHitB{ vecAB.cross(vecAHit).length() / flArea };
      float flProportionAreaHitC{ vecAHit.cross(vecAC).length() / flArea };
      float flProportionAreaRemaining{ 1.0f - (flProportionAreaHitB + flProportionAreaHitC) };

      pHit->vecNormal = ((vertA.vecNormal * flProportionAreaRemaining) +
                         (vertB.vecNormal * flProportionAreaHitC) +
                         (vertC.vecNormal * flProportionAreaHitB))
                            .normalize();
    }
  });

  return (flClosestIntersection != 0.0f);
}

/**
 * 
 */
CModel::CModel(int iVertexCount, int iNormalCount, int iTriangleCount)
    : m_arrVertexes{ new CVector3f[static_cast<size_t>(iVertexCount)] },
      m_iVertexCount{ iVertexCount },
      m_arrNormals{ new CVector3f[static_cast<size_t>(iNormalCount)] },
      m_iNormalCount{ iNormalCount },
      m_arrTriangles{ new STriangle[static_cast<size_t>(iTriangleCount)] },
      m_iTriangleCount{ iTriangleCount }
{
}

/**
 * 
 */
CModel::CModel(void)
{
}