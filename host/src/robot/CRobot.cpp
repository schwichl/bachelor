#include "CRobot.hpp"

#include "scanconfig.hpp"
#include "shared_prefs.hpp"
#include "CDeformer.hpp"

#include "protocol/packet/CGetPathSegment.hpp"
#include "protocol/packet/CPosition.hpp"
#include "protocol/packet/CSetAngles.hpp"
#include "protocol/packet/CSetPath.hpp"
#include "protocol/packet/CUploadPath.hpp"
#include "protocol/packet/CVerifyProtocol.hpp"
#include "protocol/protocol.hpp"

#include "serial/CSerial.hpp"

#include <algorithm>
#include <chrono>
#include <execution>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>

/**
 * 
 */
CRobot* CRobot::instance(void)
{
  static auto s_pInstance{ std::make_unique<CRobot>() };

  return s_pInstance.get();
}

/**
 * 
 */
void CRobot::onImageCaptured(int iNodeId)
{
  if (!this->m_ofsImageLog.is_open())
  {
    this->m_ofsImageLog.open("images.log");

    if (!this->m_ofsImageLog.is_open())
    {
      std::cout << "couldn't open images.log for writing\n";
      return;
    }
  }

  const auto& node{ this->m_path[iNodeId] };
  const auto& vecOrigin{ node.getOrigin() };
  const auto& vecDirection{ node.getDirection() };

  auto time{ std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() };

  this->m_ofsImageLog << time << ' '
                      << vecOrigin[0] << ' '
                      << vecOrigin[1] << ' '
                      << vecOrigin[2] << ' '
                      << vecDirection[0] << ' '
                      << vecDirection[1] << ' '
                      << vecDirection[2] << std::endl; // @std::endl on purpose
}

/**
 * Sends an observer path segment to the Arduino.
 * @param iNodeStart See {@link protocol::packet::CGetPathSegment}
 * @param iNodeEnd See {@link protocol::packet::CGetPathSegment}
 */
void CRobot::sendPathSegment(int iNodeStart, int iNodeEnd)
{
  if (iNodeEnd < iNodeStart)
  {
    std::cout << "invalid segment requested: " << iNodeStart << " to " << iNodeEnd << '\n';
    return;
  }

  long iSegmentSize{ std::min((iNodeEnd - iNodeStart) + 1l, this->m_path.size()) };

  CObserverPath pathSegment{};

  pathSegment.resize(iSegmentSize);

  std::copy_n(std::execution::unseq, this->m_path.begin() + iNodeStart,
              iSegmentSize, pathSegment.begin());

  int iSerializedSegmentSize{};
  char* arrBufSegment{ protocol::packet::CUploadPath::serialize(pathSegment, &iSerializedSegmentSize) };

  if constexpr (true) // Verify that serialization is working
  {
    CObserverPath pathTest{ protocol::packet::CUploadPath::deserialize(arrBufSegment) };

    if (
        (pathTest.size() != pathSegment.size()) || // Size
        (pathTest[pathTest.size() / 2].getOrigin() != pathSegment[pathSegment.size() / 2].getOrigin()) // Some node's origin
    )
    {
      std::cout << "deserialization failed\n";
    }
  }

  this->m_pTTY->write(protocol::CPacket{
      protocol::ECommand::kUploadPath,
      arrBufSegment,
      iSerializedSegmentSize });

  std::cout << "sent path\n";

  delete[] arrBufSegment;
}

/**
 * 
 */
void CRobot::processPacket(const protocol::CPacket& pckReceived)
{
  switch (pckReceived.getCommand())
  {
  // kVerifyProtocol is handled by {@link connect}
  case protocol::ECommand::kPing:
    this->m_pTTY->write(protocol::ECommand::kPong, 0);
    break;
  case protocol::ECommand::kPong:
    std::cout << "arduino sent pong\n";
    break;
  case protocol::ECommand::kRequestConfirmation:
  {
    std::cout << "(y/n):";

    char ch{};
    std::cin >> ch;
    std::cin.ignore(1, '\n');

    this->m_pTTY->write(
        (ch == 'y') ? protocol::ECommand::kConfirmationAllow : protocol::ECommand::kConfirmationDeny,
        '\0');
    break;
  }
  case protocol::ECommand::kPrint:
    std::cout << "(arduino) " << pckReceived.getPayload<char>() << '\n';
    break;
  case protocol::ECommand::kImageCaptured:
    this->onImageCaptured(static_cast<int>(*pckReceived.getPayload<protocol::Int32>()));
    break;
  case protocol::ECommand::kPosition:
  {
    // We're only using the command for alignment.
    auto pPckPosition{ pckReceived.getPayload<protocol::packet::CPosition>() };

    CVector3f vecPosition{
      pPckPosition->flX,
      pPckPosition->flY,
      pPckPosition->flZ
    };

    this->setAlign(&vecPosition);

    break;
  }
  case protocol::ECommand::kUploadPathSuccess:
    std::cout << "path upload was successful\n";
    break;
  case protocol::ECommand::kUploadPathFailure:
    std::cout << "path upload failed\n";
    break;
  case protocol::ECommand::kGetPathSegment:
  {
    auto pPckGetPathSegment{ pckReceived.getPayload<protocol::packet::CGetPathSegment>() };
    std::cout << "requested segment " << pPckGetPathSegment->iNodeStart << " to " << pPckGetPathSegment->iNodeEnd << '\n';
    this->sendPathSegment(pPckGetPathSegment->iNodeStart, pPckGetPathSegment->iNodeEnd);
    break;
  }
  case protocol::ECommand::kScanComplete:
    this->m_bScanning = false;
    std::cout << "scan complete.\n";
    break;
  default:
    std::cerr << "unhandled command " << static_cast<int>(pckReceived.getCommand()) << '\n';
    break;
  }
}

/**
 * 
 */
const CSerial* CRobot::getSerial(void) const
{
  return this->m_pTTY.get();
}

/**
 * 
 */
CSerial* CRobot::getSerial(void)
{
  return this->m_pTTY.get();
}

/**
 * 
 */
const char* CRobot::getSerialPort(void) const
{
  return this->m_szSerialPort;
}

/**
 * 
 */
bool CRobot::listenOnce(void)
{
  if (auto pPckReceived{ this->m_pTTY->readPacket() })
  {
    this->processPacket(*pPckReceived);
  }
  else
  {
    if (std::get<CSerial::EError>(this->m_pTTY->getLastError()) != CSerial::EError::kSuccess)
    {
      std::cerr << "error reading next packet" << '\n';
      std::cerr << this->m_pTTY->getLastErrorString() << '\n';

      return false;
    }
  }

  return true;
}

/**
 * 
 */
bool CRobot::listenAll(void)
{
  // This could lead to partial reads.
  while (this->m_pTTY->available() > 0)
  {
    if (!this->listenOnce())
    {
      return false;
    }
  }

  return true;
}

/**
 * 
 */
bool CRobot::connect(const char* szSerialPort)
{
  this->m_szSerialPort = szSerialPort;

  if (this->m_pTTY->open(szSerialPort))
  {
    // Arduino mega restarts on connection.
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // Wake up the arduino
    if (!this->m_pTTY->write("\0", 1))
    {
      std::cerr << "couldn't wake up arduino\n";
      std::cerr << this->m_pTTY->getLastErrorString() << '\n';
      return false;
    }

    if (auto pckVerification{ this->m_pTTY->readPacket() })
    {
      if (pckVerification->getCommand() == protocol::ECommand::kVerifyProtocol)
      {
        auto* pTest{ pckVerification->getPayload<protocol::packet::CVerifyProtocol>() };

        if (auto errVerify{ protocol::verify(*pTest) };
            errVerify == protocol::EVerificationError::kSuccess)
        {
          if (!this->m_pTTY->write(protocol::ECommand::kVerifyProtocolSuccess, '\0'))
          {
            std::cerr << "verification succeeded, but couldn't tell arduino\n";
            std::cerr << this->m_pTTY->getLastErrorString() << '\n';
            return false;
          }
          else
          {
            return true;
          }
        }
        else
        {
          this->m_pTTY->write(protocol::ECommand::kVerifyProtocolFailed, '\0');

          std::cerr << "verification failed\n";
          std::cerr << protocol::EVerificationErrorSTR[static_cast<int>(errVerify)] << '\n';

          if (errVerify == protocol::EVerificationError::kVersionMismatch)
          {
            std::cerr << "host version    " << protocol::k_iVersion << '\n';
            std::cerr << "arduino version " << pTest->iProtocolVersion << '\n';
          }

          this->m_pTTY->close();
          return false;
        }
      }
      else
      {
        std::cerr << "initial packet wasn't a verification packet\n";
        std::cerr << "was " << protocol::ECommandSTR[static_cast<int>(pckVerification->getCommand())] << '\n';
        this->m_pTTY->close();
        return false;
      }
    }
    else
    {
      if (std::get<CSerial::EError>(this->m_pTTY->getLastError()) != CSerial::EError::kSuccess)
      {
        std::cerr << "couldn't read verification packet " << szSerialPort << '\n';
        std::cerr << this->m_pTTY->getLastErrorString() << '\n';
      }
      else
      {
        std::cerr << "verification timed out\n";
      }

      this->m_pTTY->close();
      return false;
    }
  }
  else
  {
    std::cout << "couldn't open serial port " << szSerialPort << '\n';
    std::cout << this->m_pTTY->getLastErrorString() << '\n';
    return {};
  }
}

/**
 * 
 */
bool CRobot::disconnect(void)
{
  this->m_szSerialPort = nullptr;

  if (!this->m_pTTY->write(protocol::ECommand::kDisconnect, '\0'))
  {
    return false;
  }

  if (!this->m_pTTY->close())
  {
    return false;
  }

  return true;
}

/**
 * 
 */
bool CRobot::reset(void)
{
  const char* szSerialPort{ this->m_szSerialPort };

  if (!this->disconnect())
  {
    return false;
  }

  if (!this->connect(szSerialPort))
  {
    return false;
  }

  return true;
}

/**
 * 
 */
bool CRobot::setTarget(const CModel& model)
{
  this->m_bAligned = false;

  this->m_path = CObserverPath::fromModel(model, 30.0f, scanconfig::k_flScanDistance);

  this->m_path.generatePreviewPath("preview_path_unfiltered.ply", {}, 1.0f / 1000.0f);
  this->m_path.generatePreviewImages("preview_images_unfiltered.ply", {}, 1.0f / 1000.0f);

  // {@link CObserverPath::fromModel} centers the path at (0, 0, 0).
  for (auto& node : this->m_path)
  {
    node.shift(shared_prefs::k_vecLinearActuatorHalfRanges);
  }

  return true;
}

/**
 * 
 */
bool CRobot::isNodeReachable(const CObserverNode& node, CRobot::ECheckZ checkZ) const
{
  int iStart{ (checkZ == ECheckZ::kOnly) ? 2 : 0 };
  int iEnd{ ((checkZ == ECheckZ::kIgnore) ? 2 : 3) };

  const CVector3f &vecOrigin{ node.getHardwareOrigin() };

  for (int i{ iStart }; i < iEnd; ++i)
  {
    if ((vecOrigin[i] < 0.0f) ||
        (vecOrigin[i] > shared_prefs::k_vecLinearActuatorRanges[i]))
    {
      return false;
    }
  }

  CVector3f vecAngles{ node.getDirection().toAngles() };

  if ((vecAngles[0] < shared_prefs::k_flMinTilt) || (vecAngles[0] > shared_prefs::k_flMaxTilt))
  {
    return false;
  }

  if ((vecAngles[1] < shared_prefs::k_flMinPan) || (vecAngles[1] > shared_prefs::k_flMaxPan))
  {
    return false;
  }

  return true;
}

/**
 * Called without a position by the command.
 * Then called by {@link CRobot::processCommand} with a position.
 */
void CRobot::setAlign(const CVector3f* pVecPosition)
{
  if (!this->m_bAligning)
  {
    std::cout << "can't set alignment before 'align' is used.\n";
    return;
  }

  if (pVecPosition == nullptr)
  {
    this->m_pTTY->write(protocol::ECommand::kGetPosition, protocol::Bool{ true });
  }
  else
  {
    this->m_vecPathOffset = (*pVecPosition - this->m_vecAlignmentPoint);

    auto iOldSize{ this->m_path.size() };

    // Apply deformation
    for (auto& node : this->m_path)
    {
      node.setOrigin(
        node.getOrigin(),
        CDeformer::instance()->realizeOrigin(node.getOrigin(), node.getDirection()));
    }

    this->m_path.offsetAndFilterNodes(this->m_vecPathOffset);
    this->m_path.sortNodes();

    auto iNewSize{ this->m_path.size() };

    std::cout << "removed " << (iOldSize - iNewSize) << " node(s). "
              << iNewSize << " remaining\n";

    protocol::packet::CSetPath pckSetPath{};

    pckSetPath.iNodeCount = this->m_path.getNodeCount();

    this->m_pTTY->write(protocol::ECommand::kSetPath, pckSetPath);

    this->m_path.generatePreviewPath(
      "preview_path.ply",
      -this->m_vecPathOffset - shared_prefs::k_vecLinearActuatorHalfRanges,
      1.0f / 1000.0f);

    this->m_path.generatePreviewImages(
      "preview_images.ply",
      -this->m_vecPathOffset - shared_prefs::k_vecLinearActuatorHalfRanges,
      1.0f / 1000.0f);

    this->m_bAligned = true;
    this->m_bAligning = false;
  }
}

/**
 * 
 */
void CRobot::align(void)
{
  if (this->m_path.empty())
  {
    std::cout << "cannot align, path is empty.\n";
    return;
  }

  this->m_bAligning = true;

  // Node closest to (0, 0, 0), ignoring Z.
  CObserverNode* pAlignmentNode{
    std::min_element(std::execution::seq, this->m_path.begin(), this->m_path.end(), [](const auto& a, const auto& b) {
      float flA{
        CVector3f{
          a.getOrigin()[0] - shared_prefs::k_vecLinearActuatorHalfRanges[0],
          a.getOrigin()[1] - shared_prefs::k_vecLinearActuatorHalfRanges[1],
          0.0f
        }.lengthSqr()
      };
      
      float flB{
        CVector3f{
          b.getOrigin()[0] - shared_prefs::k_vecLinearActuatorHalfRanges[0],
          b.getOrigin()[1] - shared_prefs::k_vecLinearActuatorHalfRanges[1],
          0.0f
        }.lengthSqr()
      };

      return (flA < flB);
    })
  };

  CVector3f vecAlignmentPoint{ pAlignmentNode->getOrigin() };

  CVector3f vecPlySurfacePoint{
    (pAlignmentNode->getOrigin() + (pAlignmentNode->getDirection() * scanconfig::k_flScanDistance))
    - shared_prefs::k_vecLinearActuatorHalfRanges
  };

  this->m_vecAlignmentPoint = vecAlignmentPoint;

  CVector3f vecPlyAlignmentPoint{ vecAlignmentPoint - shared_prefs::k_vecLinearActuatorHalfRanges };

  CPly plyAlign{};

  plyAlign.addVertex(vecPlySurfacePoint / 1000.0f);
  plyAlign.addVertex(vecPlyAlignmentPoint / 1000.0f);

  std::ofstream ofsPreview{ "preview_align.ply" };
  ofsPreview << plyAlign;
  std::cout << "preview_align.ply written. please align and confirm with 'align set'.\n";

  CVector3f vecAngles{ pAlignmentNode->getDirection().toAngles() };

  this->m_pTTY->write(
      protocol::ECommand::kSetAngles,
      protocol::packet::CSetAngles{ vecAngles[0], vecAngles[1] });
}

/**
 * 
 */
void CRobot::scan(void)
{
  if (this->m_path.getNodeCount() == 0)
  {
    std::cout << "no path set, can't scan.\n";
    return;
  }

  if (!this->m_bAligned)
  {
    std::cout << "model is not aligned. Use 'align' and 'align set' first\n";
    return;
  }

  if (!this->m_pTTY->write(protocol::ECommand::kScan, '\0'))
  {
    std::cerr << "couldn't start scan\n";
    std::cerr << this->m_pTTY->getLastErrorString() << '\n';
    return;
  }

  this->m_bScanning = true;
}

/**
 * 
 */
const CObserverPath& CRobot::getPath(void) const
{
  return this->m_path;
}

/**
 * 
 */
CRobot::CRobot(void)
    : m_pTTY{ std::make_unique<CSerial>() }
{
}