#if !defined(HPProbot_CRobot)
#define HPProbot_CRobot

#include "serial/CSerial.hpp"

#include "model/CModel.hpp"

#include "CObserverPath.hpp"
#include "CObserverNode.hpp"

#include <fstream>
#include <memory>

enum class ECheckZ
{
  // Treat z like the other axes
  kNormal,
  // Ignore the z axis
  kIgnore,
  // Only check the z axis
  kOnly,
};

class CRobot
{
public:

  // Backwards compatibility. New code should use {@link ECheckZ}.
  using ECheckZ = ECheckZ;

public:

  /**
   * @return Instance
   */
  static CRobot *instance(void);

private:

  /**
   * Port we're connected to.
   */
  const char *m_szSerialPort{ nullptr };

  /**
   * Serial connection to talk to the robot.
   */
  std::unique_ptr<CSerial> m_pTTY{};

  /**
   * Observer path offset.
   */
  CVector3f m_vecPathOffset{};

  /**
   * The full observer path.
   */
  CObserverPath m_path{};

  /**
   * 
   */
  CVector3f m_vecAlignmentPoint{};

  /**
   * True if the model has been aligned. A scan cannot be started unless this is
   * true.
   */
  bool m_bAligned{ false };

  /**
   * True if we're currently aligning the robot.
   */
  bool m_bAligning{ false };

  /**
   * Scan in progress.
   */
  bool m_bScanning{ false };

  /**
   * 
   */
  std::ofstream m_ofsImageLog{};

private:

  /**
   * Fired when an image is taken.
   */
  void onImageCaptured(int iNodeId);

  /**
   * Sends an observer path segment to the Arduino.
   * @param iNodeStart See {@link protocol::packet::CGetPathSegment}
   * @param iNodeEnd See {@link protocol::packet::CGetPathSegment}
   */
  void sendPathSegment(int iNodeStart, int iNodeEnd);
  
  /**
   * Processes an incoming packet.
   */
  void processPacket(const protocol::CPacket &pckReceived);

public:

  /**
   * Only valid as long as the robot exists.
   */
  const CSerial *getSerial(void) const;

  /**
   * 
   */
  CSerial *getSerial(void);

  /**
   * @return Port we're connected to
   */
  const char *getSerialPort(void) const;

  /**
   * Reads 1 (if any) packet from the serial connection and processes it.
   * @return False of error.
   */
  bool listenOnce(void);

  /**
   * Reads and processes all available packets.
   */
  bool listenAll(void);

  /**
   * Tries to establish a verified connection to the robot. Error are printed
   * to stderr.
   */
  bool connect(const char *szSerialPort);

  /**
   * Safely closes the serial connection.
   * @return True on success. On failure, check the serial's last error.
   */
  bool disconnect(void);

  /**
   * Resets the arduino.
   * @return True on success.
   */
  bool reset(void);

  /**
   * Sets the target model.
   */
  bool setTarget(const CModel &model);

  /**
   * @return True if the given node is reachable.
   */
  bool isNodeReachable(const CObserverNode& node, ECheckZ checkZ = ECheckZ::kNormal) const;

  /**
   * Notifies the robot that the alignment node is now aligned.
   * @param pVecPosition If nullptr, requests the arduino's position. When the
   *                  position is received, this function is called with the
   *                  position.
   */
  void setAlign(const CVector3f* pVecPosition = nullptr);

  /**
   * Moves the robot into a position that allows the physical target to be
   * aligned with the internal model.
   */
  void align(void);

  /**
   * Starts the scan. Blocks.
   */
  void scan(void);

  /**
   * @return The observer path.
   */
  const CObserverPath& getPath(void) const;

  /**
   * 
   */
  CRobot(void);
};

#endif