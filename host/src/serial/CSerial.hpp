#if !defined(HPPserial_CSerial)
#define HPPserial_CSerial

#include <optional>
#include <tuple>
#include <vector>

#include "protocol/protocol.hpp"

struct termios;

/**
 * Based on {@link https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/}
 * [2019-08-09]
 */
class CSerial
{
private:
  // In milliseconds.
  constexpr static int k_iReadTimeOut{ 1000 };

public:
  enum EError
  {
    kSuccess = 0,
    kOpen,
    kClose,
    kRead,
    kWrite,
    kGetAttributes,
    kSetAttributes,
    kSetInputSpeed,
    kSetOutputSpeed,
    kPartialHeaderRead,
    kPartialPayloadRead,
    kDeviceDisconnected,

    kCount,
  };

  static constexpr const char *EErrorSTR[EError::kCount]{
    "success",
    "open",
    "close",
    "read",
    "write",
    "get_attributes",
    "set_attributes",
    "set_input_speed",
    "set_output_speed",
    "partial_header_read",
    "partial_payload_read",
    "device_disconnected",
  };

private:
  /**
   * 
   */
  int m_iFileDescriptor{ -1 };

  /**
   * Last error in a library (errno).
   */
  int m_iLastError{};

  /**
   * Last error in our code.
   */
  EError m_lastError{};

private:
  /**
   * Sets {@link m_iLastError} and {@link m_lastError}
   * @return false
   */
  bool errorOut(EError error);

  /**
   * Configures a termios session
   */
  bool configure(termios *pTTY);

  /**
   * @param pRead Out, number of bytes read.
   */
  bool readImpl(char *buffer, int iLength, int *pRead);

public:
  /**
   * 
   */
  bool open(const char *szPort);

  /**
   * 
   */
  bool close(void);

  /**
   * @return ~~Number of bytes available for reading~~. 1 if data is available
   * for reading, -1 on error, 0 otherwise.
   */
  int available(void);

  /**
   * Tries to read @iLength bytes. If less bytes are read, false is returned.
   * {@link getSerial()->getLastError} should be checked.
   * @param pRead Out, number of bytes read.
   */
  bool read(char *buffer, int iLength, int *pRead);

  /**
   * Reads the next packet.
   * If no packet is returned, {@link getLastError} should be checked.
   */
  std::optional<protocol::CPacket> readPacket(void);

  /**
   * 
   */
  bool write(const char *buffer, int iLength);

  /**
   * 
   */
  bool write(const protocol::CPacket &pckSend);

  /**
   * 
   */
  template <class T>
  bool write(protocol::ECommand command, const T &data)
  {
    return this->write(protocol::CPacket{ command, data });
  }

  /**
   * Clears the error flags.
   */
  void clearLastError(void);

  /**
   * Check the {@link EError} component to see if an error occurred.
   */
  std::tuple<EError, int> getLastError(void) const;

  /**
   * @return The last error as a human-readable string.
   */
  std::string getLastErrorString(void) const;

public:
  /**
   * 
   */
  CSerial(void);
};

#endif