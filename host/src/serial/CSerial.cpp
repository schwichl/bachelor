#include "CSerial.hpp"

#include <cstring>
#include <fcntl.h>
#include <termio.h>
#include <unistd.h>

/**
 * 
 */
bool CSerial::errorOut(CSerial::EError error)
{
  this->m_lastError = error;
  this->m_iLastError = errno;
  return false;
}

/**
 * {@link https://linux.die.net/man/3/termios}
 */
bool CSerial::configure(termios *pTTY)
{
  pTTY->c_cflag &= static_cast<tcflag_t>(~CSTOPB);

  pTTY->c_cflag |= CS8;
  pTTY->c_cflag |= (CREAD | CLOCAL);
  pTTY->c_cflag &= static_cast<tcflag_t>(~PARENB);
  pTTY->c_cflag &= static_cast<tcflag_t>(~CRTSCTS);

  pTTY->c_lflag &= static_cast<tcflag_t>(~ICANON);
  pTTY->c_lflag &= static_cast<tcflag_t>(~ECHO);
  pTTY->c_lflag &= static_cast<tcflag_t>(~ECHOE);
  pTTY->c_lflag &= static_cast<tcflag_t>(~ECHONL);
  pTTY->c_lflag &= static_cast<tcflag_t>(~ISIG);

  pTTY->c_iflag &= static_cast<tcflag_t>(~(IXON | IXOFF | IXANY));
  pTTY->c_iflag &= static_cast<tcflag_t>(~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL));

  pTTY->c_oflag &= static_cast<tcflag_t>(~OPOST);
  pTTY->c_oflag &= static_cast<tcflag_t>(~ONLCR);

  pTTY->c_cc[VTIME] = (CSerial::k_iReadTimeOut / 100);
  pTTY->c_cc[VMIN] = 0;

  if (cfsetispeed(pTTY, B9600) == -1)
  {
    return this->errorOut(EError::kSetInputSpeed);
  }

  if (cfsetospeed(pTTY, B9600) == -1)
  {
    return this->errorOut(EError::kSetOutputSpeed);
  }

  if (tcsetattr(this->m_iFileDescriptor, TCSANOW, pTTY) == -1)
  {
    return this->errorOut(EError::kSetAttributes);
  }

  return true;
}

/**
 * 
 */
bool CSerial::readImpl(char *buffer, int iLength, int *pRead)
{
  fd_set fdSet{};
  FD_SET(this->m_iFileDescriptor, &fdSet);
  timeval tvTimeOut{ CSerial::k_iReadTimeOut / 1000, 0 };

  int iStatus{ ::select(this->m_iFileDescriptor + 1, &fdSet, nullptr, nullptr, &tvTimeOut) };

  if (iStatus == -1)
  {
    // Same error is returned below.
    return this->errorOut(EError::kRead);
  }

  if (FD_ISSET(this->m_iFileDescriptor, &fdSet))
  {
    // If no data is available, this will cause a double-wait. This will only
    // happen on disconnect, so it doesn't matter.
    int iRead{ static_cast<int>(::read(this->m_iFileDescriptor, buffer, static_cast<size_t>(iLength))) };

    if (iRead == -1)
    {
      // Same error is returned above.
      return this->errorOut(EError::kRead);
    }
    else if (iRead == 0)
    {
      if (this->m_iFileDescriptor != -1)
      {
        this->close();
      }

      // {@link ::select} said there is data, but there is none. This only
      // happens when the device disconnected.
      return this->errorOut(EError::kDeviceDisconnected);
    }

    *pRead = iRead;
  }
  else
  {
    *pRead = 0;
  }

  return true;
}

/**
 * 
 */
bool CSerial::open(const char *szPort)
{
  if ((this->m_iFileDescriptor = ::open(szPort, O_RDWR)) == -1)
  {
    return this->errorOut(EError::kOpen);
  }

  termios tty{};

  if (tcgetattr(this->m_iFileDescriptor, &tty) == -1)
  {
    this->close();
    return this->errorOut(EError::kGetAttributes);
  }

  if (!this->configure(&tty))
  {
    this->close();
    // {@link configure} already called {@link errorOut}
    return false;
  }

  return true;
}

/**
 * 
 */
bool CSerial::close(void)
{
  if (::close(this->m_iFileDescriptor) == -1)
  {
    return this->errorOut(EError::kClose);
  }

  this->m_iFileDescriptor = -1;

  return true;
}

/**
 * 
 */
int CSerial::available(void)
{
  fd_set fdSet{};
  FD_SET(this->m_iFileDescriptor, &fdSet);
  timeval tvTimeOut{ 0, 0 };

  int iStatus{ ::select(this->m_iFileDescriptor + 1, &fdSet, nullptr, nullptr, &tvTimeOut) };

  if (iStatus == -1)
  {
    this->errorOut(EError::kRead);
    return -1;
  }

  if (FD_ISSET(this->m_iFileDescriptor, &fdSet))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/**
 * 
 */
bool CSerial::read(char *buffer, int iLength, int *pRead)
{
  int iReadTotal{ 0 };

  while (iReadTotal < iLength)
  {
    int iRead{};

    if (!this->readImpl(&buffer[iReadTotal], iLength - iReadTotal, &iRead))
    {
      // Error
      return false;
    }

    if (iRead == 0)
    {
      // End of data before the requested length was received.
      *pRead = iReadTotal;
      return false;
    }

    iReadTotal += iRead;
  }

  *pRead = iReadTotal;
  return true;
}

/**
 * 
 */
std::optional<protocol::CPacket> CSerial::readPacket(void)
{
  static constexpr int k_iHeaderSize{
    static_cast<int>(sizeof(protocol::CPacket::SHeader))
  };

  int iBytesRead{};
  protocol::CPacket::SHeader header;

  if (!this->read(
          reinterpret_cast<char *>(&header),
          k_iHeaderSize,
          &iBytesRead))
  {
    // Last error was set by {@link read}.
    return {};
  }

  if (iBytesRead != k_iHeaderSize)
  {
    if (iBytesRead > 0)
    {
      this->m_lastError = EError::kPartialHeaderRead;
    }

    return {};
  }

  char *arrPayload{ new char[static_cast<std::size_t>(header.iLength)] };

  if (!this->read(
          arrPayload,
          header.iLength,
          &iBytesRead))
  {
    // Last error was set by {@link read}.
    return {};
  }

  if (iBytesRead != header.iLength)
  {
    delete[] arrPayload;
    this->m_lastError = EError::kPartialPayloadRead;
    return {};
  }

  return protocol::CPacket::fromRaw(std::move(header), arrPayload);
}

/**
 * 
 */
bool CSerial::write(const char *buffer, int iLength)
{
  if (::write(this->m_iFileDescriptor, buffer, static_cast<size_t>(iLength)) == -1)
  {
    return this->errorOut(EError::kWrite);
  }

  return true;
}

/**
 * 
 */
bool CSerial::write(const protocol::CPacket &pckSend)
{
  if (!this->write(pckSend.getRawHeader(), static_cast<int>(sizeof(protocol::CPacket::SHeader))))
  {
    return false;
  }

  if (!this->write(pckSend.getRawPayload(), pckSend.getPayloadLength()))
  {
    return false;
  }

  return true;
}

/**
 * 
 */
void CSerial::clearLastError(void)
{
  this->m_lastError = EError::kSuccess;
  this->m_iLastError = 0;
}

/**
 * 
 */
std::tuple<CSerial::EError, int> CSerial::getLastError(void) const
{
  return { this->m_lastError, this->m_iLastError };
}

/**
 * 
 */
std::string CSerial::getLastErrorString(void) const
{
  return (std::string{ EErrorSTR[this->m_lastError] } + " " + strerror(this->m_iLastError));
}

/**
 * 
 */
CSerial::CSerial(void)
{
}