#include "commands/CCommandHandler.hpp"
#include "robot/CRobot.hpp"

#include <atomic>
#include <chrono>
#include <csignal>
#include <cstring> 
#include <iostream>
#include <thread>

/**
 * Gets and processes the next command in command mode.
 */
void getCommand(void)
{
  std::string strInput;

  std::cout << "\n> ";
  std::getline(std::cin, strInput);

  if (!strInput.empty())
  {
    CCommandHandler::instance()->processInput(strInput);
  }
}

int main(int argc, char* argv[])
{
  if (auto err{ protocol::verifyOffline() };
      err != protocol::EVerificationError::kSuccess)
  {
    std::cerr << "protocol verification failed: "
              << protocol::EVerificationErrorSTR[static_cast<int>(err)] << '\n';
    return -1;
  }

  static std::atomic<bool> s_bExit{ false };
  static std::atomic<bool> s_bIsInCommandMode{ false };

  std::signal(SIGINT, [](int iSignal) {
    if (iSignal == SIGINT)
    {
      if (s_bIsInCommandMode)
      {
        // Allow graceful termination when the loop ends.
        // Any additional interrupts will cause immediate termination.
        // (1) Ctrl+C -> Command mode
        // (2) Ctrl+C followed by Enter -> Graceful termination
        // (3) Ctrl+C -> Immediate termination

        s_bExit = true;

        std::signal(SIGINT, SIG_DFL);
      }
      else
      {
        s_bIsInCommandMode = true;
      }
    }
  });

  // testObjParser();

  if (argc != 2)
  {
    std::cout << "usage: bachelor_host <serial_port>\n";
    return 0;
  }

  const char* szSerialPort{ argv[1] };

  if (std::strcmp(szSerialPort, "--offline") == 0)
  {
    std::cout << "offline mode. CRobot is uninitialized!\n";

    while (!s_bExit)
    {
      getCommand();
    }
  }
  else
  {
    if (CRobot::instance()->connect(szSerialPort))
    {
      std::cout << "connected to " << szSerialPort << '\n';
      std::cout << "press ctrl+c to enter command mode\n";

      // if (0)
      while (!s_bExit)
      {
        // Give the Arduino some time to process the command.
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        if (!CRobot::instance()->listenAll())
        {
          std::cout << "read failed, exiting\n";
          break;
        }

        if (s_bIsInCommandMode)
        {
          getCommand();

          s_bIsInCommandMode = false;
        }
      }

      if (!CRobot::instance()->disconnect())
      {
        std::cerr << "couldn't disconnect\n";
        std::cerr << CRobot::instance()->getSerial()->getLastErrorString() << '\n';
      }
    }
  }

  std::cout << "done\n";

  return 0;
}